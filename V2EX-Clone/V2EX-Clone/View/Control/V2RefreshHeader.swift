//
//  V2RefreshHeader.swift
//  V2EX-Clone
//
//  Created by yidao on 2019/9/24.
//  Copyright © 2019 yidao. All rights reserved.
//

import UIKit
import MJRefresh

class V2RefreshHeader: MJRefreshHeader {
    var loadingView: UIActivityIndicatorView?
    var arrowImageView: UIImageView?
    
    override var state: MJRefreshState {
        didSet {
            switch state {
            case .idle:
                self.loadingView?.isHidden = true
                self.arrowImageView?.isHidden = false
                self.loadingView?.stopAnimating()
            case .pulling:
                self.loadingView?.isHidden = false
                self.arrowImageView?.isHidden = true
                self.loadingView?.startAnimating()
            case .refreshing:
                self.loadingView?.isHidden = false
                self.arrowImageView?.isHidden = true
                self.loadingView?.startAnimating()
            default:
                debugPrint("")
            }
        }
    }
    
    override func prepare() {
        super.prepare()
        self.mj_h = 50
        
        self.loadingView = UIActivityIndicatorView(style: .white)
        self.addSubview(self.loadingView!)
        
        self.arrowImageView = UIImageView(image: UIImage(named: "ic_arrow_downward")?.withRenderingMode(.alwaysTemplate))
        self.addSubview(self.arrowImageView!)

        self.themeChangedHandler = { [weak self] (style) in
            guard let self = self else { return }
            if V2EXColor.instance.style == V2EXColor.V2EXColorStyleDefault {
                self.loadingView?.style = .gray
                self.arrowImageView?.tintColor = .gray
            } else {
                self.loadingView?.style = .white
                self.arrowImageView?.tintColor = .white
            }
        }
    }
    
    override func placeSubviews() {
        super.placeSubviews()
        self.loadingView!.center = CGPoint(x: self.mj_w / 2, y: self.mj_h / 2)
        self.arrowImageView!.frame = CGRect(x: 0, y: 0, width: 24, height: 24)
        self.arrowImageView!.center = self.loadingView!.center
    }

}

//
//  V2CommentAttachmentImage.swift
//  V2EX-Clone
//
//  Created by yidao on 2019/9/25.
//  Copyright © 2019 yidao. All rights reserved.
//

import UIKit
import Kingfisher

protocol V2CommentAttachmentImageTapDelegate: class {
    func V2CommentAttachmentImageSingleTap(_ imageView: V2CommentAttachmentImage)
}

class V2CommentAttachmentImage: AnimatedImageView {
    var index: Int = 0 // 父容器中的第几张图片
    var imageURL: String? // 图片地址
    
    weak var attachmentDelegate: V2CommentAttachmentImageTapDelegate?
    
    init() {
        super.init(frame: CGRect(x: 0, y: 0, width: 80, height: 80))
        self.autoPlayAnimatedImage = false
        self.contentMode = .scaleAspectFill
        self.clipsToBounds = true
        self.isUserInteractionEnabled = true
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func willMove(toSuperview newSuperview: UIView?) {
        super.willMove(toSuperview: newSuperview)
        if self.image != nil {
            return
        }
        
        if let imageURL = self.imageURL, let url = URL(string: imageURL) {
            self.kf.setImage(with: url, placeholder: nil, options: nil, progressBlock: nil) { (image, error, cacheType, imageURL) in
                if let image = image {
                    if image.size.width < 80 && image.size.height < 80 {
                        self.contentMode = .bottomLeft
                    }
                }
            }
        }
    }
    
    override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
        let touch = touches.first
        let tapCount = touch?.tapCount
        if let tapCount = tapCount {
            if tapCount == 1 {
                self.handleSignleTap(touch!)
            }
        }
        // 取消后续的事件响应
        self.next?.touchesCancelled(touches, with: event)
    }
    
    func handleSignleTap(_ touch: UITouch) {
        self.attachmentDelegate?.V2CommentAttachmentImageSingleTap(self)
    }
}

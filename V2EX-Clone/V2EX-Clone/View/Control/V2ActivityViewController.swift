//
//  V2ActivityViewController.swift
//  V2EX-Clone
//
//  Created by yidao on 2019/9/26.
//  Copyright © 2019 yidao. All rights reserved.
//

import UIKit

@objc protocol V2ActivityViewDataSource {
    // 获取几个
    func V2ActivityView(_ activityView: V2ActivityViewController, numberOfCellsInSection section: Int) -> Int
    
    // 返回V2Activity，主要是标题和图片
    func V2ActivityView(_ activityView: V2ActivityViewController, ActivityAtIndexPath indexPath: IndexPath) -> V2Activity
    // 有几组
    @objc optional func numberOfSectionsInV2ActivityView(_ activityView: V2ActivityViewController) -> Int
    
    @objc optional func V2ActivityView(_ activityView:V2ActivityViewController ,heightForHeaderInSection section: Int) -> CGFloat
    @objc optional func V2ActivityView(_ activityView:V2ActivityViewController ,heightForFooterInSection section: Int) -> CGFloat
    @objc optional func V2ActivityView(_ activityView:V2ActivityViewController ,viewForHeaderInSection section: Int) ->UIView?
    @objc optional func V2ActivityView(_ activityView:V2ActivityViewController ,viewForFooterInSection section: Int) ->UIView?
    
    @objc optional func V2ActivityView(_ activityView: V2ActivityViewController, didSelectRowAtIndexPath indexPath: IndexPath)

}

class V2Activity: NSObject {
    var title: String
    var image: UIImage
    init(title: String, image aImage: UIImage) {
        self.title = title
        self.image = aImage
    }
}

class V2ActivityButton: UIButton {
    var indexPath: IndexPath?
}

class V2ActivityViewController: UIViewController {
    weak var dataSource: V2ActivityViewDataSource?
    
    var section: Int {
        if let _section = dataSource?.numberOfSectionsInV2ActivityView?(self) {
            return _section
        } else {
            return 1
        }
    }
    
    var panel: UIToolbar = CTToolBar()
    
    func numberOfCellsInSection(_ section: Int) -> Int {
        if var cells = dataSource?.V2ActivityView(self, numberOfCellsInSection: section) {
            if cells > 5 { cells = 5}
            return cells
        } else {
            return 0
        }
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.view.backgroundColor = UIColor(white: 0, alpha: 0)
        self.transitioningDelegate = self
        
        self.panel.barStyle = V2EXColor.instance.style == V2EXColor.V2EXColorStyleDefault ? .default : .black
        self.view.addSubview(self.panel)
        self.panel.snp.makeConstraints { (make) in
            make.bottom.equalToSuperview().offset(-90)
            make.left.equalToSuperview().offset(10)
            make.right.equalToSuperview().offset(-10)
        }
        self.panel.layer.cornerRadius = 6.0
        self.panel.layer.masksToBounds = true
        
        self.setupView()
        
        if let lastView = self.panel.subviews.last {
            self.panel.snp.makeConstraints { (make) in
                make.bottom.equalTo(lastView)
            }
        }
        
        let cancelPanel = CTToolBar()
        cancelPanel.barStyle = self.panel.barStyle
        cancelPanel.layer.cornerRadius = self.panel.layer.cornerRadius
        cancelPanel.layer.masksToBounds = true
        self.view.addSubview(cancelPanel)
        cancelPanel.snp.makeConstraints { (make) in
            make.top.equalTo(self.panel.snp_bottom).offset(10)
            make.left.right.equalTo(self.panel)
            make.height.equalTo(45)
        }
        
        let cancelBtn = UIButton()
        cancelBtn.setTitle("cancel2".localized, for: .normal)
        cancelBtn.setTitleColor(V2EXColor.colors.v2_topicListTitleColor, for: .normal)
        cancelPanel.addSubview(cancelBtn)
        cancelBtn.snp.makeConstraints { (make) in
            make.edges.equalToSuperview()
        }
    
        cancelBtn.addTarget(self, action: #selector(self.dismissVC), for: .touchUpInside)
        self.view.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(self.dismissVC)))
    }
    
    private func setupView() {
        for i in 0 ..< section {
            let sectionView = self.setupSectionView(i)
            self.panel.addSubview(sectionView)
            sectionView.snp.makeConstraints { (make) in
                make.left.right.equalTo(self.panel)
                make.height.equalTo(110)
                if self.panel.subviews.count > 1 {
                    make.top.equalTo(self.panel.subviews.first!.snp_bottom)
                } else {
                    make.top.equalTo(self.panel)
                }
            }
            
            self.setupFooterView(i)
        }

        
        
    }
    
    private func setupSectionView(_ _section: Int) -> UIView {
        let sectionView = UIView()
        
        let margin = (kScreenWidth - 20 - 60 * 5) / 6.0
        for i in 0 ..< self.numberOfCellsInSection(_section) {
            let cellView = self.setupCellView(i, currentSection: _section)
            sectionView.addSubview(cellView)
            
            cellView.snp.makeConstraints { (make) in
                make.width.equalTo(60)
                make.height.equalTo(80)
                make.centerY.equalTo(sectionView)
                make.left.equalTo(sectionView).offset(CGFloat(i + 1) * margin + CGFloat(i * 60))
            }
        }
        return sectionView
    }
    
    private func setupFooterView(_ _section: Int) {
        if let view = dataSource?.V2ActivityView?(self, viewForFooterInSection: _section) {
            var height = dataSource?.V2ActivityView?(self, heightForFooterInSection: _section)
            if height == nil { height = 40}
            self.panel.addSubview(view)
            
            view.snp.makeConstraints { (make) in
                make.left.right.equalToSuperview()
                make.height.equalTo(height!)
                if self.panel.subviews.count > 1 {
                    make.top.equalTo(self.panel.subviews.first!.snp_bottom)
                } else {
                    make.top.equalToSuperview()
                }
            }
        }
    }
    
    private func setupCellView(_ index: Int, currentSection: Int) -> UIView {
        let cellView = UIView()
        
        let buttonBackgroundView = UIImageView()
        buttonBackgroundView.image = UIColor.createImageByColor(V2EXColor.colors.v2_cellWhiteBackgroundColor, size: CGSize(width: 15, height: 15)).roundedCornerImageWithCornerRadius(5).stretchableImage(withLeftCapWidth: 7, topCapHeight: 7)
        cellView.addSubview(buttonBackgroundView)
        buttonBackgroundView.snp.makeConstraints { (make) in
            make.width.height.equalTo(60)
            make.top.left.equalTo(cellView)
        }
        
        let activity = dataSource?.V2ActivityView(self, ActivityAtIndexPath: IndexPath(row: index, section: currentSection))
        
        let button = V2ActivityButton()
        button.setImage(activity?.image.withRenderingMode(.alwaysTemplate), for: .normal)
        cellView.addSubview(button)
        button.tintColor = V2EXColor.colors.v2_topicListUserNameColor
        button.snp.makeConstraints { (make) in
            make.edges.equalTo(buttonBackgroundView)
        }
        
        button.indexPath = IndexPath(row: index, section: currentSection)
        button.addTarget(self, action: #selector(self.cellDidSelected(_:)), for: .touchUpInside)
        
        let titleLabel = UILabel()
        titleLabel.textAlignment = .center
        titleLabel.numberOfLines = 2
        titleLabel.text = activity?.title
        titleLabel.font = UIFont.systemFont(ofSize: 12.0)
        titleLabel.textColor = V2EXColor.colors.v2_topicListTitleColor
        cellView.addSubview(titleLabel)
        titleLabel.snp.makeConstraints { (make) in
            make.centerX.equalTo(cellView)
            make.left.right.equalToSuperview()
            make.top.equalTo(buttonBackgroundView.snp_bottom).offset(5)
        }
        
        return cellView
    }
    
    
    
    @objc func dismissVC() {
        self.dismiss(animated: true, completion: nil)
    }
    
    @objc private func cellDidSelected(_ sender: V2ActivityButton) {
        dataSource?.V2ActivityView?(self, didSelectRowAtIndexPath: sender.indexPath!)
    }
}

extension V2ActivityViewController: UIViewControllerTransitioningDelegate {
    func animationController(forPresented presented: UIViewController,
    presenting: UIViewController,
    source: UIViewController) -> UIViewControllerAnimatedTransitioning? {
     return V2ActivityTransionPresent()
    }
    
    
    func animationController(forDismissed dismissed: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        return V2ActivityTransionDismiss()
    }
}

class V2ActivityTransionPresent: NSObject, UIViewControllerAnimatedTransitioning {
    func transitionDuration(using transitionContext: UIViewControllerContextTransitioning?) -> TimeInterval {
        return 1.0
    }
    
    func animateTransition(using transitionContext: UIViewControllerContextTransitioning) {
        let container = transitionContext.containerView
        
        let fromVC = transitionContext.viewController(forKey: .from)
        fromVC?.view.isHidden = true
        let screenShotImage = fromVC?.view.screenshot()
        let tempView = UIImageView(image: screenShotImage)
        tempView.tag = 9998
        container.addSubview(tempView)
        
        let toVC = transitionContext.viewController(forKey: .to)
        container.addSubview(toVC!.view)
        
        toVC?.view.frame = CGRect(x: 0, y: kScreenHeight, width: kScreenWidth, height: kScreenHeight)
        UIView.animate(withDuration: transitionDuration(using: transitionContext), delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 7, options: .curveLinear, animations: {
            toVC?.view.frame = CGRect(x: 0, y: 0, width: kScreenWidth, height: kScreenHeight)
            tempView.transform = CGAffineTransform(scaleX: 0.98, y: 0.98)
        }) { (finished) in
            transitionContext.completeTransition(!transitionContext.transitionWasCancelled)
        }
    }
    
}


class V2ActivityTransionDismiss: NSObject, UIViewControllerAnimatedTransitioning {
    func transitionDuration(using transitionContext: UIViewControllerContextTransitioning?) -> TimeInterval {
        return 0.3
    }
    
    func animateTransition(using transitionContext: UIViewControllerContextTransitioning) {
        let container = transitionContext.containerView
        
        let toVC = transitionContext.viewController(forKey: .to)
        container.addSubview(toVC!.view)
        
        let fromVC = transitionContext.viewController(forKey: .from)
        container.addSubview(fromVC!.view)
        
        let tempView = container.viewWithTag(9998)
        
        UIView.animate(withDuration: transitionDuration(using: transitionContext), animations: {
            fromVC?.view.frame = CGRect(x: 0, y: kScreenHeight, width: kScreenWidth, height: kScreenHeight)
//            tempView?.transform = .identity
        }) { (finished) in
            transitionContext.completeTransition(!transitionContext.transitionWasCancelled)
            toVC?.view.isHidden = false
        }
        
    }
}





class CTToolBar: UIToolbar {
    override func layoutSubviews() {
        super.layoutSubviews()
        // Fix iOS11 UIToolbar的子View都不响应点击事件
        for view in self.subviews {
            if NSStringFromClass(view.classForCoder) == "_UIToolbarContentView" {
                view.isUserInteractionEnabled = false
            }
        }
    }
}

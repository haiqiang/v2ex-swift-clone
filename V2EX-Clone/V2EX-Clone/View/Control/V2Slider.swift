//
//  V2Slider.swift
//  V2EX-Clone
//
//  Created by yidao on 2019/9/23.
//  Copyright © 2019 yidao. All rights reserved.
//

import UIKit

class V2Slider: UISlider {

    var valueChanged: ((_ value: Float) -> Void)?
    
    override init(frame: CGRect) {
        super.init(frame: .zero)
        
        self.minimumValue = 0
        self.maximumValue = 16
        
        self.value = (V2Style.sharedInstance.fontScale - 0.8) / 0.5 * 10
        self.addTarget(self, action: #selector(valueChanged(_:)), for: .valueChanged)
        
        self.themeChangedHandler = {[weak self] (style) -> Void in
            guard let self = self else { return }
            self.minimumTrackTintColor = V2EXColor.colors.v2_topicListTitleColor
            self.maximumTrackTintColor = V2EXColor.colors.v2_backgroundColor
        }
    }
    
    deinit {
        debugPrint("deinit - \(self)")
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    @objc func valueChanged(_ sender: UISlider) {
        sender.value = Float(Int(sender.value))
        valueChanged?(sender.value)
    }
}

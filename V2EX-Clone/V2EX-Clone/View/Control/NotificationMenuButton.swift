//
//  NotificationMenuButton.swift
//  V2EX-Clone
//
//  Created by yidao on 2019/9/11.
//  Copyright © 2019 yidao. All rights reserved.
//

import UIKit

class NotificationMenuButton: UIButton {
    var aPointImageView: UIImageView?
    override init(frame: CGRect) {
        super.init(frame: .zero)
        
        self.contentMode = .center
        self.imageEdgeInsets = UIEdgeInsets(top: 0, left: -10, bottom: 0, right: 0)
        self.setImage(UIImage(named: "ic_menu_36pt"), for: .normal)
        
        self.aPointImageView = UIImageView()
        self.aPointImageView!.backgroundColor = V2EXColor.colors.v2_noticePointColor
        self.aPointImageView!.layer.cornerRadius = 4
        self.aPointImageView!.layer.masksToBounds = true
        self.addSubview(self.aPointImageView!)
        self.aPointImageView!.snp.makeConstraints{ (make) -> Void in
            make.width.height.equalTo(8)
            make.top.equalToSuperview().offset(3)
            make.right.equalToSuperview().offset(-6)
        }
        
        self.kvoController.observe(V2User.sharedInstance, keyPath: "notificationCount", options: [.initial,.new]) {  [weak self](cell, clien, change) -> Void in
            guard let self = self else { return }
            if V2User.sharedInstance.notificationCount > 0 {
                self.aPointImageView!.isHidden = false
            }
            else{
                self.aPointImageView!.isHidden = true
            }
        }
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

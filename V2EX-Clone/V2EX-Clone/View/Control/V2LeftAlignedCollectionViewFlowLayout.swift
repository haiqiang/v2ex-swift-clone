//
//  V2LeftAlignedCollectionViewFlowLayout.swift
//  V2EX-Clone
//
//  Created by yidao on 2019/9/23.
//  Copyright © 2019 yidao. All rights reserved.
//

import UIKit

class V2LeftAlignedCollectionViewFlowLayout: UICollectionViewFlowLayout {
    var cellSpacing: CGFloat = 15
    
    override func layoutAttributesForElements(in rect: CGRect) -> [UICollectionViewLayoutAttributes]? {
        let attributesToReturn: [UICollectionViewLayoutAttributes] = NSArray(array: super.layoutAttributesForElements(in: rect) ?? [], copyItems: true) as! Array
        
        for attributes in attributesToReturn {
            if attributes.representedElementKind == nil {
                let indexPath = attributes.indexPath
                attributes.frame = self.layoutAttributesForItem(at: indexPath)?.frame ?? .zero
            }
        }
        return attributesToReturn
    }
    
    override func layoutAttributesForItem(at indexPath: IndexPath) -> UICollectionViewLayoutAttributes? {
        let currentItemAttributes: UICollectionViewLayoutAttributes = super.layoutAttributesForItem(at: indexPath)?.copy() as! UICollectionViewLayoutAttributes
        
        let sectionInset = self.sectionInset
        if indexPath.item == 0 {
            var frame = currentItemAttributes.frame
            frame.origin.x = sectionInset.left
            currentItemAttributes.frame = frame
            return currentItemAttributes
        }

        let previousIndexPath = IndexPath(item: indexPath.item - 1, section: indexPath.section)
        let previousFrame = self.layoutAttributesForItem(at: previousIndexPath)?.frame ?? .zero
        let previousFrameRightPoint = previousFrame.maxX + cellSpacing
        let currentFrame = currentItemAttributes.frame
        let stretchedCurrentFrame = CGRect(x: 0, y: currentFrame.minY, width: self.collectionView!.bounds.width, height: currentFrame.height)

        if !previousFrame.intersects(stretchedCurrentFrame) {
            var frame = currentItemAttributes.frame
            frame.origin.x = sectionInset.left
            currentItemAttributes.frame = frame
            return currentItemAttributes
        }

        var frame = currentItemAttributes.frame
        frame.origin.x = previousFrameRightPoint
        currentItemAttributes.frame = frame
        return currentItemAttributes
    }

}

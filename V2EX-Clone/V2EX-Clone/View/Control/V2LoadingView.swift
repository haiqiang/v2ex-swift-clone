//
//  V2LoadingView.swift
//  V2EX-Clone
//
//  Created by yidao on 2019/9/6.
//  Copyright © 2019 yidao. All rights reserved.
//

import UIKit

let noticeString = [
    "正在拼命加载",
    "前方发现楼主",
    "年轻人,不要着急",
    "让我飞一会儿",
    "大爷,您又来了?",
    "楼主正在抓皮卡丘，等他一会儿吧",
    "爱我，就等我一万年",
    "未满18禁止入内",
    "正在前往 花村",
    "正在前往 阿努比斯神殿",
    "正在前往 沃斯卡娅工业区",
    "正在前往 观测站：直布罗陀",
    "正在前往 好莱坞",
    "正在前往 66号公路",
    "正在前往 国王大道",
    "正在前往 伊利奥斯",
    "正在前往 漓江塔",
    "正在前往 尼泊尔"
]

class V2LoadingView: UIView {
    var activityIndicatorView = UIActivityIndicatorView(style: .gray)
    
    override init(frame: CGRect) {
        super.init(frame: .zero)
       
        addSubview(activityIndicatorView)
        activityIndicatorView.snp.makeConstraints { make in
            make.centerX.equalToSuperview()
            make.centerY.equalToSuperview().offset(-32)
        }
        
        addSubview(self.noticeLabel)
        noticeLabel.snp.makeConstraints { make in
            make.top.equalTo(activityIndicatorView.snp.bottom).offset(10)
            make.centerX.equalTo(activityIndicatorView)
        }
        
        self.themeChangedHandler = { [weak self] style -> Void in
            guard let self = self else {
                return
            }
            if V2EXColor.instance.style == V2EXColor.V2EXColorStyleDefault {
                self.activityIndicatorView.style = .gray
            } else {
                self.activityIndicatorView.style = .white
            }
        }
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func willMove(toSuperview newSuperview: UIView?) {
        self.activityIndicatorView.startAnimating()
    }
    
    func hide() {
        self.superview?.bringSubviewToFront(self)
        
        UIView.animate(withDuration: 0.2, animations: {
            self.alpha = 0
        }) { finished in
            if finished {
                self.removeFromSuperview()
            }
        }
    }
    
    lazy var noticeLabel: UILabel = {
       let label = UILabel()
        label.text = noticeString[Int(arc4random() % UInt32(noticeString.count))]
        label.font = UIFont.systemFont(ofSize: 10.0)
        label.textColor = V2EXColor.colors.v2_topicListDateColor
        return label
    }()
}

//
//  V2FPSLabel.swift
//  V2EX-Clone
//
//  Created by yidao on 2019/9/9.
//  Copyright © 2019 yidao. All rights reserved.
//

import UIKit

class V2FPSLabel: UILabel {
    private var _link: CADisplayLink?
    private var _count: Int = 0
    private var _lastTime: TimeInterval = 0.0
    
    private let _defaultSize = CGSize(width: 55.0, height: 20.0)
    
    override init(frame: CGRect) {
        var targetFrame = frame
        if frame.size.width == 0 && frame.size.height == 0 {
            targetFrame.size = _defaultSize
        }
        super.init(frame: targetFrame)
        self.layer.cornerRadius = 5.0
        self.clipsToBounds = true
        self.textAlignment = .center
        self.isUserInteractionEnabled = false
        self.textColor = UIColor(white: 0, alpha: 0.7)
        self.font = UIFont(name: "Menlo", size: 14.0)
        _link = CADisplayLink(target: self, selector: #selector(self.tick(_:)))
        _link?.add(to: RunLoop.main, forMode: RunLoop.Mode.common)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    @objc func tick(_ link: CADisplayLink) {
        if _lastTime == 0 {
            _lastTime = link.timestamp
            return
        }
        _count += 1
        let delta = link.timestamp - _lastTime
        if delta < 1 {
            return
        }
        _lastTime = link.timestamp
        
        let fps = Double(_count) / delta
        _count = 0
        
        let progress = fps / 60.0
        self.textColor = UIColor(hue: CGFloat(0.27 * (progress - 0.2)), saturation: 1, brightness: 0.9, alpha: 1)
        self.text = "\(Int(fps + 0.5))FPS"
    }
    
    deinit {
        _link?.invalidate()
    }
}

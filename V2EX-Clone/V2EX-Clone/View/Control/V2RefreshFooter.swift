//
//  V2RefreshFooter.swift
//  V2EX-Clone
//
//  Created by yidao on 2019/9/26.
//  Copyright © 2019 yidao. All rights reserved.
//

import MJRefresh

class V2RefreshFooter: MJRefreshAutoFooter {
    var loadingView: UIActivityIndicatorView?
    var stateLabel: UILabel?
    
    var centerOffset: CGFloat = 0
    
    private var _noMoreDataStateString: String?
    var noMoreDataStateString: String? {
        get {
            return self._noMoreDataStateString
        }
        set {
            self._noMoreDataStateString = newValue
            self.stateLabel?.text = newValue
        }
    }
    
    override var state: MJRefreshState {
        didSet {
            switch state {
            case .idle:
                self.stateLabel?.text = nil
                self.loadingView?.isHidden = true
                self.loadingView?.stopAnimating()
            case .refreshing:
                self.stateLabel?.text = nil
                self.loadingView?.isHidden = false
                self.loadingView?.startAnimating()
            case .noMoreData:
                self.stateLabel?.text = self.noMoreDataStateString
                self.loadingView?.isHidden = true
                self.loadingView?.stopAnimating()
            default:
                break
            }
        }
    }
    
    override func prepare() {
        super.prepare()
        self.mj_h = 50
        
        self.loadingView = UIActivityIndicatorView(style: .white)
        self.addSubview(self.loadingView!)
        
        self.stateLabel = UILabel(frame: CGRect(x: 0, y: 0, width: 300, height: 40))
        self.stateLabel?.textAlignment = .center
        self.stateLabel?.font = UIFont.systemFont(ofSize: 12.0)
        self.addSubview(self.stateLabel!)
        
        self.noMoreDataStateString = "没有更多数据了"
        
        self.themeChangedHandler = { [weak self] (style) in
            guard let self = self else {
                return
            }
            if V2EXColor.instance.style == V2EXColor.V2EXColorStyleDefault {
                self.loadingView?.style = .gray
                self.stateLabel?.textColor = UIColor(white: 0, alpha: 0.3)
            } else {
                self.loadingView?.style = .white
                self.stateLabel?.textColor = UIColor(white: 1, alpha: 0.3)
            }
        }
    }
    
    override func placeSubviews() {
        super.placeSubviews()
        self.loadingView?.center = CGPoint(x: self.mj_w / 2, y: self.mj_h / 2 + self.centerOffset)
        self.stateLabel?.center = CGPoint(x: self.mj_w / 2, y: self.mj_h / 2 + self.centerOffset)
    }
    
}

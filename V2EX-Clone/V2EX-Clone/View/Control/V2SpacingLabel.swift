//
//  V2SpacingLabel.swift
//  V2EX-Clone
//
//  Created by yidao on 2019/9/18.
//  Copyright © 2019 yidao. All rights reserved.
//

import UIKit

class V2SpacingLabel: UILabel {
    var spacing: CGFloat = 3.0
    override var text: String? {
        set {
            if let len = newValue?.count, len > 0 {
                let attributedString = NSMutableAttributedString(string: newValue!);
                let paragraphStyle = NSMutableParagraphStyle();
                paragraphStyle.lineBreakMode=NSLineBreakMode.byTruncatingTail;
                paragraphStyle.lineSpacing=self.spacing;
                paragraphStyle.alignment=self.textAlignment;
                attributedString.addAttributes(
                    [
                        NSAttributedString.Key.paragraphStyle:paragraphStyle
                    ],
                    range: NSMakeRange(0, newValue!.count));
                super.attributedText = attributedString;
            }
        }
        get {
            return super.text
        }
    }
}

//
//  RightNodeTableViewCell.swift
//  V2EX-Clone
//
//  Created by yidao on 2019/9/11.
//  Copyright © 2019 yidao. All rights reserved.
//

import UIKit

class RightNodeTableViewCell: UITableViewCell {

    lazy var nodeNameLabel: UILabel = {
        let label = UILabel()
        label.font = UIFont.systemFont(ofSize: 15.0)
        return label
    }()
    
    var panel = UIView()
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        setup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func setup() {
        self.selectionStyle = .none
        self.backgroundColor = .clear
        
        contentView.addSubview(panel)
        panel.snp.makeConstraints { (make) in
            make.left.right.top.equalToSuperview()
            make.bottom.equalToSuperview().offset(-1 * SEPARATOR_HEIGHT)
        }
        
        panel.addSubview(nodeNameLabel)
        nodeNameLabel.snp.makeConstraints { (make) in
            make.right.equalToSuperview().offset(-22)
            make.centerY.equalToSuperview()
        }
        
        self.themeChangedHandler = { [weak self] style in
            guard let self = self else { return }
            self.nodeNameLabel.textColor = V2EXColor.colors.v2_leftNodeTintColor
        }
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        self.refreshBackgroundColor()
    }
    
    func refreshBackgroundColor() {
        if self.isSelected {
            panel.backgroundColor = V2EXColor.colors.v2_leftNodeBackgroundHighLightColor
        } else {
            panel.backgroundColor = V2EXColor.colors.v2_leftNodeBackgroundColor
        }
    }
}

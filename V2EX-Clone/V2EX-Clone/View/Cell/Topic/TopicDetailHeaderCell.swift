//
//  TopicDetailHeaderCell.swift
//  V2EX-Clone
//
//  Created by yidao on 2019/9/27.
//  Copyright © 2019 yidao. All rights reserved.
//

import UIKit

class TopicDetailHeaderCell: UITableViewCell {
    lazy var avatarImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.contentMode = .scaleAspectFit
        imageView.layer.cornerRadius = 3
        imageView.layer.masksToBounds = true
        return imageView
    }()
    
    lazy var userNameLabel: UILabel = {
        let label = UILabel()
        label.textColor = V2EXColor.colors.v2_topicListUserNameColor
        label.font = UIFont.systemFont(ofSize: 14.0)
        return label
    }()
    
    lazy var dateAndLastPostUserLabel: UILabel = {
        let label = UILabel()
        label.textColor = V2EXColor.colors.v2_topicListDateColor
        label.font = UIFont.systemFont(ofSize: 12.0)
        return label
    }()

    var nodeNameLabel: UILabel = {
        let label = UILabel();
        label.textColor = V2EXColor.colors.v2_topicListDateColor
        label.font = UIFont.systemFont(ofSize: 11.0)
        label.backgroundColor = V2EXColor.colors.v2_nodeBackgroundColor
        label.layer.cornerRadius = 2
        label.clipsToBounds = true
        label.isUserInteractionEnabled = true
        return label
    }()
    
    /// 帖子标题
    var topicTitleLabel: UILabel = {
        let label = V2SpacingLabel();
        label.textColor = V2EXColor.colors.v2_topicListTitleColor
        label.font = UIFont.systemFont(ofSize: 17)
        label.numberOfLines = 0
        label.preferredMaxLayoutWidth = kScreenWidth - 24
        return label
    }()
    
    /// 装上面定义的那些元素的容器
    var contentPanel:UIView = {
        let view = UIView()
        view.backgroundColor = V2EXColor.colors.v2_cellWhiteBackgroundColor
        return view
    }()
    
    weak var itemModel:TopicDetailModel?
    var nodeClickHandler:(() -> Void)?
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        self.setup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func setup() {
        self.selectionStyle = .none
        self.backgroundColor = V2EXColor.colors.v2_backgroundColor
        
        self.contentView.addSubview(self.contentPanel)
        self.contentPanel.addSubview(self.avatarImageView)
        self.contentPanel.addSubview(self.userNameLabel)
        self.contentPanel.addSubview(self.dateAndLastPostUserLabel)
        self.contentPanel.addSubview(self.nodeNameLabel)
        self.contentPanel.addSubview(self.topicTitleLabel)

        self.setupLayout()
        
        //点击用户头像，跳转到用户主页
        self.avatarImageView.isUserInteractionEnabled = true
        self.userNameLabel.isUserInteractionEnabled = true
        var userNameTap = UITapGestureRecognizer(target: self, action: #selector(TopicDetailHeaderCell.userNameTap(_:)))
        self.avatarImageView.addGestureRecognizer(userNameTap)
        userNameTap = UITapGestureRecognizer(target: self, action: #selector(TopicDetailHeaderCell.userNameTap(_:)))
        self.userNameLabel.addGestureRecognizer(userNameTap)
        self.nodeNameLabel.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(nodeClick)))
    }
    
    fileprivate func setupLayout(){
        self.avatarImageView.snp.makeConstraints{ (make) -> Void in
            make.left.top.equalTo(self.contentPanel).offset(12);
            make.width.height.equalTo(35);
        }
        self.userNameLabel.snp.makeConstraints{ (make) -> Void in
            make.left.equalTo(self.avatarImageView.snp.right).offset(10);
            make.top.equalTo(self.avatarImageView);
        }
        self.dateAndLastPostUserLabel.snp.makeConstraints{ (make) -> Void in
            make.bottom.equalTo(self.avatarImageView);
            make.left.equalTo(self.userNameLabel);
        }
        self.nodeNameLabel.snp.makeConstraints{ (make) -> Void in
            make.centerY.equalTo(self.userNameLabel);
            make.right.equalTo(self.contentPanel.snp.right).offset(-10)
            make.bottom.equalTo(self.userNameLabel).offset(1);
            make.top.equalTo(self.userNameLabel).offset(-1);
        }
        self.topicTitleLabel.snp.makeConstraints { (make) in
            make.top.equalTo(self.avatarImageView.snp.bottom).offset(12);
            make.left.equalTo(self.avatarImageView);
            make.right.equalTo(self.contentPanel).offset(-12);
        }
        self.contentPanel.snp.makeConstraints{ (make) -> Void in
            make.top.left.right.equalTo(self.contentView)
            make.bottom.equalTo(self.topicTitleLabel.snp.bottom).offset(12)
            make.bottom.equalTo(self.contentView).offset(SEPARATOR_HEIGHT * -1)
        }
    }
    
    @objc func nodeClick() {
        nodeClickHandler?()
    }
    
    @objc func userNameTap(_ sender:UITapGestureRecognizer) {
        if let _ = self.itemModel , let username = itemModel?.userName {
            let memberViewController = MemberViewController()
            memberViewController.username = username
            V2Client.instance.centerNavigation?.pushViewController(memberViewController, animated: true)
        }
    }
    
    func bind(_ model: TopicDetailModel) {
        self.itemModel = model
        
        self.userNameLabel.text = model.userName
        self.dateAndLastPostUserLabel.text = model.date
        self.topicTitleLabel.text = model.topicTitle
        
        if let avatar = model.avata {
            self.avatarImageView.fin_setImageWithUrl(URL(string: "https:" + avatar)!, placeholderImage: nil, imageModificationClosure: fin_defaultImageModification())
        }
        
        if let node = model.nodeName {
            self.nodeNameLabel.text = "  " + node + "  "
        }
    }
        
}

//
//  LeftUserHeadCell.swift
//  V2EX-Clone
//
//  Created by yidao on 2019/9/11.
//  Copyright © 2019 yidao. All rights reserved.
//

import UIKit
import Kingfisher

class LeftUserHeadCell: UITableViewCell {
    lazy var avatarImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.backgroundColor = UIColor(white: 0.9, alpha: 0.3)
        imageView.layer.borderWidth = 1.5
        imageView.layer.borderColor = UIColor(white: 1, alpha: 0.6).cgColor
        imageView.layer.masksToBounds = true
        imageView.layer.cornerRadius = 38
        return imageView
    }()
    /// 用户名
    lazy var userNameLabel: UILabel = {
        let label = UILabel()
        label.font = UIFont.systemFont(ofSize: 16.0)
        return label
    }()
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        setup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func setup() {
        self.backgroundColor = UIColor.clear
        self.selectionStyle = .none
        
        contentView.addSubview(avatarImageView)
        contentView.addSubview(userNameLabel)
        
        avatarImageView.snp.makeConstraints { (make) in
            make.centerX.equalTo(contentView)
            make.centerY.equalTo(contentView).offset(-8)
            make.width.height.equalTo(avatarImageView.layer.cornerRadius * 2)
        }
        
        userNameLabel.snp.makeConstraints { (make) in
            make.top.equalTo(avatarImageView.snp.bottom).offset(10)
            make.centerX.equalTo(self.avatarImageView)
        }
        
        self.kvoController.observe(V2User.sharedInstance, keyPath: "username", options: [.initial, .new]) { [weak self] (observe, observer, change) in
            guard let self = self else { return }
            self.userNameLabel.text = V2User.sharedInstance.username ?? "请先登录"
            if let avatar = V2User.sharedInstance.user?.avatar_large {
                self.avatarImageView.kf.setImage(with: URL(string: "https:" + avatar)!, placeholder: nil, options: nil, progressBlock: nil, completionHandler: { (image, error, cacheType, imageURL) in
                    if !V2User.sharedInstance.isLogin {
                        self.avatarImageView.image = nil
                    }
                })
            } else {
                self.avatarImageView.image = nil
            }
        }
        
        self.themeChangedHandler = { [weak self] (style) in
            guard let self = self else { return }
            self.userNameLabel.textColor = V2EXColor.colors.v2_topicListUserNameColor
        }
    }
    
}

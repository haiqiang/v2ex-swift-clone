//
//  LeftNodeTableViewCell.swift
//  V2EX-Clone
//
//  Created by yidao on 2019/9/11.
//  Copyright © 2019 yidao. All rights reserved.
//

import UIKit

class LeftNodeTableViewCell: UITableViewCell {
    var nodeImageView: UIImageView = UIImageView()
    lazy var nodeNameLabel: UILabel = {
        let label =  UILabel()
        label.font = UIFont.systemFont(ofSize: 16.0)
        return label
    }()
    var panel = UIView()
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        setup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func setup() {
        self.selectionStyle = .none
        self.backgroundColor = .clear
        
        contentView.addSubview(panel)
        panel.addSubview(nodeImageView)
        panel.addSubview(nodeNameLabel)
        
        panel.snp.makeConstraints { (make) in
            make.left.right.top.equalToSuperview()
            make.height.equalTo(55)
        }
        
        nodeImageView.snp.makeConstraints{ (make) -> Void in
            make.centerY.equalTo(panel)
            make.left.equalTo(panel).offset(20)
            make.width.height.equalTo(25)
        }
        nodeNameLabel.snp.makeConstraints{ (make) -> Void in
            make.left.equalTo(nodeImageView.snp.right).offset(20)
            make.centerY.equalTo(nodeImageView)
        }
        
        self.themeChangedHandler = { [weak self] (style) in
            guard let self = self else { return }
            self.configureColor()
        }
    }
    
    func configureColor() {
        self.panel.backgroundColor = V2EXColor.colors.v2_leftNodeBackgroundColor
        self.nodeImageView.tintColor =  V2EXColor.colors.v2_leftNodeTintColor
        self.nodeNameLabel.textColor = V2EXColor.colors.v2_leftNodeTintColor
    }
}

class LeftNotificationCell: LeftNodeTableViewCell {
    lazy var notificationCountLabel:UILabel = {
        let label = UILabel()
        label.font = UIFont.systemFont(ofSize: 10.0)
        label.textColor = UIColor.white
        label.layer.cornerRadius = 7
        label.layer.masksToBounds = true
        label.backgroundColor = V2EXColor.colors.v2_noticePointColor
        return label
    }()
    
    override func setup() {
        super.setup()
        nodeNameLabel.text = "notifications".localized
        
        contentView.addSubview(notificationCountLabel)
        notificationCountLabel.snp.makeConstraints{ (make) -> Void in
            make.centerY.equalTo(nodeNameLabel)
            make.left.equalTo(nodeNameLabel.snp.right).offset(5)
            make.height.equalTo(14)
        }
        
        self.kvoController.observe(V2User.sharedInstance, keyPath: "notificationCount", options: [.initial,.new]) {  [weak self](cell, clien, change) -> Void in
            guard let self = self else { return }
            if V2User.sharedInstance.notificationCount > 0 {
                self.notificationCountLabel.text = "   \(V2User.sharedInstance.notificationCount)   "
            }
            else{
                self.notificationCountLabel.text = ""
            }
        }
    }
}

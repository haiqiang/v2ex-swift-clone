//
//  NodeTableViewCell.swift
//  V2EX-Clone
//
//  Created by yidao on 2019/9/23.
//  Copyright © 2019 yidao. All rights reserved.
//

import UIKit

class NodeTableViewCell: UICollectionViewCell {
    lazy var textLabel: UILabel = {
        let l = UILabel()
        l.font = UIFont.systemFont(ofSize: 15.0)
        l.textColor = V2EXColor.colors.v2_topicListUserNameColor
        l.backgroundColor = V2EXColor.colors.v2_cellWhiteBackgroundColor
        return l
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.contentView.backgroundColor = V2EXColor.colors.v2_cellWhiteBackgroundColor
        self.contentView.addSubview(self.textLabel)
        
        self.textLabel.snp.makeConstraints { (make) in
            make.center.equalToSuperview()
        }
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    
}

//
//  SettingsTableViewController.swift
//  V2EX-Clone
//
//  Created by yidao on 2019/9/23.
//  Copyright © 2019 yidao. All rights reserved.
//

import UIKit

class SettingsTableViewController: UITableViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        self.title = "viewOptions".localized
        self.tableView.separatorStyle = .none
        
        self.tableView.register(BaseDetailTableViewCell.self, forCellReuseIdentifier: "BaseDetailTableViewCell")
        self.tableView.register(FontSizeSliderTableViewCell.self, forCellReuseIdentifier: "FontSizeSliderTableViewCell")
        self.tableView.register(FontDisplayTableViewCell.self, forCellReuseIdentifier: "FontDisplayTableViewCell")
        
        self.themeChangedHandler = { [weak self] (style) in
            guard let self = self else { return }
            self.view.backgroundColor = V2EXColor.colors.v2_backgroundColor
            self.tableView.reloadData()
        }
    }

}

extension SettingsTableViewController {
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return [2, 3][section]
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.section == 0 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "BaseDetailTableViewCell") as! BaseDetailTableViewCell
            cell.titleLabel.text = ["default".localized, "dark".localized][indexPath.row]
            if V2EXColor.instance.style == V2EXColor.V2EXColorStyleDefault {
                cell.detailLabel.text = ["current".localized ,""][indexPath.row]
            }
            else{
                cell.detailLabel.text = ["", "current".localized][indexPath.row]
            }
            return cell
        }
            
        else {
            if indexPath.row == 0 {
                let cell = tableView.dequeueReusableCell(withIdentifier: "FontSizeSliderTableViewCell") as! FontSizeSliderTableViewCell
                return cell
            }
            else if indexPath.row == 1 {
                let cell = tableView.dequeueReusableCell(withIdentifier: "BaseDetailTableViewCell") as! BaseDetailTableViewCell
                cell.backgroundColor = V2EXColor.colors.v2_backgroundColor
                cell.detailMarkHidden = true
                return cell
            }
            else{
                let cell = tableView.dequeueReusableCell(withIdentifier: "FontDisplayTableViewCell") as! FontDisplayTableViewCell
                return cell
            }
        }
    }
}

extension SettingsTableViewController {
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.section == 0 {
            V2EXColor.instance.setStyleAndSave([V2EXColor.V2EXColorStyleDefault,V2EXColor.V2EXColorStyleDark][indexPath.row])
        }
    }
    
    override func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 44
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.section == 0 {
            return 44
        }
        else {
            return [70,25,185][indexPath.row]
        }
    }
    
    override func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let label = UILabel()
        label.text = ["viewOptionThemeSet".localized
            , "viewOptionTextSize".localized
            ][section]
        label.textColor = V2EXColor.colors.v2_topicListUserNameColor
        label.font = UIFont.systemFont(ofSize: 12.0)
        label.backgroundColor = V2EXColor.colors.v2_backgroundColor
        return label
    }
}

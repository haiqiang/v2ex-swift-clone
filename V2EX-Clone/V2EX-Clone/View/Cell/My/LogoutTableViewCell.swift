//
//  LogoutTableViewCell.swift
//  V2EX-Clone
//
//  Created by yidao on 2019/9/19.
//  Copyright © 2019 yidao. All rights reserved.
//

import UIKit

class LogoutTableViewCell: UITableViewCell {

    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        self.setup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func setup() {
        self.backgroundColor = V2EXColor.colors.v2_cellWhiteBackgroundColor
        
        self.textLabel!.text = "logOut".localized
        self.textLabel!.textAlignment = .center
        self.textLabel!.textColor = V2EXColor.colors.v2_noticePointColor
        
        let separator = UIImageView(image: UIColor.createImageByColor(V2EXColor.colors.v2_separatorColor))
        self.contentView.addSubview(separator)
        separator.snp.makeConstraints{ (make) -> Void in
            make.left.equalTo(self.contentView)
            make.right.bottom.equalTo(self.contentView)
            make.height.equalTo(SEPARATOR_HEIGHT)
        }
    }
}

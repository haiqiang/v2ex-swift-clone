//
//  MemberTopicCell.swift
//  V2EX-Clone
//
//  Created by yidao on 2019/9/18.
//  Copyright © 2019 yidao. All rights reserved.
//

import UIKit

class MemberTopicCell: UITableViewCell {
    /// 日期 和 最后发送人
    private var dateAndLastPostUserLabel: UILabel = {
        let dateAndLastPostUserLabel = UILabel()
        dateAndLastPostUserLabel.textColor = V2EXColor.colors.v2_topicListDateColor
        dateAndLastPostUserLabel.font = UIFont.systemFont(ofSize: 12)
        return dateAndLastPostUserLabel
    }()
    /// 评论数量
    private var replyCountLabel: UILabel = {
        let replyCountLabel = UILabel()
        replyCountLabel.textColor = V2EXColor.colors.v2_topicListDateColor
        replyCountLabel.font = UIFont.systemFont(ofSize: 12)
        return replyCountLabel
    }()
    
    private var replyCountIconImageView: UIImageView = {
        let replyCountIconImageView = UIImageView(image: UIImage(named: "reply_n"))
        replyCountIconImageView.contentMode = .scaleAspectFit
        return replyCountIconImageView
    }()
    
    /// 节点
    private var nodeNameLabel: UILabel = {
        let nodeNameLabel = UILabel();
        nodeNameLabel.textColor = V2EXColor.colors.v2_topicListDateColor
        nodeNameLabel.font = UIFont.systemFont(ofSize: 11)
        nodeNameLabel.backgroundColor = V2EXColor.colors.v2_nodeBackgroundColor
        nodeNameLabel.layer.cornerRadius = 2
        nodeNameLabel.clipsToBounds = true
        return nodeNameLabel
    }()
    /// 帖子标题
    private var topicTitleLabel: UILabel = {
        let topicTitleLabel = V2SpacingLabel()
        topicTitleLabel.textColor = V2EXColor.colors.v2_topicListTitleColor
        topicTitleLabel.font = UIFont.systemFont(ofSize: 15)
        topicTitleLabel.numberOfLines = 0
        topicTitleLabel.preferredMaxLayoutWidth = kScreenWidth - 24
        return topicTitleLabel
    }()
    
    /// 装上面定义的那些元素的容器
    private var contentPanel:UIView = {
        let contentPanel = UIView();
        contentPanel.backgroundColor =  V2EXColor.colors.v2_cellWhiteBackgroundColor
        return contentPanel
    }()
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        setup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func setup() {
        self.selectionStyle = .none
        self.backgroundColor = V2EXColor.colors.v2_backgroundColor
        
        self.contentView .addSubview(self.contentPanel);
        self.contentPanel.addSubview(self.dateAndLastPostUserLabel);
        self.contentPanel.addSubview(self.replyCountLabel);
        self.contentPanel.addSubview(self.replyCountIconImageView);
        self.contentPanel.addSubview(self.nodeNameLabel)
        self.contentPanel.addSubview(self.topicTitleLabel);
        
        setupLayout()
        
        self.dateAndLastPostUserLabel.backgroundColor = self.contentPanel.backgroundColor
        self.replyCountLabel.backgroundColor = self.contentPanel.backgroundColor
        self.replyCountIconImageView.backgroundColor = self.contentPanel.backgroundColor
        self.topicTitleLabel.backgroundColor = self.contentPanel.backgroundColor
    }
    
    func setupLayout() {
        self.contentPanel.snp.makeConstraints{ (make) in
            make.top.left.right.equalToSuperview()
        }
        self.dateAndLastPostUserLabel.snp.makeConstraints{ (make) in
            make.top.equalTo(self.contentPanel).offset(12)
            make.left.equalTo(self.contentPanel).offset(12)
        }
        self.replyCountLabel.snp.makeConstraints{ (make) in
            make.centerY.equalTo(self.dateAndLastPostUserLabel)
            make.right.equalTo(self.contentPanel).offset(-12)
        }
        self.replyCountIconImageView.snp.makeConstraints{ (make) in
            make.centerY.equalTo(self.replyCountLabel)
            make.width.height.equalTo(18)
            make.right.equalTo(self.replyCountLabel.snp.left).offset(-2)
        }
        self.nodeNameLabel.snp.makeConstraints{ (make) in
            make.centerY.equalTo(self.replyCountLabel)
            make.right.equalTo(self.replyCountIconImageView.snp.left).offset(-4)
            make.bottom.equalTo(self.replyCountLabel).offset(1)
            make.top.equalTo(self.replyCountLabel).offset(-1)
        }
        self.topicTitleLabel.snp.makeConstraints{ (make) in
            make.top.equalTo(self.dateAndLastPostUserLabel.snp.bottom).offset(12);
            make.left.equalTo(self.dateAndLastPostUserLabel)
            make.right.equalTo(self.contentPanel).offset(-12)
        }
        self.contentPanel.snp.makeConstraints{ (make) in
            make.bottom.equalTo(self.topicTitleLabel.snp.bottom).offset(12)
            make.bottom.equalTo(self.contentView).offset(SEPARATOR_HEIGHT * -1)
        }
    }
    
    func bind(_ model: MemberTopicsModel) {
        self.dateAndLastPostUserLabel.text = model.date
        self.topicTitleLabel.text = model.topicTitle
        
        self.replyCountLabel.text = model.replies
        if let node = model.nodeName{
            self.nodeNameLabel.text = "  " + node + "  "
        }
    }
}

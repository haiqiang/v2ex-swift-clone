//
//  NodeCollectionReusableView.swift
//  V2EX-Clone
//
//  Created by yidao on 2019/9/23.
//  Copyright © 2019 yidao. All rights reserved.
//

import UIKit

class NodeCollectionReusableView: UICollectionReusableView {
    lazy var label: UILabel = {
       let _label = UILabel()
        _label.font = UIFont.systemFont(ofSize: 16.0)
        _label.textColor = V2EXColor.colors.v2_topicListTitleColor
        _label.backgroundColor = V2EXColor.colors.v2_backgroundColor
        return _label
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.backgroundColor = V2EXColor.colors.v2_backgroundColor
        self.addSubview(self.label)
        
        self.label.snp.makeConstraints { (make) in
            make.centerY.equalToSuperview()
            make.left.equalToSuperview().offset(15.0)
        }
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    
}

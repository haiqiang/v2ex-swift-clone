//
//  MemberHeaderCell.swift
//  V2EX-Clone
//
//  Created by yidao on 2019/9/18.
//  Copyright © 2019 yidao. All rights reserved.
//

import UIKit
import Kingfisher

class MemberHeaderCell: UITableViewCell {
    
    private let avatarRadius: CGFloat = 38.0
    
    private lazy var avatarImageView: UIImageView = {
       let imgV = UIImageView()
       imgV.backgroundColor = UIColor(white: 0.9, alpha: 0.3)
       imgV.layer.borderWidth = 1.5
       imgV.layer.borderColor = UIColor(white: 1, alpha: 0.6).cgColor
       imgV.layer.masksToBounds = true
       imgV.layer.cornerRadius = avatarRadius
       return imgV
    }()
    
    private lazy var userNameLabel: UILabel = {
        let useLabel = UILabel()
        useLabel.textColor = UIColor(white: 0.85, alpha: 1)
        useLabel.font = UIFont.systemFont(ofSize: 16.0)
        useLabel.text = "Hello"
        return useLabel
    }()
    
    private lazy var introduceLabel: UILabel = {
        let introLabel = UILabel()
        introLabel.textColor = UIColor(white: 0.75, alpha: 1)
        introLabel.font = UIFont.systemFont(ofSize: 16.0)
        introLabel.numberOfLines = 2
        introLabel.textAlignment = .center
        return introLabel
    }()
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        setup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func setup() {
        self.backgroundColor = .clear
        self.selectionStyle = .none
        
        contentView.addSubview(self.avatarImageView)
        contentView.addSubview(self.userNameLabel)
        contentView.addSubview(self.introduceLabel)
        
        setupLayout()
    }
    
    private func setupLayout() {
        self.avatarImageView.snp.makeConstraints { (make) in
            make.centerX.equalToSuperview()
            make.centerY.equalToSuperview().offset(-15)
            make.width.height.equalTo(avatarRadius * 2)
        }
        
        self.userNameLabel.snp.makeConstraints { (make) in
            make.top.equalTo(self.avatarImageView.snp_bottom).offset(10)
            make.centerX.equalTo(self.avatarImageView)
        }
        
        self.introduceLabel.snp.makeConstraints { (make) in
            make.top.equalTo(self.userNameLabel.snp_bottom).offset(5)
            make.centerX.equalTo(self.avatarImageView)
            make.left.equalToSuperview().offset(15)
            make.right.equalToSuperview().offset(-15)
        }
    }
    
    func bind(_ model: MemberModel?) {
        if let model = model {
            if let avatar = model.avata {
                self.avatarImageView.kf.setImage(with: URL(string: "https:" + avatar))
            }
            self.userNameLabel.text = model.userName
            self.introduceLabel.text = model.introduce
        }
    }
}

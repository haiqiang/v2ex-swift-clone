//
//  BaseDetailTableViewCell.swift
//  V2EX-Clone
//
//  Created by yidao on 2019/9/19.
//  Copyright © 2019 yidao. All rights reserved.
//

import UIKit

class BaseDetailTableViewCell: UITableViewCell {
    lazy var titleLabel: UILabel = {
       let label = UILabel()
        label.font = UIFont.systemFont(ofSize: 16)
        return label
    }()
    
    lazy var detailLabel:UILabel = {
        let label = UILabel()
        label.font = UIFont.systemFont(ofSize: 13)
        return label
    }()
    
    lazy var detailMarkImageView:UIImageView = {
        let imageview = UIImageView(image: UIImage(named: "ic_keyboard_arrow_right"))
        imageview.contentMode = .center
        return imageview
    }()
    
    var separator:UIImageView = UIImageView()
    
    var detailMarkHidden: Bool {
        get {
            return self.detailMarkImageView.isHidden
        }
        set {
            if self.detailMarkImageView.isHidden == newValue {
                return
            }
            
            self.detailMarkImageView.isHidden = newValue
            if newValue {
                self.detailMarkImageView.snp.remakeConstraints { (make) in
                    make.width.height.equalTo(0)
                    make.centerY.equalTo(self.contentView)
                    make.right.equalTo(self.contentView).offset(-12)
                }
            } else {
                self.detailMarkImageView.snp.remakeConstraints { (make) in
                    make.width.height.equalTo(20)
                    make.centerY.equalTo(self.contentView)
                    make.right.equalTo(self.contentView).offset(-12)
                }
            }
        }
    }
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        setup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func setup() {
        let selectedBackgroundView = UIView()
        self.selectedBackgroundView = selectedBackgroundView
        
        self.contentView.addSubview(self.titleLabel)
        self.contentView.addSubview(self.detailMarkImageView);
        self.contentView.addSubview(self.detailLabel)
        self.contentView.addSubview(self.separator)
        
        
        self.titleLabel.snp.makeConstraints{ (make) -> Void in
            make.left.equalTo(self.contentView).offset(12)
            make.centerY.equalTo(self.contentView)
        }
        self.detailMarkImageView.snp.makeConstraints { (make) -> Void in
            make.height.equalTo(24)
            make.width.equalTo(14)
            make.centerY.equalTo(self.contentView)
            make.right.equalTo(self.contentView).offset(-12)
        }
        self.detailLabel.snp.makeConstraints{ (make) -> Void in
            make.right.equalTo(self.detailMarkImageView.snp.left).offset(-5)
            make.centerY.equalTo(self.contentView)
        }
        self.separator.snp.makeConstraints{ (make) -> Void in
            make.left.right.bottom.equalTo(self.contentView)
            make.height.equalTo(SEPARATOR_HEIGHT)
        }
        
        
        self.themeChangedHandler = {[weak self] (style) -> Void in
            self?.backgroundColor = V2EXColor.colors.v2_cellWhiteBackgroundColor
            self?.selectedBackgroundView!.backgroundColor = V2EXColor.colors.v2_backgroundColor
            self?.titleLabel.textColor = V2EXColor.colors.v2_topicListTitleColor
            self?.detailMarkImageView.tintColor = self?.titleLabel.textColor
            self?.detailLabel.textColor = V2EXColor.colors.v2_topicListUserNameColor
            self?.separator.image = UIColor.createImageByColor(V2EXColor.colors.v2_separatorColor)
        }
    }
}

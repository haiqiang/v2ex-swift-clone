//
//  AccountListTableViewCell.swift
//  V2EX-Clone
//
//  Created by yidao on 2019/9/19.
//  Copyright © 2019 yidao. All rights reserved.
//

import UIKit

class AccountListTableViewCell: UITableViewCell {
    private lazy var avatarImageView:UIImageView = {
        let avatarImageView = UIImageView()
        avatarImageView.backgroundColor = UIColor(white: 0.9, alpha: 0.3)
        avatarImageView.layer.masksToBounds = true
        avatarImageView.layer.cornerRadius = 22
        return avatarImageView
    }()
    private lazy var userNameLabel:UILabel = {
        let userNameLabel = UILabel()
        userNameLabel.textColor = V2EXColor.colors.v2_topicListUserNameColor
        userNameLabel.font = UIFont.systemFont(ofSize: 14.0)
        return userNameLabel
    }()
    private lazy var usedLabel:UILabel = {
        let usedLabel = UILabel()
        usedLabel.textColor = V2EXColor.colors.v2_noticePointColor
        usedLabel.font = UIFont.systemFont(ofSize: 11.0)
        usedLabel.text = "current".localized
        return usedLabel
    }()
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
       
        setup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    func setup() {
        self.selectionStyle = .none
        self.backgroundColor = V2EXColor.colors.v2_cellWhiteBackgroundColor
        
        self.contentView.addSubview(self.avatarImageView)
        self.contentView.addSubview(self.userNameLabel)
        self.contentView.addSubview(self.usedLabel)
        let separator = UIImageView(image: UIColor.createImageByColor(V2EXColor.colors.v2_separatorColor))
        self.contentView.addSubview(separator)
        
        self.usedLabel.isHidden = true;
        
        self.avatarImageView.snp.makeConstraints{ (make) -> Void in
            make.left.equalTo(self.contentView).offset(15)
            make.centerY.equalTo(self.contentView)
            make.width.height.equalTo(self.avatarImageView.layer.cornerRadius * 2)
        }
        self.userNameLabel.snp.makeConstraints{ (make) -> Void in
            make.left.equalTo(self.avatarImageView.snp.right).offset(15)
            make.centerY.equalTo(self.avatarImageView)
        }
        self.usedLabel.snp.makeConstraints{ (make) -> Void in
            make.right.equalTo(self.contentView).offset(-15)
            make.centerY.equalTo(self.avatarImageView)
        }
        separator.snp.makeConstraints{ (make) -> Void in
            make.left.equalTo(self.avatarImageView.snp.right).offset(5)
            make.right.bottom.equalTo(self.contentView)
            make.height.equalTo(SEPARATOR_HEIGHT)
        }
    }
    
    func bind(_ model:LocalSecurityAccountModel) {
        self.userNameLabel.text = model.username
        if let avatar = model.avatar , let url = URL(string: avatar) {
            self.avatarImageView.kf.setImage(with: url)
        }
        if V2User.sharedInstance.username == model.username {
            self.usedLabel.isHidden = false
        }
        else {
            self.usedLabel.isHidden = true
        }
    }
}

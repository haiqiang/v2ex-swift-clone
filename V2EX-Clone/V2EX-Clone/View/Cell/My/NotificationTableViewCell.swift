//
//  NotificationTableViewCell.swift
//  V2EX-Clone
//
//  Created by yidao on 2019/9/24.
//  Copyright © 2019 yidao. All rights reserved.
//

import UIKit

class NotificationTableViewCell: UITableViewCell {
    /// 头像
    var avatarImageView: UIImageView = {
        let imageView =  UIImageView()
        imageView.contentMode = .scaleAspectFit
        imageView.layer.cornerRadius = 3
        imageView.layer.masksToBounds = true
        imageView.backgroundColor = .random
        return imageView
    }()
    /// 用户名
    var userNameLabel: UILabel = {
        let label = UILabel()
        label.textColor = V2EXColor.colors.v2_topicListUserNameColor
        label.font = UIFont.systemFont(ofSize: 14.0)
        return label
    }()
    /// 日期
    var dateLabel: UILabel = {
        let label = UILabel()
        label.textColor=V2EXColor.colors.v2_topicListDateColor
        label.font = UIFont.systemFont(ofSize:12)
        return label
    }()
    
    /// 操作描述
    var detailLabel: UILabel = {
        let label = V2SpacingLabel()
        label.textColor=V2EXColor.colors.v2_topicListTitleColor
        label.font = UIFont.systemFont(ofSize:14)
        label.numberOfLines = 0
        label.preferredMaxLayoutWidth = kScreenWidth - 24
        return label
    }()
    
    /// 回复正文
    var commentLabel: UILabel = {
        let label = V2SpacingLabel();
        label.textColor=V2EXColor.colors.v2_topicListTitleColor
        label.font = UIFont.systemFont(ofSize: 14)
        label.numberOfLines=0
        label.preferredMaxLayoutWidth = kScreenWidth - 24
        return label
    }()
    
    /// 回复正文的背景容器
    var commentPanel: UIView = {
        let view = UIView()
        view.layer.cornerRadius = 3
        view.layer.masksToBounds = true
        view.backgroundColor = V2EXColor.colors.v2_backgroundColor
        return view
    }()
    
    lazy var dropUpImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.image = UIImage(named: "ic_arrow_drop_up")?.withRenderingMode(.alwaysTemplate)
        imageView.contentMode = .scaleAspectFit
        imageView.tintColor = self.commentPanel.backgroundColor
        return imageView
    }()
    
    /// 整个cell元素的容器
    var contentPanel:UIView = {
        let view = UIView()
        view.backgroundColor = V2EXColor.colors.v2_cellWhiteBackgroundColor
        view.clipsToBounds = true
        return view
    }()
    
    /// 回复按钮
    var replyButton:UIButton = {
        let button = UIButton(type: .custom)
        button.setTitle("reply".localized, for: .normal)
        button.layer.masksToBounds = true
        button.layer.cornerRadius = 3.0
        button.backgroundColor  = V2EXColor.colors.v2_buttonBackgroundColor
        button.titleLabel!.font = UIFont.systemFont(ofSize: 14.0)
        button.setTitleColor(.white, for: .normal)
        return button
    }()
    
    weak var itemModel: NotificationsModel?
    
    var replyButtonClickHandler: ((UIButton) -> Void)?
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        setup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    
    func setup() {
        self.backgroundColor=V2EXColor.colors.v2_backgroundColor;
        let selectedBackgroundView = UIView()
        selectedBackgroundView.backgroundColor = V2EXColor.colors.v2_backgroundColor
        self.selectedBackgroundView = selectedBackgroundView
        
        self.contentView .addSubview(self.contentPanel);
        self.contentPanel.addSubview(self.avatarImageView);
        self.contentPanel.addSubview(self.userNameLabel);
        self.contentPanel.addSubview(self.dateLabel);
        self.contentPanel.addSubview(self.detailLabel);
        self.contentPanel.addSubview(self.commentPanel);
        self.contentPanel.addSubview(self.commentLabel);
        self.contentPanel.addSubview(self.dropUpImageView)
        self.contentPanel.addSubview(self.replyButton)
        
        self.setupLayout()
        
        //点击用户头像，跳转到用户主页
        self.avatarImageView.isUserInteractionEnabled = true
        self.userNameLabel.isUserInteractionEnabled = true
        var userNameTap = UITapGestureRecognizer(target: self, action: #selector(self.userNameTap(_:)))
        self.avatarImageView.addGestureRecognizer(userNameTap)
        userNameTap = UITapGestureRecognizer(target: self, action: #selector(self.userNameTap(_:)))
        self.userNameLabel.addGestureRecognizer(userNameTap)
        
        //按钮点击事件
        self.replyButton.addTarget(self, action: #selector(self.replyButtonClick(_:)), for: .touchUpInside)
    }
    
    @objc func userNameTap(_ sender: UITapGestureRecognizer) {
        if let _ = self.itemModel, let username = itemModel?.userName {
            let memberVC = MemberViewController()
            memberVC.username = username
            V2Client.instance.centerNavigation?.pushViewController(memberVC, animated: true)
        }
    }
    
    @objc func replyButtonClick(_ sender: UIButton) {
        self.replyButtonClickHandler?(sender)
    }
    
    func bind(_ model: NotificationsModel) {
        self.itemModel = model
        
        self.userNameLabel.text = model.userName
        self.dateLabel.text = model.date
        self.detailLabel.text = model.title
        if let text = model.reply {
            self.commentLabel.text = text
            self.setCommentPanelHidden(false)
        }
        else {
            self.setCommentPanelHidden(true)
        }
        
        if let avata = model.avata {
            self.avatarImageView.kf.setImage(with:  URL(string: "https:" + avata)!)
        }
    }
    
    func setCommentPanelHidden(_ hidden: Bool) {
        if hidden {
            self.commentPanel.isHidden = true
            self.dropUpImageView.isHidden = true
            self.commentLabel.text = ""
            self.contentPanel.snp.remakeConstraints{ (make) -> Void in
                make.bottom.equalTo(self.detailLabel.snp.bottom).offset(12);
                make.top.left.right.equalTo(self.contentView);
                make.bottom.equalTo(self.contentView).offset(SEPARATOR_HEIGHT * -1)
            }
        }
        else{
            self.commentPanel.isHidden = false
            self.dropUpImageView.isHidden = false
            self.contentPanel.snp.remakeConstraints{ (make) -> Void in
                make.bottom.equalTo(self.commentPanel.snp.bottom).offset(12);
                make.top.left.right.equalTo(self.contentView);
                make.bottom.equalTo(self.contentView).offset(SEPARATOR_HEIGHT * -1)
            }
        }
    }
    
    func setupLayout() {
        
        self.avatarImageView.snp.makeConstraints{ (make) -> Void in
            make.left.top.equalTo(self.contentView).offset(12);
            make.width.height.equalTo(35);
        }
        self.userNameLabel.snp.makeConstraints{ (make) -> Void in
            make.left.equalTo(self.avatarImageView.snp.right).offset(10);
            make.top.equalTo(self.avatarImageView);
        }
        self.dateLabel.snp.makeConstraints{ (make) -> Void in
            make.bottom.equalTo(self.avatarImageView);
            make.left.equalTo(self.userNameLabel);
        }
        self.detailLabel.snp.makeConstraints{ (make) -> Void in
            make.top.equalTo(self.avatarImageView.snp.bottom).offset(12);
            make.left.equalTo(self.avatarImageView);
            make.right.equalTo(self.contentPanel).offset(-12);
        }
        self.commentLabel.snp.makeConstraints{ (make) -> Void in
            make.top.equalTo(self.detailLabel.snp.bottom).offset(20);
            make.left.equalTo(self.contentPanel).offset(22);
            make.right.equalTo(self.contentPanel).offset(-22);
        }
        self.commentPanel.snp.makeConstraints{ (make) -> Void in
            make.top.left.equalTo(self.commentLabel).offset(-10)
            make.right.bottom.equalTo(self.commentLabel).offset(10)
        }
        self.dropUpImageView.snp.makeConstraints{ (make) -> Void in
            make.bottom.equalTo(self.commentPanel.snp.top)
            make.left.equalTo(self.commentPanel).offset(25)
            make.width.equalTo(10)
            make.height.equalTo(5)
        }
        self.replyButton.snp.makeConstraints{ (make) -> Void in
            make.centerY.equalTo(self.avatarImageView)
            make.right.equalTo(self.contentPanel).offset(-12)
            make.width.equalTo(50)
            make.height.equalTo(25)
        }
        
        
    }
}

//
//  V2WebViewViewController.swift
//  V2EX-Clone
//
//  Created by yidao on 2019/9/24.
//  Copyright © 2019 yidao. All rights reserved.
//

import UIKit
import WebKit

class V2WebViewViewController: UIViewController {
    private var url: String = ""
    
    init(url: String) {
        super.init(nibName: nil, bundle: nil)
        self.url = url
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

//
//  WritingViewController.swift
//  V2EX-Clone
//
//  Created by yidao on 2019/9/24.
//  Copyright © 2019 yidao. All rights reserved.
//

import UIKit
import YYText

class WritingViewController: UIViewController {

    var textView: YYTextView?
    var topicModel: TopicDetailModel?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "写东西"
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(title: "取消", style: .plain, target: self, action: #selector(WritingViewController.leftClick))
        
        let rightButton = UIButton(frame: CGRect(x: 0, y: 0, width: 40, height: 40))
        rightButton.contentMode = .center
        rightButton.imageEdgeInsets = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: -20)
        rightButton.setImage(UIImage(named: "ic_send")!.withRenderingMode(.alwaysTemplate), for: .normal)
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(customView: rightButton)
        rightButton.addTarget(self, action: #selector(self.rightClick), for: .touchUpInside)
        
        self.view.backgroundColor = V2EXColor.colors.v2_backgroundColor
        self.textView = YYTextView()
        self.textView!.scrollsToTop = false
        self.textView!.backgroundColor = V2EXColor.colors.v2_textViewBackgroundColor
        self.textView!.font = UIFont.systemFont(ofSize: 18.0)
        self.textView!.delegate = self
        self.textView!.textColor = V2EXColor.colors.v2_topicListUserNameColor
        self.textView!.textParser = V2EXMentionedBindingParser()
        textView!.textContainerInset = UIEdgeInsets(top: 10, left: 10, bottom: 10, right: 10)
        textView?.keyboardDismissMode = .interactive
        self.view.addSubview(self.textView!)
        self.textView!.snp.makeConstraints{ (make) -> Void in
            make.top.right.bottom.left.equalTo(self.view)
        }
        
    }
    
    @objc func leftClick (){
        self.dismiss(animated: true, completion: nil)
    }
    
    @objc func rightClick (){
        
    }
}

extension WritingViewController: YYTextViewDelegate {
    func textViewDidChange(_ textView: YYTextView) {
        if textView.text.count <= 0 {
            textView.textColor = V2EXColor.colors.v2_topicListUserNameColor
        }
    }
}

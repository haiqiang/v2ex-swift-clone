//
//  RightViewController.swift
//  V2EX-Clone
//
//  Created by yidao on 2019/9/11.
//  Copyright © 2019 yidao. All rights reserved.
//

import UIKit
import FXBlurView

class RightViewController: UIViewController {

     let rightNodes = [rightNodeModel(nodeName: "tech".localized, nodeTab: "tech"),
                              rightNodeModel(nodeName: "creative".localized, nodeTab: "creative"),
                              rightNodeModel(nodeName: "play".localized, nodeTab: "play"),
                              rightNodeModel(nodeName: "apple".localized, nodeTab: "apple"),
                              rightNodeModel(nodeName: "jobs".localized, nodeTab: "jobs"),
                              rightNodeModel(nodeName: "deals".localized, nodeTab: "deals"),
                              rightNodeModel(nodeName: "city".localized, nodeTab: "city"),
                              rightNodeModel(nodeName: "qna".localized, nodeTab: "qna"),
                              rightNodeModel(nodeName: "hot".localized, nodeTab: "hot"),
                              rightNodeModel(nodeName: "all".localized, nodeTab: "all"),
                              rightNodeModel(nodeName: "r2".localized, nodeTab: "r2"),
                              rightNodeModel(nodeName: "nodes".localized, nodeTab: "nodes"),
                              rightNodeModel(nodeName: "members".localized, nodeTab: "members"),
    ]
    
    var currentSelectedTabIndex = 0
    var backgroundImageView: UIImageView? = UIImageView()
    var frostedView = FXBlurView()
    
    private lazy var tableView: UITableView = {
       let table = UITableView()
        table.backgroundColor = UIColor.clear
        table.estimatedRowHeight = 100
        table.separatorStyle = .none
        
        table.register(RightNodeTableViewCell.self, forCellReuseIdentifier: "RightNodeTableViewCell")
        
        table.delegate = self
        table.dataSource = self
        
        return table
    }()
        
        
    override func viewDidLoad() {
        super.viewDidLoad()
        
        view.backgroundColor = V2EXColor.colors.v2_backgroundColor
        
        var currentTab = V2EXSettings.instance[kHomeTab]
        if currentTab == nil {
            currentTab = "all"
        }
        
        for (index, model) in rightNodes.enumerated() {
            if model.nodeName == currentTab {
                self.currentSelectedTabIndex = index
                break
            }
        }
        
        backgroundImageView?.frame = self.view.frame
        backgroundImageView?.contentMode = .left
        view.addSubview(backgroundImageView!)
        
        frostedView.underlyingView = backgroundImageView
        frostedView.isDynamic = false
        frostedView.frame = self.view.frame
        frostedView.tintColor = UIColor.black
        view.addSubview(frostedView)
        
        view.addSubview(tableView)
        tableView.snp.makeConstraints { (make) in
            make.edges.equalToSuperview()
        }
        
        self.themeChangedHandler = {[weak self] (style) -> Void in
            guard let self = self else { return }
            if V2EXColor.instance.style == V2EXColor.V2EXColorStyleDefault {
                self.backgroundImageView?.image = UIImage(named: "32.jpg")
            }
            else{
                self.backgroundImageView?.image = UIImage(named: "12.jpg")
            }
            self.frostedView.updateAsynchronously(true, completion: nil)
        }
        
        
        let rowHeight = self.tableView(self.tableView, heightForRowAt: IndexPath(row: 0, section: 0))
        let rowCount = self.tableView(self.tableView, numberOfRowsInSection: 0)
        var paddingTop = (kScreenHeight - CGFloat(rowCount) * rowHeight) / 2
        if paddingTop <= 0 {
            paddingTop = 20
        }
        
        self.tableView.contentInset = UIEdgeInsets(top: paddingTop, left: 0, bottom: 0, right: 0)
    }
    
    func maximumRightDrawerWidth() -> CGFloat {
        let cell = RightNodeTableViewCell()
        let cellFont = UIFont(name: cell.nodeNameLabel.font.familyName, size: cell.nodeNameLabel.font.pointSize)
        for node in rightNodes {
            let size = node.nodeName!.boundingRect(with: CGSize(width: CGFloat(MAXFLOAT), height: CGFloat(MAXFLOAT)),
                                                   options: NSStringDrawingOptions.usesLineFragmentOrigin,
                                                   attributes: [NSAttributedString.Key(rawValue: "NSFontAttributeName"):cellFont!],
                                                   context: nil)
            let width = size.width + 50
            if width > 100 {
                return width
            }
        }
        return 100
    }
    
}

extension RightViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 48.0
    }
}

extension RightViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return rightNodes.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "RightNodeTableViewCell") as! RightNodeTableViewCell
        cell.nodeNameLabel.text = self.rightNodes[indexPath.row].nodeName
        
        if indexPath.row == self.currentSelectedTabIndex && cell.isSelected == false {
            self.tableView.selectRow(at: indexPath, animated: false, scrollPosition: .middle)
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let node = self.rightNodes[indexPath.row]
        V2Client.instance.centerViewController?.tab = node.nodeTab
        V2Client.instance.centerViewController?.refreshPage()
        V2Client.instance.drawerController?.closeDrawer(animated: true, completion: nil)
    }
    
}

struct rightNodeModel: Codable {
    var nodeName: String?
    var nodeTab: String?
}

//
//  HomeViewController.swift
//  V2EX-Clone
//
//  Created by yidao on 2019/9/11.
//  Copyright © 2019 yidao. All rights reserved.
//

import UIKit

let kHomeTab = "me.fin.homeTab"

class HomeViewController: UIViewController {
    var topicList:Array<TopicListModel>?
    
    var tab: String? {
        didSet {
            var name = "全部"
            for model in RightViewController().rightNodes {
                if model.nodeTab == tab {
                    name = model.nodeName ?? ""
                    break
                }
            }
            self.title = name
        }
    }
    
    var currentPage = 0
    
    private lazy var tableView: UITableView = {
        let table = UITableView()
        table.cancelEstimatedHeight()
        table.separatorStyle = .none
        
        table.register(HomeTopicListTableViewCell.self, forCellReuseIdentifier: "HomeTopicListTableViewCell")
        
        table.delegate = self
        table.dataSource = self
        
        return table
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tab = V2EXSettings.instance[kHomeTab]
        view.backgroundColor = UIColor.white
        setupNavigationItem()
        
        //监听程序即将进入前台运行、进入后台休眠 事件
        NotificationCenter.default.addObserver(self, selector: #selector(self.applicationWillEnterForeground), name: UIApplication.willEnterForegroundNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.applicationDidEnterBackground), name: UIApplication.didEnterBackgroundNotification, object: nil)
        
        self.view.addSubview(self.tableView)
        self.tableView.snp.makeConstraints { (make) in
            make.edges.equalToSuperview()
        }
        
        self.tableView.mj_header = V2RefreshHeader(refreshingBlock: { [weak self] in
            guard let self = self else { return }
            self.refresh()
        })
        self.refreshPage()
        
        let footer = V2RefreshFooter(refreshingBlock: {[weak self] () -> Void in
            guard let self = self else { return }
            self.getNextPage()
        })
        footer?.centerOffset = -4
        self.tableView.mj_footer = footer
        
        self.themeChangedHandler = {[weak self] (style) -> Void in
            guard let self = self else { return }
            self.tableView.backgroundColor = V2EXColor.colors.v2_backgroundColor
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        V2Client.instance.drawerController?.openDrawerGestureModeMask = .panningCenterView
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        V2Client.instance.drawerController?.openDrawerGestureModeMask = []
    }
    
    func setupNavigationItem() {
        let leftButton = NotificationMenuButton()
        leftButton.frame = CGRect(x: 0, y: 0, width: 40, height: 40)
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(customView: leftButton)
        leftButton.addTarget(self, action: #selector(self.leftClick), for: .touchUpInside)
        
        
        let rightButton = UIButton(frame: CGRect(x: 0, y: 0, width: 40, height: 40))
        rightButton.contentMode = .center
        rightButton.imageEdgeInsets = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: -15)
        rightButton.setImage(UIImage(named: "ic_more_horiz_36pt"), for: .normal)
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(customView: rightButton)
        rightButton.addTarget(self, action: #selector(self.rightClick), for: .touchUpInside)
    }
    
    @objc func leftClick() {
        V2Client.instance.drawerController?.toggleLeftDrawerSide(animated: true, completion: nil)
    }
    
    @objc func rightClick() {
        V2Client.instance.drawerController?.toggleRightDrawerSide(animated: true, completion: nil)
    }
    
    func refreshPage() {
        self.tableView.mj_header.beginRefreshing()
        V2EXSettings.instance[kHomeTab] = tab
    }
    
    func refresh() {
        //如果有上拉加载更多 正在执行，则取消它
        if self.tableView.mj_footer.isRefreshing {
            self.tableView.mj_footer.endRefreshing()
        }
        
        //根据 tab name 获取帖子列表
        _ = TopicListAPI.provider
            .requestAPI(.topicList(tab: tab, page: 0))
            .mapResponseToJiArray(TopicListModel.self)
            .subscribe(onNext: { (response) in
                self.topicList = response
                self.tableView.reloadData()
                
                //判断标签是否能加载下一页, 不能就提示下
                let refreshFooter = self.tableView.mj_footer as! V2RefreshFooter
                if self.tab == nil || self.tab == "all" {
                    refreshFooter.noMoreDataStateString = nil
                    refreshFooter.resetNoMoreData()
                }
                else{
                    refreshFooter.noMoreDataStateString = "没更多帖子了,只有【\("all".localized)】标签能翻页"
                    refreshFooter.endRefreshingWithNoMoreData()
                }
                
                //重置page
                self.currentPage = 0
                self.tableView.mj_header.endRefreshing()
                
            }, onError: { (error) in
                if let err = error as? APIError {
                    switch err {
                    case .needs2FA:
                        V2Client.instance.centerViewController!.navigationController?.present(TwoFAViewController(), animated: true, completion: nil);
                    default:
                        V2ProgrssHUD.error(err.rawString())
                    }
                }
                else {
                    V2ProgrssHUD.error(error.rawString())
                }
                self.tableView.mj_header.endRefreshing()
            })
    }
    
    func getNextPage() {
        if let count = self.topicList?.count , count <= 0{
            self.tableView.mj_footer.endRefreshing()
            return;
        }
        
        //根据 tab name 获取帖子列表
        self.currentPage += 1
        _ = TopicListAPI.provider
            .requestAPI(.topicList(tab: tab, page: self.currentPage))
            .mapResponseToJiArray(TopicListModel.self)
            .subscribe(onNext: { (response) in
                if response.count > 0 {
                    self.topicList? += response
                    self.tableView.reloadData()
                }
                self.tableView.mj_footer.endRefreshing()
            }, onError: { (error) in
                self.currentPage -= 1
                V2ProgrssHUD.error(error.rawString())
                self.tableView.mj_footer.endRefreshing()
            })
    }
    
    static var lastLeaveTime = Date()
    @objc func applicationWillEnterForeground(){
        //计算上次离开的时间与当前时间差
        //如果超过2分钟，则自动刷新本页面。
        let interval = -1 * HomeViewController.lastLeaveTime.timeIntervalSinceNow
        if interval > 120 {
            self.tableView.mj_header.beginRefreshing()
        }
    }
    @objc func applicationDidEnterBackground(){
        HomeViewController.lastLeaveTime = Date()
    }
    
}

extension HomeViewController: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        let item = self.topicList![indexPath.row]
        let titleHeight = item.topicTitleLayout?.textBoundingRect.size.height ?? 0
        //          上间隔   头像高度  头像下间隔       标题高度    标题下间隔 cell间隔
        let height = 12    +  35     +  12      + titleHeight   + 12      + 8
        
        return height
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        let item = self.topicList![indexPath.row]
        
        if let id = item.topicId {
            let topicDetailController = TopicDetailViewController()
            topicDetailController.topicId = id
            self.navigationController?.pushViewController(topicDetailController, animated: true)
        }
    }
}

extension HomeViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if let list = self.topicList {
            return list.count
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "HomeTopicListTableViewCell") as! HomeTopicListTableViewCell
        cell.bind(self.topicList![indexPath.row])
        return cell
    }
}

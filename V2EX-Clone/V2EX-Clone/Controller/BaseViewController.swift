//
//  BaseViewController.swift
//  V2EX-Clone
//
//  Created by yidao on 2019/9/6.
//  Copyright © 2019 yidao. All rights reserved.
//

import UIKit

class BaseViewController: UIViewController {
    private weak var _loadView: V2LoadingView?
    
    func showLoadingView() {
        self.hideLoadingView()
        
        let loadView = V2LoadingView()
        loadView.backgroundColor = self.view.backgroundColor
        self.view.addSubview(loadView)
        loadView.snp.makeConstraints { make in
            make.edges.equalToSuperview()
        }
        self._loadView = loadView
    }
    
    func hideLoadingView() {
        self._loadView?.removeFromSuperview()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }

}

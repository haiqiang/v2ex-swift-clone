//
//  TopicDetailViewController.swift
//  V2EX-Clone
//
//  Created by yidao on 2019/9/26.
//  Copyright © 2019 yidao. All rights reserved.
//

import UIKit

class TopicDetailViewController: BaseViewController {
    var topicId = "0"
    var currentPage = 1
    
    private var model: TopicDetailModel?
    private var commentsArray: [TopicCommentModel] = []
    private var webViewContentCell: TopicDetailWebViewContentCell?
    
    private enum Order {
        case asc
        case desc
    }
    
    private var commentSort = Order.asc
    
    private lazy var tableview: UITableView = {
        let table = UITableView()
        table.cancelEstimatedHeight()
        table.separatorStyle = .none
        
        table.backgroundColor = V2EXColor.colors.v2_backgroundColor
        table.register(TopicDetailHeaderCell.self, forCellReuseIdentifier: "TopicDetailHeaderCell")
        table.register(TopicDetailWebViewContentCell.self, forCellReuseIdentifier: "TopicDetailWebViewContentCell")
        table.register(TopicDetailCommentCell.self, forCellReuseIdentifier: "TopicDetailCommentCell")
        table.register(BaseDetailTableViewCell.self, forCellReuseIdentifier: "BaseDetailTableViewCell")
        table.register(TopicDetailToolCell.self, forCellReuseIdentifier: "TopicDetailToolCell")
        
        table.delegate = self
        table.dataSource = self
        
        return table
    }()
    
    // 忽略帖子成功后，调用的闭包
    var ignoreTopicHandler: ((String) -> Void)?
    // t如果activityView释放了，这里也一起释放
    private weak var activityView: V2ActivityViewController?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.view.backgroundColor = V2EXColor.colors.v2_backgroundColor
        self.title = "postDetails".localized
        self.view.addSubview(self.tableview)
        self.tableview.snp.makeConstraints { (make) in
            make.edges.equalToSuperview()
        }
        createRightBtn()
        
        self.showLoadingView()
        self.getData()
        
        self.tableview.mj_header = V2RefreshHeader(refreshingBlock: {[weak self] in
            guard let self = self else { return }
            self.getData()
        })
        self.tableview.mj_footer = V2RefreshFooter(refreshingBlock: {[weak self] in
            guard let self = self else { return }
            self.getNextPage()
        })
    }

    func getData() {
        TopicDetailModel.getTopicDetailById(self.topicId) { (response) in
            if response.success {
                if let aModel = response.value!.0 {
                    self.model = aModel
                }
                self.commentSort = .asc
                self.commentsArray = response.value!.1
                self.currentPage = 1
                //
                self.webViewContentCell = nil
                self.tableview.reloadData()
            } else {
                V2ProgrssHUD.error(response.message)
            }
            
            if self.tableview.mj_header.isRefreshing {
                self.tableview.mj_header.endRefreshing()
            }
            self.tableview.mj_footer.resetNoMoreData()
            self.hideLoadingView()
        }
    }
    
    func getNextPage() {
        if self.model == nil {
            self.endRefreshingWithNoDataAtAll()
            return
        }
        
        if self.commentSort == .asc {
            self.currentPage += 1
        } else {
            self.currentPage -= 1
        }
        
        if self.model == nil || self.currentPage > self.model!.commentTotalPages || self.currentPage <= 0 {
            self.endRefreshWithNoMoreData()
        }
        
        TopicDetailModel.getTopicCommentsById(self.topicId, page: self.currentPage) { (response) in
            if response.success {
                if self.needsClearOldComments {
                    self.needsClearOldComments = false
                    self.commentsArray.removeAll()
                }
                var arr = response.value!
                if self.commentSort == .desc {
                    arr = arr.reversed()
                    if self.currentPage == 0 {
                        self.endRefreshWithNoMoreData()
                    }
                } else {
                    if self.currentPage == self.model?.commentTotalPages {
                        self.endRefreshWithNoMoreData()
                    }
                }
                self.commentsArray += arr
                self.tableview.reloadData()
                self.tableview.mj_footer.endRefreshing()
            } else {
                self.currentPage -= 1
            }
        }
        
    }
    
    var needsClearOldComments = false
    
    func toggleSoryType() {
        guard self.model != nil && !self.tableview.mj_footer.isRefreshing && !self.tableview.mj_header.isRefreshing else {
            return
        }
        self.commentSort = self.commentSort == .asc ? .desc : .asc
        if self.commentSort == .asc {
            self.currentPage = 0
        } else {
            self.currentPage = self.model!.commentTotalPages + 1
        }
        self.tableview.mj_footer.resetNoMoreData()
        needsClearOldComments = true
        self.getNextPage()
    }
    
    @objc func reply() {
        self.activityView?.dismissVC()
        
        V2User.sharedInstance.ensureLoginWithHandler {
            let replyViewController = ReplyViewController()
            replyViewController.topicModel = self.model!
            let nav = V2EXNavigationController(rootViewController:replyViewController)
            self.navigationController?.present(nav, animated: true, completion:nil)
        }
    }
}

extension TopicDetailViewController {
    private func createRightBtn() {
        let rightBtn = UIButton(frame: CGRect(x: 0, y: 0, width: 40, height: 40))
        rightBtn.contentMode = .center
        rightBtn.imageEdgeInsets = .init(top: 0, left: 0, bottom: 0, right: -15)
        rightBtn.setImage(UIImage(named: "ic_more_horiz_36pt")?.withRenderingMode(.alwaysTemplate), for: .normal)
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(customView: rightBtn)
        rightBtn.addTarget(self, action: #selector(self.rightClick), for: .touchUpInside)
    }
    
    @objc private func rightClick() {
        if self.model == nil { return }
        let activityView = V2ActivityViewController()
        activityView.modalPresentationStyle = .fullScreen
        activityView.dataSource = self
        self.navigationController!.present(activityView, animated: true, completion: nil)
        self.activityView = activityView
    }
    
    func nodeClick() {
        let node = NodeModel()
        node.nodeId = self.model?.node
        node.nodeName = self.model?.nodeName
        let controller = NodeTopicListViewController()
        controller.node = node
        self.navigationController?.pushViewController(controller, animated: true)
        
    }
}

// MARK: - 底部无数据展示
extension TopicDetailViewController {
    func endRefreshingWithStateString(_ string: String) {
        (self.tableview.mj_footer as! V2RefreshFooter).noMoreDataStateString = string
        self.tableview.mj_footer.endRefreshingWithNoMoreData()
    }
    
    func endRefreshingWithNoDataAtAll() {
        self.endRefreshingWithStateString("暂无评论")
    }
    
    func endRefreshWithNoMoreData() {
        self.endRefreshingWithStateString("没有更多评论了")
    }
}

enum V2ActivityViewTopDetailAction: Int {
    case block = 0, favorite, grade, share, explore
}

extension TopicDetailViewController: V2ActivityViewDataSource {
    func V2ActivityView(_ activityView: V2ActivityViewController, numberOfCellsInSection section: Int) -> Int {
        return 5
    }
    
    func V2ActivityView(_ activityView: V2ActivityViewController, ActivityAtIndexPath indexPath: IndexPath) -> V2Activity {
        return V2Activity(title: ["ignore".localized, "favorite".localized, "thank".localized, "share".localized, "Safari".localized][indexPath.row], image: UIImage(named: ["ic_block_48pt","ic_grade_48pt","ic_favorite_48pt","ic_share_48pt","ic_explore_48pt"][indexPath.row])!)
    }
    
    func V2ActivityView(_ activityView: V2ActivityViewController, heightForFooterInSection section: Int) -> CGFloat {
        return 45
    }
    
    func V2ActivityView(_ activityView: V2ActivityViewController, viewForFooterInSection section: Int) -> UIView? {
        let view = UIView()
        view.backgroundColor = V2EXColor.colors.v2_buttonBackgroundColor
        
        let label = UILabel()
        label.font = UIFont.systemFont(ofSize: 18.0)
        label.text = "reply2".localized
        label.textAlignment = .center
        label.textColor = UIColor.white
        view.addSubview(label)
        label.snp.makeConstraints{ (make) -> Void in
            make.top.right.bottom.left.equalTo(view)
        }
        
        view.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(self.reply)))
    
        return view
    }
    
    func V2ActivityView(_ activityView: V2ActivityViewController, didSelectRowAtIndexPath indexPath: IndexPath) {
        activityView.dismissVC()
        
        let action = V2ActivityViewTopDetailAction(rawValue: indexPath.row)
        
        guard V2User.sharedInstance.isLogin || action == V2ActivityViewTopDetailAction.explore || action == V2ActivityViewTopDetailAction.share else {
            V2ProgrssHUD.show("请先登录")
            return
        }
        
      
        
        switch action {
            case .block:
                if let topicId = self.model?.topicId {
                    TopicDetailModel.ignoreTopicWithTopicId(topicId) { [weak self] (response) in
                        guard let self = self else {
                            return
                        }
                        
                        if response.success {
                            V2ProgrssHUD.success("忽略成功")
                            self.navigationController?.popViewController(animated: true)
                            self.ignoreTopicHandler?(topicId)
                        } else {
                            V2ProgrssHUD.error("忽略失败")
                        }
                    }
                }
            case .favorite:
                if let topicId = self.model?.topicId, let token = self.model?.token {
                    TopicDetailModel.favoriteTopicWithTopicId(topicId, token: token) { (response) in
                        if response.success {
                            V2ProgrssHUD.success("收藏成功")
                        } else {
                            V2ProgrssHUD.error("收藏失败")
                        }
                    }
                }
            case .grade:
                V2User.sharedInstance.getOnce { (response) in
                    if response.success, let once = V2User.sharedInstance.once {
                        TopicDetailModel.topicThankWithTopicId(self.model?.topicId ?? "", token: once) { (response) in
                            if response.success {
                                V2ProgrssHUD.success("成功送了一波铜币")
                            } else {
                                V2ProgrssHUD.error(response.message)
                            }
                        }
                    } else {
                        V2ProgrssHUD.error("收藏失败")
                    }
                }
            case .share:
                let shareUrl = NSURL.init(string: V2EXURL + "t/" + self.model!.topicId!)
                let shareArr: NSArray = [shareUrl!]
                let activityController: UIActivityViewController = UIActivityViewController(activityItems: shareArr as! [Any], applicationActivities: nil)
                activityController.excludedActivityTypes = [.airDrop]
                self.navigationController?.present(activityController, animated: true, completion: nil)
            case .explore:
                UIApplication.shared.open(URL(string: V2EXURL + "t/" + self.model!.topicId!)!, options: [:], completionHandler: nil)
            
        
            default:
                       break
        }
    }
    
}



// MARK: - UITableView DataSource
enum TopicDetailTableViewSection: Int {
    case header = 0, comment, other
}

enum TopicDetailHeaderComponent: Int {
    case title = 0, webViewContent, other
}

extension TopicDetailViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        let _section = TopicDetailTableViewSection(rawValue: indexPath.section)!
        var _headerComponent = TopicDetailHeaderComponent.other
        if let headerComponent = TopicDetailHeaderComponent(rawValue: indexPath.row) {
            _headerComponent = headerComponent
        }
        switch _section {
        case .header:
            switch _headerComponent {
            case .title:
                return tableView.fin_heightForCellWithIdentifier(TopicDetailHeaderCell.self, indexpath: indexPath) { (cell) in
                    cell.bind(self.model!)
                }
            case .webViewContent:
                if let height =  self.webViewContentCell?.contentHeight , height > 0 {
                    return self.webViewContentCell!.contentHeight
                }
                else {
                    return 1
                }
            case .other:
                return 45
            }
        case .comment:
            let layout = self.commentsArray[indexPath.row].textLayout!
            return layout.textBoundingRect.size.height + 1 + 12 + 35 + 12 + 12 + 1
        case .other:
            return 200
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        
        if indexPath.section == 1 {
            let sheet = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
            let reply = UIAlertAction(title: "回复", style: .default) { (_) in
                let item = self.commentsArray[indexPath.row]
                let replyVC = ReplyViewController()
                replyVC.atSomeone = "@" + item.userName! + " "
                replyVC.topicModel = self.model!
                let nav = V2EXNavigationController(rootViewController: replyVC)
                self.navigationController?.present(nav, animated: true, completion: nil)
            }
            sheet.addAction(reply)
            
            let thank = UIAlertAction(title: "感谢", style: .default) { (_) in
                guard V2User.sharedInstance.isLogin else {
                    V2ProgrssHUD.show("请先登录")
                    return
                }
                let item = self.commentsArray[indexPath.row]
                if item.replyId == nil {
                    V2ProgrssHUD.error("回复replyId为空")
                    return
                }
                if self.model?.token == nil {
                    V2ProgrssHUD.error("帖子token为空")
                    return
                }
                item.favorites += 1
                self.tableview.reloadRows(at: [IndexPath(row: indexPath.row, section: 1)], with: .none)
                V2User.sharedInstance.getOnce { (response) -> Void in
                    if response.success , let once = V2User.sharedInstance.once {
                        TopicCommentModel.replyThankWithReplyId(item.replyId!, token:once ) {
                            [weak item, weak self](response) in
                            if response.success {
                            }
                            else{
                                V2ProgrssHUD.error("感谢失败了")
                                //失败后 取消增加的数量
                                item?.favorites -= 1
                                self?.tableview.reloadRows(at: [IndexPath(row: indexPath.row, section: 1)], with: .none)
                            }
                        }
                    }
                    else{
                        V2ProgrssHUD.error("获取 once 失败")
                    }
                }
            }
            sheet.addAction(thank)
            
            let dialog = UIAlertAction(title: "查看对话", style: .default) { (_) in
                let item = self.commentsArray[indexPath.row]
                let sourceArr = self.commentSort == .asc ? self.commentsArray : self.commentsArray.reversed()
                let relevantComments = TopicCommentModel.getRelevantCommentsInArray(sourceArr, firstComment: item)
                if relevantComments.count <= 0 {
                   return;
                }

                let controller = RelevantCommentsNav(comments: relevantComments)
                controller.modalPresentationStyle = .fullScreen
                self.present(controller, animated: true, completion: nil)
            }
            sheet.addAction(dialog)
            self.present(sheet, animated: true, completion: nil)
        }
    }
}

extension TopicDetailViewController: UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
           return 2
       }
    
       func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
           let _section = TopicDetailTableViewSection(rawValue: section)!
           switch _section {
           case .header:
               if self.model != nil{
                   return 3
               }
               else{
                   return 0
               }
           case .comment:
               return self.commentsArray.count;
           case .other:
               return 0;
           }
       }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let _section = TopicDetailTableViewSection(rawValue: indexPath.section)!
        var _headerComponent = TopicDetailHeaderComponent.other
        if let headerComponent = TopicDetailHeaderComponent(rawValue: indexPath.row) {
            _headerComponent = headerComponent
        }

        switch _section {
        case .header:
            switch _headerComponent {
            case .title:
                //帖子标题
                let cell = tableView.dequeueReusableCell(withIdentifier: "TopicDetailHeaderCell") as! TopicDetailHeaderCell
                if(cell.nodeClickHandler == nil){
                    cell.nodeClickHandler = {[weak self] () -> Void in
                        self?.nodeClick()
                    }
                }
                cell.bind(self.model!);
                return cell;
            case .webViewContent:
                //帖子内容
                if self.webViewContentCell == nil {
                    self.webViewContentCell = (tableView.dequeueReusableCell(withIdentifier: "TopicDetailWebViewContentCell") as! TopicDetailWebViewContentCell)
                    self.webViewContentCell?.parentScrollView = self.tableview
                }
                else {
                    return self.webViewContentCell!
                }
                self.webViewContentCell!.load(self.model!);
                if self.webViewContentCell!.contentHeightChanged == nil {
                    self.webViewContentCell!.contentHeightChanged = { [weak self] (height:CGFloat) -> Void  in
                        if let weakSelf = self {
                            //在cell显示在屏幕时更新，否则会崩溃会崩溃会崩溃
                            //另外刷新清空旧cell,重新创建这个cell ,所以 contentHeightChanged 需要判断cell是否为nil
                            if let cell = weakSelf.webViewContentCell, weakSelf.tableview.visibleCells.contains(cell) {
                                if let height = weakSelf.webViewContentCell?.contentHeight, height > 1.5 * kScreenHeight{ //太长了就别动画了。。
                                    UIView.animate(withDuration: 0, animations: { () -> Void in
                                        self?.tableview.beginUpdates()
                                        self?.tableview.endUpdates()
                                    })
                                }
                                else {
                                    self?.tableview.beginUpdates()
                                    self?.tableview.endUpdates()
                                }
                            }
                        }
                    }
                }
                return self.webViewContentCell!
            case .other:
                let cell = tableView.dequeueReusableCell(withIdentifier: "TopicDetailToolCell") as! TopicDetailToolCell
                cell.titleLabel.text = self.model?.topicCommentTotalCount
                cell.sortButton.setTitle(self.commentSort == .asc ? "顺序查看" : "倒序查看", for: .normal)
                if cell.sortButtonClick == nil {
                    cell.sortButtonClick = {[weak self] sender in
                        sender.setTitle("正在加载", for: .normal)
                        self?.toggleSoryType()
                    }
                }
                return cell
            }
        case .comment:
            let cell = tableView.dequeueReusableCell(withIdentifier: "TopicDetailCommentCell") as! TopicDetailCommentCell
            let comment = self.commentsArray[indexPath.row]
            cell.bind(comment)
            cell.isAuthor = self.model?.userName == comment.userName
            return cell
        case .other:
            return UITableViewCell();
        }    }
    
    
}

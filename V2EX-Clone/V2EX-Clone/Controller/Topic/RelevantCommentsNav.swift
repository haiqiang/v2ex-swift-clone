//
//  RelevantCommentsNav.swift
//  V2EX-Clone
//
//  Created by yidao on 2019/10/24.
//  Copyright © 2019 yidao. All rights reserved.
//

import UIKit
import FXBlurView

class RelevantCommentsNav: V2EXNavigationController {
    override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?) {
        super.init(nibName: "", bundle: nil)
    }
    
    init(comments: [TopicCommentModel]) {
        let viewController = RelevantCommentsViewController()
        viewController.commentsArray = comments
        super.init(rootViewController: viewController)
        self.transitioningDelegate = self
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

extension RelevantCommentsNav: UIViewControllerTransitioningDelegate {
    func animationController(forPresented presented: UIViewController, presenting: UIViewController, source: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        return RelevantCommentsViewControllerTransionPresent()
    }
}

class RelevantCommentsViewControllerTransionPresent:NSObject, UIViewControllerAnimatedTransitioning {
    func transitionDuration(using transitionContext: UIViewControllerContextTransitioning?) -> TimeInterval {
        return 0.3
    }
    
    func animateTransition(using transitionContext: UIViewControllerContextTransitioning) {
        let toVC = transitionContext.viewController(forKey: .to) as! RelevantCommentsNav
        let container = transitionContext.containerView
        container.addSubview(toVC.view)
        toVC.view.alpha = 0
        
        UIView.animate(withDuration: transitionDuration(using: transitionContext), delay: 0, options: .curveEaseIn, animations: {
            toVC.view.alpha = 1
        }) { (finished) in
            transitionContext.completeTransition(!transitionContext.transitionWasCancelled)
        }
    }
}

class RelevantCommentsViewController: UIViewController  {
    var commentsArray:[TopicCommentModel] = []

    private var dismissing = false
    private lazy var tableView: UITableView = {
        let tableView = UITableView()
        tableView.cancelEstimatedHeight()
        tableView.separatorStyle = .none
        if #available(iOS 11.0, *) {
            tableView.contentInsetAdjustmentBehavior = .never
        }
        tableView.backgroundColor = .clear
        tableView.contentInset = UIEdgeInsets(top: 64, left: 0, bottom: 0, right: 0)
        tableView.register(TopicDetailCommentCell.self, forCellReuseIdentifier: "TopicDetailCommentCell")
        
        tableView.delegate = self
        tableView.dataSource = self
        
        return tableView
    }()
    
    var frostedView = FXBlurView()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        frostedView.underlyingView = V2Client.instance.centerNavigation!.view
        frostedView.isDynamic = false
        frostedView.blurRadius = 35
        frostedView.tintColor = UIColor.black
        frostedView.frame = self.view.frame
        self.view.addSubview(frostedView)
        
        
        self.view.addSubview(self.tableView);
        self.tableView.snp.makeConstraints{ (make) -> Void in
            make.left.right.equalTo(self.view);
            make.height.equalTo(self.view)
            make.top.equalTo(self.view.snp.bottom)
        }
        
      
        let label = UILabel(frame: CGRect(x: 0, y: 0, width: 80, height: 44))
        self.view.addSubview(label)

        label.snp.makeConstraints { (make) in
            make.topMargin.equalToSuperview().offset(20)
            make.centerX.equalToSuperview()
        }
        
        label.text = "下拉关闭查看"
        label.font = UIFont(name: "HelveticaNeue-Light", size: 12)
        if V2EXColor.instance.style == V2EXColor.V2EXColorStyleDefault {
            label.textColor = UIColor.black
        }
        else{
            label.textColor = UIColor.white
        }
        label.textAlignment = .center
        label.backgroundColor = UIColor.clear
        
        scrollToBottom()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        self.tableView.snp.remakeConstraints{ (make) -> Void in
            make.left.right.equalTo(self.view);
            make.height.equalTo(self.view)
            make.top.equalTo(self.view)
        }
        
        UIView.animate(withDuration: 0.6, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 8, options: UIView.AnimationOptions.curveLinear, animations: { () -> Void in
            self.view.layoutIfNeeded()
        }, completion: nil)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        (self.navigationController as! V2EXNavigationController).navigationBarAlpha = 0
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        (self.navigationController as! V2EXNavigationController).navigationBarAlpha = 1
    }
    
    func scrollToBottom() {
        let section = self.tableView.numberOfSections - 1
        let row = self.tableView.numberOfRows(inSection: section) - 1
        if section < 0 || row < 0 {
            return
        }
        let path = IndexPath(row: row, section: section)
        self.tableView.scrollToRow(at: path, at: .bottom, animated: false)
    }
    
    
}

extension RelevantCommentsViewController: UITableViewDelegate, UITableViewDataSource {
    func scrollViewDidEndDragging(_ scrollView: UIScrollView, willDecelerate decelerate: Bool) {
        if scrollView.contentOffset.y <= -100 {
            scrollView.contentInset = UIEdgeInsets(top: -1 * scrollView.contentOffset.y, left: 0, bottom: 0, right: 0)
            scrollView.isScrollEnabled = false
            self.navigationController?.dismiss(animated: true, completion: nil)
            self.dismissing = true
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.commentsArray.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        let layout = self.commentsArray[indexPath.row].textLayout!
        return layout.textBoundingRect.size.height + 12 + 35 + 12 + 12 + 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "TopicDetailCommentCell") as! TopicDetailCommentCell
        cell.bind(self.commentsArray[indexPath.row])
        return cell
    }
}

//
//  MoreViewController.swift
//  V2EX-Clone
//
//  Created by yidao on 2019/9/23.
//  Copyright © 2019 yidao. All rights reserved.
//

import UIKit

class MoreViewController: UITableViewController {

    private let moreArray: [String] = [
    "",
    "viewOptions".localized,
    "",
    "rateV2ex".localized,
    "reportAProblem".localized,
    "",
    "followThisProjectSourceCode".localized,
    "open-SourceLibraries".localized,
    "version".localized]
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.title = "more".localized
        
        self.tableView.separatorStyle = .none
        self.tableView.register(BaseDetailTableViewCell.self, forCellReuseIdentifier: "BaseDetailTableViewCell")
        
        self.themeChangedHandler = { [weak self] (style) in
            guard let self = self else {
                return
            }
            self.view.backgroundColor = V2EXColor.colors.v2_backgroundColor
            self.tableView.reloadData()
        }
    }


   
}

extension MoreViewController {
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return [30,50,12,50,50,12,50,50,50][indexPath.row]
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        
        if indexPath.row == 1 {
            V2Client.instance.centerNavigation?.pushViewController(SettingsTableViewController(), animated: true)
        }
    }
}

extension MoreViewController {
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 9
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "BaseDetailTableViewCell") as! BaseDetailTableViewCell
        
        cell.selectionStyle = .none
        
        //设置标题
        cell.titleLabel.text = self.moreArray[indexPath.row]
        
        //设置颜色
        if [0,2,5].contains(indexPath.row) {
            cell.backgroundColor = self.tableView.backgroundColor
        }
        else{
            cell.backgroundColor = V2EXColor.colors.v2_cellWhiteBackgroundColor
        }
        
        //设置右侧箭头
        if [0,2,5,8].contains(indexPath.row) {
            cell.detailMarkHidden = true
        }
        else {
            cell.detailMarkHidden = false
        }
        
        //设置右侧文本
        if indexPath.row == 8 {
            cell.detailLabel.text = "Version " + (Bundle.main.infoDictionary!["CFBundleShortVersionString"] as! String)
        }
        else {
            cell.detailLabel.text = ""
        }
        
        return cell
    }
}

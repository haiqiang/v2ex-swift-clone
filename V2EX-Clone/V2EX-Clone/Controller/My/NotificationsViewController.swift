//
//  NotificationsViewController.swift
//  V2EX-Clone
//
//  Created by yidao on 2019/9/24.
//  Copyright © 2019 yidao. All rights reserved.
//

import UIKit
import MJRefresh

class NotificationsViewController: BaseViewController {

    private var notificatioinsArray: [NotificationsModel] = []
    
    private lazy var tableView: UITableView = {
        let tableView = UITableView()
        tableView.backgroundColor = .clear
        tableView.estimatedRowHeight = 100
        tableView.separatorStyle = .none
        
        tableView.register(NotificationTableViewCell.self, forCellReuseIdentifier: "NotificationTableViewCell")
        tableView.delegate = self
        tableView.dataSource = self
        return tableView
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.title = "notifications".localized
        self.view.backgroundColor = V2EXColor.colors.v2_backgroundColor
        
        self.view.addSubview(self.tableView)
        self.tableView.snp.makeConstraints { (make) in
            make.edges.equalToSuperview()
        }
        
        self.tableView.mj_header = V2RefreshHeader(refreshingBlock:{ [weak self] in
            guard let self = self else { return }
            self.refresh()
        })
        
        self.showLoadingView()
        self.tableView.mj_header.beginRefreshing()
    }
    
    private func refresh() {
        NotificationsModel.getNotifications { [weak self] (response) in
            guard let self = self else { return }
            if response.success && response.value != nil {
                self.notificatioinsArray = response.value!
                self.tableView.reloadData()
            }
            self.tableView.mj_header.endRefreshing()
            self.hideLoadingView()
        }
    }
    
}

extension NotificationsViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return tableView.fin_heightForCellWithIdentifier(NotificationTableViewCell.self, indexpath: indexPath, configuration: { (cell) in
            cell.bind(self.notificatioinsArray[indexPath.row])
        })
    }
}

extension NotificationsViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.notificatioinsArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "NotificationTableViewCell") as! NotificationTableViewCell
        cell.bind(self.notificatioinsArray[indexPath.row])
        cell.replyButton.tag = indexPath.row
        if cell.replyButtonClickHandler == nil {
            cell.replyButtonClickHandler = { [weak self] (sender) in
                guard let self = self else { return }
                self.replyClick(sender)
            }
        }
        return cell
    }
}

extension NotificationsViewController {
    func replyClick(_ sender: UIButton) {

        let item = self.notificatioinsArray[sender.tag]
        
        let replyViewController = ReplyViewController()
        
        let tempTopicModel = TopicDetailModel()
        replyViewController.atSomeone = "@" + item.userName! + " "
        tempTopicModel.topicId = item.topicId
        replyViewController.topicModel = tempTopicModel
        
        let nav = V2EXNavigationController(rootViewController:replyViewController)
        self.navigationController?.present(nav, animated: true, completion:nil)
    }
}

//
//  MemberViewController.swift
//  V2EX-Clone
//
//  Created by yidao on 2019/9/11.
//  Copyright © 2019 yidao. All rights reserved.
//

import UIKit
import FXBlurView
import SnapKit

class MemberViewController: UIViewController {

    var color:CGFloat = 0
    
    var username: String?
    var blockButton: UIButton?
    var followButton: UIButton?
    var model: MemberModel?
    
    var headerHeight: CGFloat = {
        if UIDevice.current.isIphoneX {
            return 240 + 24
        }
        return 240
    }()
    
    var nickLabelTop: CGFloat = {
        if UIDevice.current.isIphoneX {
            return 156 + 24
        }
        return 156
    }()
    
    private lazy var tableView: UITableView = {
        let tab = UITableView()
        tab.backgroundColor = .clear
        tab.estimatedRowHeight = 200
        tab.separatorStyle = .none
        
        tab.register(MemberHeaderCell.self, forCellReuseIdentifier: "MemberHeaderCell")
        tab.register(MemberTopicCell.self, forCellReuseIdentifier: "MemberTopicCell")
        tab.register(MemberReplyCell.self, forCellReuseIdentifier: "MemberReplyCell")

        tab.delegate = self
        tab.dataSource = self
        
        return tab
    }()
    
    private weak var _loadView: UIActivityIndicatorView?
    var tableViewHeader: [UIView?] = []
    
    var titleView: UIView = UIView()
    var titleLabel: UILabel = UILabel()
    var backgroundImageView: UIImageView?

    override func viewDidLoad() {
        super.viewDidLoad()
    
        self.view.backgroundColor = V2EXColor.colors.v2_backgroundColor
        
        self.backgroundImageView = UIImageView(image: UIImage(named: "12.jpg"))
        self.backgroundImageView!.frame = self.view.frame
        self.backgroundImageView!.contentMode = .scaleToFill
        view.addSubview(self.backgroundImageView!)
        
        let frostedView = FXBlurView()
        frostedView.underlyingView = self.backgroundImageView!
        frostedView.isDynamic = false
        frostedView.frame = self.view.frame
        frostedView.tintColor = UIColor.black
        self.view.addSubview(frostedView)
        
        
        self.view.addSubview(self.tableView)
        self.tableView.snp.makeConstraints { (make) in
            make.edges.equalToSuperview()
        }
        
        self.titleView = UIView(frame: CGRect(x: 0, y: 0, width: 150, height: 44))
        self.navigationItem.titleView = self.titleView
        
        
        let aloadView = UIActivityIndicatorView(style: .white)
        self.view.addSubview(aloadView)
        aloadView.startAnimating()
        aloadView.snp.makeConstraints{ (make) -> Void in
            make.centerY.equalTo(self.view.snp.top).offset( (kNavigationBarHeight - 44 ) + 44 / 2 )
            make.right.equalTo(self.view).offset(-15)
        }
        self._loadView = aloadView
        
        refreshData()
        
        if V2EXColor.instance.style == V2EXColor.V2EXColorStyleDark {
            self.color = 100
        }
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        (self.navigationController as? V2EXNavigationController)?.navigationBarAlpha = 0
        changeNavigationBarTintColor()
        (self.navigationController as? V2EXNavigationController)?.navigationBarAlpha = tableView.contentOffset.y / 100
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        (self.navigationController as? V2EXNavigationController)?.navigationBarAlpha = 1
        self.navigationController?.navigationBar.tintColor = V2EXColor.colors.v2_navigationBarTintColor
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        (self.navigationController as? V2EXNavigationController)?.navigationBarAlpha = 1
        self.navigationController?.navigationBar.tintColor = V2EXColor.colors.v2_navigationBarTintColor
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        var frame = self.titleView.frame
        frame.origin.x = (frame.size.width - kScreenWidth)/2
        frame.size.width = kScreenWidth
        
        let coverView = UIView(frame: frame)
        coverView.clipsToBounds = true
        self.titleView.addSubview(coverView)
        
        self.titleLabel = UILabel(frame: CGRect(x: 0, y: 44, width: kScreenWidth, height: 44))
        self.titleLabel.text = self.model != nil ? self.model!.userName! : "Hello"
        self.titleLabel.font = UIFont.systemFont(ofSize: 16)
        self.titleLabel.textAlignment = .center
        self.titleLabel.textColor = V2EXColor.colors.v2_topicListTitleColor
        coverView.addSubview(self.titleLabel)

    }
    
    func refreshData() {
        MemberModel.getMemberInfo(self.username!) { (response) in
            if response.success {
                if let aModel = response.value {
                    self.getDataSuccessfully(aModel)
                } else {
                    self.tableView.fin_reloadData()
                }
            }
            if let view = self._loadView {
                view.removeFromSuperview()
            }
        }
    }
    
    func getDataSuccessfully(_ aModel: MemberModel) {
        self.model = aModel
        self.titleLabel.text = self.model?.userName
        if self.model?.userToken != nil {
            setupBlockAndFollowButtons()
        }
        self.tableView.fin_reloadData()
    }
    
    func changeNavigationBarTintColor() {
        let offsetY = self.tableView.contentOffset.y
        var y = 100 - offsetY
        if offsetY < 0 {
            y = 100
        }
        else if offsetY > 100 {
            y = 0
        }
        //后退按钮颜色
        self.navigationController?.navigationBar.tintColor = UIColor.RGB(r: y * 2.4 + self.color, g: y * 2.4 + self.color, b: y * 2.4 + self.color)
    
    
    }
    
    func changeNavigationAlpha() {
        (self.navigationController as? V2EXNavigationController)?.navigationBarAlpha = self.tableView.contentOffset.y / 100
    }
}

extension MemberViewController: UITableViewDelegate {
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        var offsetY = scrollView.contentOffset.y
        // navigationBar 的透明度
        self.changeNavigationAlpha()
        // 后退按钮颜色
        self.changeNavigationBarTintColor()
        
        let navigationBarHeight = kNavigationBarHeight
        
        let nickLabelDistanceToNavigationBarBottom = nickLabelTop - navigationBarHeight
   
        offsetY += (44 - 13) / 2
        
        var y: CGFloat = 0
        if offsetY <= nickLabelDistanceToNavigationBarBottom {
            y = 44
        } else if offsetY >= nickLabelDistanceToNavigationBarBottom + 44 {
            y = 0
        } else {
            y = 44 - (offsetY - nickLabelDistanceToNavigationBarBottom)
        }
        
        var frame = self.titleLabel.frame
        frame.origin.y = y
        self.titleLabel.frame = frame
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return [0, 40, 40][section]
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.section == 0 {
            return headerHeight
        }
        else if indexPath.section == 1 {
            return tableView.fin_heightForCellWithIdentifier(MemberTopicCell.self, indexpath: indexPath, configuration: { (cell) in
                cell.bind(self.model!.topics[indexPath.row])
            })
        }
        else {
            return tableView.fin_heightForCellWithIdentifier(MemberReplyCell.self, indexpath: indexPath, configuration: { (cell) in
                cell.bind(self.model!.replies[indexPath.row])
            })
        }
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        if tableViewHeader.count > section - 1 {
            return tableViewHeader[section-1]
        }
        let view = UIView()
        view.backgroundColor = V2EXColor.colors.v2_cellWhiteBackgroundColor
        
        let label = UILabel()
        label.text = ["posts".localized, "comments".localized][section - 1]
        view.addSubview(label)
        label.font = UIFont.systemFont(ofSize: 15.0)
        label.textColor = V2EXColor.colors.v2_topicListUserNameColor
        label.snp.makeConstraints{ (make) -> Void in
            make.centerY.equalTo(view)
            make.leading.equalTo(view).offset(12)
        }
        
        tableViewHeader.append(view)
        return view
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView .deselectRow(at: indexPath, animated: true)
        
        var id: String?
        if indexPath.section == 1 {
            id = self.model?.topics[indexPath.row].topicId
        } else if indexPath.section == 2 {
            id = self.model?.replies[indexPath.row].topicId
        }
        
        if let id = id {
            V2ProgrssHUD.inform("话题 + \(id)")
        }
    }
}

extension MemberViewController: UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 3
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if let rows = [1, self.model?.topics.count, self.model?.replies.count][section] {
            return rows
        }
        return 0
    }
    
   
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.section == 0 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "MemberHeaderCell") as! MemberHeaderCell
            cell.bind(self.model)
            return cell
        }
        else if indexPath.section == 1 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "MemberTopicCell") as! MemberTopicCell
            cell.bind(self.model!.topics[indexPath.row])
            return cell
        } else {
            let cell = tableView.dequeueReusableCell(withIdentifier: "MemberReplyCell") as! MemberReplyCell
            cell.bind(self.model!.replies[indexPath.row])
            return cell
        }
    }
    
}

extension MemberViewController {
    func setupBlockAndFollowButtons() {
        if !self.isMember(of: MemberViewController.self) {
            return
        }
        
        let blockButton = UIButton(frame:CGRect(x: 0, y: 0, width: 26, height: 26))
        blockButton.addTarget(self, action: #selector(toggleBlockState), for: .touchUpInside)
        let followButton = UIButton(frame:CGRect(x: 0, y: 0, width: 26, height: 26))
        followButton.addTarget(self, action: #selector(toggleFollowState), for: .touchUpInside)
        
        let blockItem = UIBarButtonItem(customView: blockButton)
        let followItem = UIBarButtonItem(customView: followButton)
        
        //处理间距
        let fixedSpaceItem = UIBarButtonItem(barButtonSystemItem: .fixedSpace, target: nil, action: nil)
        fixedSpaceItem.width = -5
        self.navigationItem.rightBarButtonItems = [fixedSpaceItem,followItem,blockItem]
        
        self.blockButton = blockButton;
        self.followButton = followButton;
        
        refreshButtonImage()
    }
    
    func refreshButtonImage() {
        let blockImage = self.model?.blockState == .blocked ? UIImage(named: "ic_visibility_off")! : UIImage(named: "ic_visibility")!
        let followImage = self.model?.followState == .followed ? UIImage(named: "ic_favorite")! : UIImage(named: "ic_favorite_border")!
        self.blockButton?.setImage(blockImage.withRenderingMode(.alwaysTemplate), for: .normal)
        self.followButton?.setImage(followImage.withRenderingMode(.alwaysTemplate), for: .normal)
    }
    
    @objc func toggleFollowState() {
        if(self.model?.followState == .followed){
            UnFollow()
        }
        else{
            Follow()
        }
        refreshButtonImage()
    }
    
    func Follow() {
        if let userId = self.model?.userId, let userToken = self.model?.userToken {
            MemberModel.follow(userId, userToken: userToken, type: .followed, completionHandler: nil)
            self.model?.followState = .followed
            V2ProgrssHUD.success("关注成功")
        }
    }
    
    func UnFollow() {
        if let userId = self.model?.userId, let userToken = self.model?.userToken {
            MemberModel.follow(userId, userToken: userToken, type: .unFollowed, completionHandler: nil)
            self.model?.followState = .unFollowed
            V2ProgrssHUD.success("取消关注了~")
        }
    }
    
    @objc func toggleBlockState() {
        if self.model?.blockState == .blocked {
            UnBlock()
        } else {
            Block()
        }
        refreshButtonImage()
    }
    
    func Block() {
        if let userId = self.model?.userId, let userToken = self.model?.blockToken {
            MemberModel.block(userId, userToken: userToken, type: .blocked, completionHandler: nil)
            self.model?.blockState = .blocked
            V2ProgrssHUD.success("屏蔽成功")
        }
    }
    
    func UnBlock() {
        if let userId = self.model?.userId, let userToken = self.model?.blockToken {
            MemberModel.block(userId, userToken: userToken, type: .unBlocked, completionHandler: nil)
            self.model?.blockState = .unBlocked
            V2ProgrssHUD.success("取消屏蔽了~")
        }
    }
    
}

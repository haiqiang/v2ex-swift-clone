//
//  FavoritesViewController.swift
//  V2EX-Clone
//
//  Created by yidao on 2019/9/25.
//  Copyright © 2019 yidao. All rights reserved.
//

import UIKit

class FavoritesViewController: BaseViewController {
    var topicList: [TopicListModel]?
    var currentPage = 1
    var maxPage = 1
    
    private lazy var tableView: UITableView = {
       let table = UITableView()
       table.cancelEstimatedHeight()
        table.backgroundColor = V2EXColor.colors.v2_backgroundColor
        table.separatorStyle = .none
        
        table.register(HomeTopicListTableViewCell.self, forCellReuseIdentifier: "HomeTopicListTableViewCell")
        table.delegate = self
        table.dataSource = self 
       return table
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.title = "favorites".localized
        self.view.backgroundColor = V2EXColor.colors.v2_backgroundColor
        self.view.addSubview(self.tableView)
        self.tableView.snp.makeConstraints { (make) in
            make.edges.equalToSuperview()
        }
        
        self.showLoadingView()
        self.tableView.mj_header = V2RefreshHeader(refreshingBlock: {
            [weak self] in
            guard let self = self else { return }
            self.refresh()
        })
        self.tableView.mj_header.beginRefreshing()
        
        let footer = V2RefreshFooter(refreshingBlock: { [weak self] in
            guard let self = self else { return }
            self.getNextPage()
        })
        footer?.centerOffset = -4
        self.tableView.mj_footer = footer
    }
    
    func refresh() {
        self.currentPage = 1
        
        _ = TopicListAPI.provider.requestAPI(.favoriteList(page: self.currentPage)).getJiDataFirst(handler: { (ji) in
            var maxPage = 1
            if let aRootNode = ji.xPath("//*[@class='page_normal']")?.last, let page = aRootNode.content, let pageInt = Int(page) {
                maxPage = pageInt
            }
            self.maxPage = maxPage
        }).mapResponseToJiArray(FavoriteListModel.self).subscribe(onNext: { (response) in
            self.topicList = response
            self.tableView.mj_footer.resetNoMoreData()
            self.tableView.reloadData()
            
            self.tableView.mj_header.endRefreshing()
            self.hideLoadingView()
        }, onError: { (error) in
            V2ProgrssHUD.error(error.rawString())
            self.tableView.mj_header.endRefreshing()
            self.hideLoadingView()
        })
    }
    
    func getNextPage() {
        if let count = self.topicList?.count, count <= 0 {
            self.tableView.mj_footer.endRefreshing()
            return;
        }
        if self.currentPage >= maxPage {
            self.tableView.mj_footer.endRefreshingWithNoMoreData()
            return;
        }
        self.currentPage += 1
        
        _ = TopicListAPI.provider
            .requestAPI(.favoriteList(page: self.currentPage))
            .mapResponseToJiArray(FavoriteListModel.self)
            .subscribe(onNext: { (response) in
                self.topicList?.append(contentsOf: response)
                self.tableView.reloadData()
                self.tableView.mj_footer.endRefreshing()
            }, onError: { (error) in
                self.currentPage -= 1
                V2ProgrssHUD.error(error.rawString())
            })
    }
}

extension FavoritesViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        let item = self.topicList![indexPath.row]
        let titleHeight = item.topicTitleLayout?.textBoundingRect.size.height ?? 0
        //          上间隔   头像高度  头像下间隔       标题高度    标题下间隔 cell间隔
        let height = 12    +  35     +  12      + titleHeight   + 12      + 8
        return height
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        
    }
}

extension FavoritesViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if let list = self.topicList {
            return list.count
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "HomeTopicListTableViewCell") as! HomeTopicListTableViewCell
        cell.bind(self.topicList![indexPath.row])
        return cell
    }
    
    
}

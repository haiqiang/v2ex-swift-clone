//
//  AccountsManagerViewController.swift
//  V2EX-Clone
//
//  Created by yidao on 2019/9/19.
//  Copyright © 2019 yidao. All rights reserved.
//

import UIKit

class AccountsManagerViewController: UIViewController {

    private var users: [LocalSecurityAccountModel] = []
    private lazy var tableView: UITableView = {
        let table = UITableView()
        table.backgroundColor = V2EXColor.colors.v2_backgroundColor
        table.estimatedRowHeight = 100
        table.separatorStyle = .none
        
        table.register(BaseDetailTableViewCell.self, forCellReuseIdentifier: "BaseDetailTableViewCell")
        table.register(AccountListTableViewCell.self, forCellReuseIdentifier: "AccountListTableViewCell")
        table.register(LogoutTableViewCell.self, forCellReuseIdentifier: "LogoutTableViewCell")

        table.delegate = self
        table.dataSource = self
        return table
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        setup()
    }
    
    func setup() {
        self.title = "accounts".localized
        self.view.backgroundColor = V2EXColor.colors.v2_backgroundColor
        
        let warningButton = UIButton(frame: CGRect(x: 0, y: 0, width: 40, height: 40))
        warningButton.contentMode = .center
        warningButton.imageEdgeInsets = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: -20)
        warningButton.setImage(UIImage(named: "ic_warning")?.withRenderingMode(.alwaysTemplate), for: .normal)
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(customView: warningButton)
        warningButton.addTarget(self, action: #selector(self.warningClick), for: .touchUpInside)
        
        self.view.addSubview(self.tableView)
        self.tableView.contentInset = UIEdgeInsets(top: 20, left: 0, bottom: 0, right: 0)
        self.tableView.snp.makeConstraints { (make) in
            make.top.bottom.equalToSuperview()
            make.center.equalToSuperview()
            make.width.equalTo(kScreenWidth)
        }
        
        for (_, user) in V2UserKeychain.sharedInstance.users {
            self.users.append(user)
        }
    }
    
    @objc func warningClick() {
        let alert = UIAlertController(title: "临时隐私声明", message: "当你登录时，软件会自动将你的用户名保存于系统的Keychain中（非常安全）。如果你不希望软件保存你的用户名，可以左滑账号并点击删除。\n后续会完善隐私声明页面，并添加 关闭保存用户名 的选项。", preferredStyle: .alert)
        let okAction = UIAlertAction(title: "Ok", style: .default, handler: nil)
        alert.addAction(okAction)
        self.present(alert, animated: true, completion: nil)
    }
}


extension AccountsManagerViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.row < self.users.count {
            return 55
        } else if indexPath.row == self.users.count {
            return 15
        } else {
            return 45
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        
        let rows = self.tableView.numberOfRows(inSection: 0)
        if indexPath.row == rows - 1 {
            let alert = UIAlertController(title: "确定注销当前账号吗？", message: "注销只会退出登录，并不会删除保存在Keychain中的账户名与密码。如需删除，请左滑需要删除的账号，然后点击删除按钮", preferredStyle: .alert)
            let cancelAction = UIAlertAction(title: "cancel", style: .cancel)
            let logoutAction = UIAlertAction(title: "logout", style: .destructive) { (_) in
                V2User.sharedInstance.loginOut()
                self.navigationController?.popToRootViewController(animated: true)
            }
            alert.addAction(cancelAction)
            alert.addAction(logoutAction)
            self.present(alert, animated: true, completion: nil)
        }
    }
}

extension AccountsManagerViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.users.count + 2
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.row < self.users.count {
            let cell = tableView.dequeueReusableCell(withIdentifier: "AccountListTableViewCell") as! AccountListTableViewCell
            cell.bind(self.users[indexPath.row])
            return cell
        } else if indexPath.row == self.users.count {
            let cell = tableView.dequeueReusableCell(withIdentifier: "BaseDetailTableViewCell") as! BaseDetailTableViewCell
            cell.detailMarkHidden = true
            cell.backgroundColor = tableView.backgroundColor
            return cell
        } else {
            let cell = tableView.dequeueReusableCell(withIdentifier: "LogoutTableViewCell") as! LogoutTableViewCell
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        if indexPath.row < self.users.count {
            return true
        }
        return false
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            if let username = self.users[indexPath.row].username {
                self.users.remove(at: indexPath.row)
                V2UserKeychain.sharedInstance.removeUser(username)
                tableView.deleteRows(at: [indexPath], with: .none)
            }
        }
    }
}

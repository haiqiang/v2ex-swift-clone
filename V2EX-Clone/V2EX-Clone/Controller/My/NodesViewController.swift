//
//  NodesViewController.swift
//  V2EX-Clone
//
//  Created by yidao on 2019/9/23.
//  Copyright © 2019 yidao. All rights reserved.
//

import UIKit

class NodesViewController: BaseViewController {

    var nodeGroupArray: [NodeGroupModel]?
    lazy var collectionView: UICollectionView = {
        let layout: V2LeftAlignedCollectionViewFlowLayout = V2LeftAlignedCollectionViewFlowLayout()
        layout.sectionInset = UIEdgeInsets(top: 10, left: 15, bottom: 10, right: 15)
        let collection: UICollectionView = UICollectionView(frame: self.view.bounds, collectionViewLayout: layout)
        collection.backgroundColor = V2EXColor.colors.v2_cellWhiteBackgroundColor
        collection.delegate = self
        collection.dataSource = self
        self.view.addSubview(collection)
        
        collection .register(NodeTableViewCell.self, forCellWithReuseIdentifier: "Cell")
        collection.register(NodeCollectionReusableView.self, forSupplementaryViewOfKind: UICollectionView.elementKindSectionHeader, withReuseIdentifier: "nodeGroupNameView")
        return collection
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.title = "Navigation".localized
        self.view.backgroundColor = V2EXColor.colors.v2_backgroundColor
        
        self.collectionView.snp.makeConstraints { (make) in
            make.edges.equalToSuperview()
        }
        
        NodeGroupModel.getNodes { (response) in
            if response.success {
                self.nodeGroupArray = response.value
                self.collectionView.reloadData()
            }
            self.hideLoadingView()
        }
        self.showLoadingView()
    }
    
}

extension NodesViewController: UICollectionViewDelegate {
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        V2ProgrssHUD.inform("啊！！！")
    }
}

extension NodesViewController: UICollectionViewDataSource {
    func numberOfSections(in collectionView: UICollectionView) -> Int {
       return self.nodeGroupArray?.count ?? 0
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.nodeGroupArray![section].children.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let nodeModel = self.nodeGroupArray![indexPath.section].children[indexPath.row]
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "Cell", for: indexPath) as! NodeTableViewCell
        cell.textLabel.text = nodeModel.nodeName
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        let nodeGroupNameView = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: "nodeGroupNameView", for: indexPath) as! NodeCollectionReusableView
        nodeGroupNameView.label.text = self.nodeGroupArray![indexPath.section].groupName
        return nodeGroupNameView
    }
    
}

extension NodesViewController: UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let nodeModel = self.nodeGroupArray![indexPath.section].children[indexPath.row]
        return CGSize(width: nodeModel.width, height: 25)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 15
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForHeaderInSection section: Int) -> CGSize {
        return CGSize(width: collectionView.bounds.width, height: 35)
    }
}

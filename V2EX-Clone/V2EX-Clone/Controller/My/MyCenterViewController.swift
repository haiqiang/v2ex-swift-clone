//
//  MyCenterViewController.swift
//  V2EX-Clone
//
//  Created by yidao on 2019/9/11.
//  Copyright © 2019 yidao. All rights reserved.
//

import UIKit

class MyCenterViewController: MemberViewController {

    var settingBtn: UIButton = UIButton(frame: CGRect(x: 0, y: 0, width: 40, height: 40))
    
    override func viewDidLoad() {
        super.viewDidLoad()

        setup()
    }

    private func setup() {
        self.settingBtn.contentMode = .center
        self.settingBtn.imageEdgeInsets = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: -20)
        
        self.settingBtn.setImage(UIImage(named: "ic_supervisor_account")!.withRenderingMode(.alwaysTemplate), for: .normal)
   
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(customView: self.settingBtn)
        self.settingBtn.addTarget(self, action: #selector(self.accountManagerClick), for: .touchUpInside)
        self.settingBtn.isHidden = true
    }
    
    override func getDataSuccessfully(_ aModel: MemberModel) {
        super.getDataSuccessfully(aModel)
        self.settingBtn.isHidden = false
    }
    
    @objc private func accountManagerClick() {
        self.navigationController?.pushViewController(AccountsManagerViewController(), animated: true)
    }
}

//
//  LeftViewController.swift
//  V2EX-Clone
//
//  Created by yidao on 2019/9/9.
//  Copyright © 2019 yidao. All rights reserved.
//

import UIKit
import FXBlurView

class LeftViewController: UIViewController {

    lazy var backgroundImageView: UIImageView = {
        let backgroundImageView = UIImageView()
        backgroundImageView.contentMode = UIView.ContentMode.scaleAspectFill
        return backgroundImageView
    }()
    
    lazy var frostedView: FXBlurView = {
       let frostedView = FXBlurView()
        frostedView.isDynamic = false
        frostedView.tintColor = UIColor.black
        return frostedView
    }()
    
    fileprivate lazy var tableView: UITableView = {
        let tableView = UITableView()
        tableView.backgroundColor = UIColor.clear
        tableView.estimatedRowHeight = 100
        tableView.separatorStyle = .none
        
        tableView.register(LeftUserHeadCell.self, forCellReuseIdentifier: "LeftUserHeadCell")
        tableView.register(LeftNodeTableViewCell.self, forCellReuseIdentifier: "LeftNodeTableViewCell")
        tableView.register(LeftNotificationCell.self, forCellReuseIdentifier: "LeftNotificationCell")
        
        tableView.delegate = self
        tableView.dataSource = self
        return tableView
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.view.backgroundColor = V2EXColor.colors.v2_backgroundColor
        
        self.backgroundImageView.frame = self.view.frame
        view.addSubview(self.backgroundImageView)
        
        self.frostedView.underlyingView = self.backgroundImageView
        self.frostedView.frame = self.view.frame
        view.addSubview(self.frostedView)
        
        self.view.addSubview(self.tableView)
        self.tableView.snp.makeConstraints { make in
            make.edges.equalToSuperview()
        }
        
        if V2User.sharedInstance.isLogin {
            self.getUserInfo(V2User.sharedInstance.username!)
        }
        
        self.themeChangedHandler = { [weak self] (style) -> Void in
            if V2EXColor.instance.style == V2EXColor.V2EXColorStyleDefault {
                self?.backgroundImageView.image = UIImage(named: "32.jpg")
            } else {
                self?.backgroundImageView.image = UIImage(named: "32.jpg")
            }
            self?.frostedView.updateAsynchronously(true, completion: nil)
        }
    }
    
    func getUserInfo(_ username: String) {
        UserModel.getUserInfoByUsername(username) { (response) in
            if response.success {
                
            } else {
                
            }
        }
    }
}

extension LeftViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.section == 1 && indexPath.row == 2 {
            return 55 + 10
        }
        return [180, 55 + SEPARATOR_HEIGHT, 55 + SEPARATOR_HEIGHT][indexPath.section]
    }
}

extension LeftViewController: UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 3
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return [1, 3, 2][section]
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.section == 0 {
            if indexPath.row == 0 {
                let cell = tableView.dequeueReusableCell(withIdentifier: "LeftUserHeadCell") as! LeftUserHeadCell
                return cell
            }
            return UITableViewCell()
        }
        else if indexPath.section == 1 {
            if indexPath.row == 1 {
                let cell = tableView.dequeueReusableCell(withIdentifier: "LeftNotificationCell") as! LeftNotificationCell
                cell.nodeImageView.image = UIImage(named: "ic_notifications_none")
                return cell
            } else {
                let cell = tableView.dequeueReusableCell(withIdentifier: "LeftNodeTableViewCell") as! LeftNodeTableViewCell
                cell.nodeNameLabel.text = ["me".localized, "", "favorites".localized][indexPath.row]
                let names = ["ic_face","","ic_turned_in_not"]
                cell.nodeImageView.image = UIImage(named: names[indexPath.row])
                return cell
            }
        } else {
            let cell = tableView.dequeueReusableCell(withIdentifier: "LeftNodeTableViewCell") as! LeftNodeTableViewCell
            cell.nodeNameLabel.text = ["nodes".localized, "more".localized][indexPath.row]
            let names = ["ic_navigation","ic_settings_input_svideo"]
            cell.nodeImageView.image = UIImage(named: names[indexPath.row])
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        if !V2User.sharedInstance.isLogin && (indexPath.section == 0 || indexPath.section == 1) {
            let loginVC = LoginViewController()
            V2Client.instance.centerViewController?.navigationController?.present(loginVC, animated: true, completion: nil)
            return
        }

        // 个人中心页面
        if (indexPath.section == 0 || indexPath.section == 1) && indexPath.row == 0 {
            let my = MyCenterViewController()
            my.username = V2User.sharedInstance.username
            V2Client.instance.centerNavigation?.pushViewController(my, animated: true)
        }
        
        if indexPath.section == 1 {
            if indexPath.row == 1 {
                pushToNextVC(NotificationsViewController())
            }
            
            if indexPath.row == 2 {
                pushToNextVC(FavoritesViewController())
            }
        }
        
        if indexPath.section == 2 {
            if indexPath.row == 0 {
                pushToNextVC(NodesViewController())
            }
            
            if indexPath.row == 1 {
                pushToNextVC(MoreViewController())
            }
        }
        
        
        V2Client.instance.drawerController?.closeDrawer(animated: true, completion: nil)

    }
    
    private func pushToNextVC<T: UIViewController>(_ other: T) {
        V2Client.instance.centerNavigation?.pushViewController(other, animated: true)
    }
}



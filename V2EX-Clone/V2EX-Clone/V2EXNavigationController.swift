//
//  V2EXNavigationController.swift
//  V2EX-Clone
//
//  Created by yidao on 2019/9/6.
//  Copyright © 2019 yidao. All rights reserved.
//

import UIKit
import SnapKit

class V2EXNavigationController: UINavigationController {

    var frostedView: UIToolbar = UIToolbar()
    var shadowImage: UIImage?
    var navigationBarAlpha: CGFloat {
        get {
            return self.frostedView.alpha
        }
        set {
            var value = newValue
            if newValue > 1 {
                value = 1
            } else if newValue < 0 {
                value = 0
            }
            self.frostedView.alpha = value
            if value == 1 {
                if self.navigationBar.shadowImage != nil {
                    self.navigationBar.shadowImage = nil
                }
            } else {
                if self.navigationBar.shadowImage == nil {
                    self.navigationBar.shadowImage = UIImage()
                }
            }
        }
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        get {
            if V2EXColor.instance.style == V2EXColor.V2EXColorStyleDefault {
                return .default
            } else {
                return .lightContent
            }
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationBar.setBackgroundImage(UIColor.createImageByColor(UIColor.clear), for: .default)
        self.navigationBar.isTranslucent = false
        
        let maskingView = UIView()
        maskingView.isUserInteractionEnabled = false
        maskingView.backgroundColor = UIColor(white: 0, alpha: 0)
        self.navigationBar.superview?.insertSubview(maskingView, belowSubview: self.navigationBar)
        maskingView.frame = CGRect(x: 0, y: 0, width: kScreenWidth, height: kNavigationBarHeight)
        
        self.frostedView.isUserInteractionEnabled = false
        self.frostedView.clipsToBounds = true
        maskingView.addSubview(self.frostedView)
        self.frostedView.snp.makeConstraints { make in
            make.edges.equalToSuperview()
        }
        
        self.themeChangedHandler = { [weak self] style in
            guard let self = self else {
                return
            }
            self.navigationBar.tintColor = V2EXColor.colors.v2_navigationBarTintColor
            self.navigationBar.titleTextAttributes = [NSAttributedString.Key.font: UIFont.systemFont(ofSize: 18.0), NSAttributedString.Key.foregroundColor: V2EXColor.colors.v2_topicListTitleColor]
            
            if V2EXColor.instance.style == V2EXColor.V2EXColorStyleDefault {
                self.frostedView.barStyle = .default
                self.setNeedsStatusBarAppearanceUpdate()
                
                UITextView.appearance().keyboardAppearance = .light
                UITextField.appearance().keyboardAppearance = .light
            } else {
                self.frostedView.barStyle = .black
                self.setNeedsStatusBarAppearanceUpdate()

                UITextView.appearance().keyboardAppearance = .dark
                UITextField.appearance().keyboardAppearance = .dark
            }
        }
    }

    
}

//
//  UserAPI.swift
//  V2EX-Clone
//
//  Created by yidao on 2019/9/9.
//  Copyright © 2019 yidao. All rights reserved.
//

import Foundation

enum UserAPI {
    case getUserInfo(username: String)
}

extension UserAPI: V2EXTargetType {
    var path: String {
        switch self {
        case .getUserInfo:
            return "/api/members/show.json"
        }
    }
    
    var parameters: [String : Any]? {
        switch self {
        case let .getUserInfo(username):
            return ["username": username]
        }
    }
}

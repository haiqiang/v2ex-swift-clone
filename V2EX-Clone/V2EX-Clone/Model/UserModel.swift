//
//  UserModel.swift
//  V2EX-Clone
//
//  Created by yidao on 2019/9/9.
//  Copyright © 2019 yidao. All rights reserved.
//

import Foundation
import Alamofire
import Ji

class UserModel: Codable {
    public var status: String?
    public var id: NSInteger?
    public var url: String?
    public var username: String?
    public var website: String?
    public var twitter: String?
    public var psn: String?
    public var btc: String?
    public var location: String?
    public var tagline: String?
    public var bio: String?
    public var avatar_mini: String?
    public var avatar_normal: String?
    public var avatar_large: String?
    public var created: NSInteger?
    public var github: String?
}

// MARK: - Request
extension UserModel {
    class func Login(_ username: String, password: String, once: String, usernameFieldName: String, passwordFieldName: String, codeFieldName: String, code: String, completionHandler: @escaping (V2ValueResponse<String>, Bool) -> Void) {
        let params = ["once": once, "next": "/",
                      passwordFieldName: password,
                      usernameFieldName: username,
                      codeFieldName: code]
        
        var dict = MOBILE_CLIENT_HEADERS
        dict["Referer"] = "https://v2ex.com/signin"
        // 登录
        Alamofire.request(V2EXURL+"signin", method: .post, parameters: params, encoding: URLEncoding(), headers: dict).responseJiHTML { response in
            if let jiHtml = response.result.value{
                //判断有没有用户头像，如果有，则证明登录成功了
                if let avatarImg = jiHtml.xPath("//*[@id='Top']/div/div/table/tr/td[3]/a[1]/img[1]")?.first {
                    if let username = avatarImg.parent?["href"]{
                        if username.hasPrefix("/member/") {
                            let username = username.replacingOccurrences(of: "/member/", with: "")
                            //用户开启了两步验证
                            if let url = response.response?.url?.absoluteString, url.contains("2fa") {
                                completionHandler(V2ValueResponse(value: username, success: true),true)
                            }
                                //登陆完成
                            else{
                                completionHandler(V2ValueResponse(value: username, success: true), false)
                            }
                            return
                        }
                    }
                }
                else if let errMessage = jiHtml.xPath("//*[contains(@class, 'problem')]/ul/li")?.first?.value , errMessage.count > 0 {
                    completionHandler(V2ValueResponse(success: false,message: errMessage),false)
                    return
                }
                
            }
            completionHandler(V2ValueResponse(success: false,message: "登录失败"),false)
        }
    }
    
    class func twoFALogin(code: String, completionHandler: @escaping (Bool) -> Void) {
        V2User.sharedInstance.getOnce { (response) in
            if response.success {
                let prames = [
                    "code":code,
                    "once":V2User.sharedInstance.once!
                    ] as [String:Any]
                let url = V2EXURL + "2fa"
                Alamofire.request(url, method: .post, parameters: prames, headers: MOBILE_CLIENT_HEADERS).responseJiHTML { (response) -> Void in
                    if(response.result.isSuccess){
                        if let url = response.response?.url?.absoluteString, url.contains("2fa") {
                            completionHandler(false)
                        }
                        else{
                            completionHandler(true)
                        }
                    }
                    else{
                        completionHandler(false)
                    }
                }
            } else {
                completionHandler(false)
            }
        }
    }
    
    class func getUserInfoByUsername(_ username: String, completionHandler: ((V2ValueResponse<UserModel>) -> Void)?) {
        _ = UserAPI.provider.requestAPI(.getUserInfo(username: username)).mapResponseToObj(UserModel.self).subscribe(onNext: { userModel in
            V2User.sharedInstance.user = userModel
            
            if let avatar = userModel.avatar_large {
                V2UserKeychain.sharedInstance.update(username, password: nil, avatar: "https:" + avatar)
            }
            completionHandler?(V2ValueResponse(value: userModel, success: true))
        }, onError: { (error) in
            completionHandler?(V2ValueResponse(success: false, message: "获取用户信息失败"))
        })
    }
    
    class func dailyRedeem() {
        V2User.sharedInstance.getOnce { (response) in
            if response.success {
                Alamofire.request(V2EXURL + "mission/daily/redeem?once=" + V2User.sharedInstance.once!, method: .get, parameters: nil, encoding: URLEncoding(), headers: MOBILE_CLIENT_HEADERS).responseJiHTML(complectionHandler: { (response) in
                    if let JiHTML = response.result.value {
                        if let aRootNode = JiHTML.xPath("//*[@id='Wrapper']/div/div/div[@class='message']")?.last {
                            if aRootNode.content?.trimmingCharacters(in: .whitespaces) == "已成功领取每日登录奖励" {
                                print("每日登录奖励 领取成功")
                                dispatch_async_safely_main_queue({ () -> () in
                                    V2ProgrssHUD.inform("已成功领取每日登录奖励")
                                })
                            }
                        }
                    }
                })
            }
        }
    }
    
    
}


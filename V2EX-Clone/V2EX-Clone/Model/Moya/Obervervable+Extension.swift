//
//  Obervervable+Extension.swift
//  V2EX-Clone
//
//  Created by yidao on 2019/9/9.
//  Copyright © 2019 yidao. All rights reserved.
//

import UIKit
import RxSwift
import SwiftyJSON
import Moya
import Ji

public enum APIError: Swift.Error {
    case Error(info: String)
    case AccountBanned(info: String)
    case LoginPermissionRequired(info: String)
    case needs2FA(info: String) // 需要两步验证
}

extension Swift.Error {
    func rawString() -> String {
        guard let err = self as? APIError else {
            return self.localizedDescription
        }
        
        switch err {
        case let .Error(info):
            return info
        case let .AccountBanned(info):
            return info
        case let .LoginPermissionRequired(info):
            return info
        case let .needs2FA(info):
            return info
        }
    }
}

// MARK: - JSON解析相关
extension Observable where Element: Moya.Response {
    // 过滤HTTP错误，例如超时、请求失败等
    func filterHTTPError() -> Observable<Element> {
        return filter { response in
            if (200 ... 209) ~= response.statusCode {
                return true
            }
            throw APIError.Error(info: "网络错误")
        }
    }
    
    // 过滤逻辑错误，例如协议里返回 错误Code
    func filterResponseError() -> Observable<JSON> {
        return filterHTTPError().map({ response -> JSON in
            let json = try JSON(data: response.data)
            var code = 200
            var msg = ""
            if let codeStr = json["code"].rawString(), let c = Int(codeStr) {
                code = c
            }
            if json["msg"].exists() {
                msg = json["msg"].rawString()!
            } else if json["message"].exists() {
                msg = json["message"].rawString()!
            } else if json["info"].exists() {
                msg = json["info"].rawString()!
            } else if json["description"].exists() {
                msg = json["description"].rawString()!
            }
            
            if (code == 200 || code == 99999 || code == 80001 || code == 1) {
                return json
            }
            
            switch code {
            default: throw APIError.Error(info: msg)
            }
        })
    }
    
    
    // 将Response 转换成 JSON Model
    func mapResponseToObj<T: Codable>(_ typeName: T.Type, dataPath:[String] = []) -> Observable<T> {
        return filterResponseError().map { json in
            var rootJSON = json
            if dataPath.count > 0 {
                rootJSON = rootJSON[dataPath]
            }
            
            if let model: T = self.resultFromJSON(json: rootJSON) {
                return model
            } else {
                throw APIError.Error(info: "JSON 转换失败")
            }
        }
    }
    
    func mapResponseToObjArray<T: Codable>(_ type: T.Type, dataPath: [String] = []) -> Observable<[T]> {
        return  filterResponseError().map({ json in
            var rootJSON = json
            if dataPath.count > 0 {
                rootJSON = rootJSON[dataPath]
            }
            var result = [T]()
            guard let jsonArray = rootJSON.array else {
                return result
            }
            
            for json in jsonArray {
                if let jsonModel: T = self.resultFromJSON(json: json) {
                    result.append(jsonModel)
                } else {
                    throw APIError.Error(info: "JSON 转换失败")
                }
            }
            return result
        })
    }
    
    private func resultFromJSON<T: Codable>(jsonString: String) -> T? {
        let decoder = JSONDecoder()
        if let data = jsonString.data(using: .utf8) {
            let model: T = try! decoder.decode(T.self, from: data)
            return model
        }
        return nil
    }
    
    private func resultFromJSON<T: Codable>(json: JSON) -> T? {
        if let str = json.rawString() {
            return resultFromJSON(jsonString: str)
        }
        return nil
    }
}

// MARK: - Ji 解析相关
extension Observable where Element: Moya.Response {
    // 过滤业务逻辑错误
    func filterJiResponseError() -> Observable<Ji> {
        return filterHTTPError().map({ (response: Element) -> Ji in
            if response.response?.url?.path == "/signin" && response.request?.url?.path != "/signin" {
                throw APIError.LoginPermissionRequired(info: "查看的内容需要登录")
            }
            if response.response?.url?.path == "/2fa" {
                throw APIError.needs2FA(info: "需要两步验证")
            }
            if let jiHTML = Ji(htmlData: response.data) {
                return jiHTML
            } else {
                throw APIError.Error(info: "Failed to serialize response")
            }
        })
    }
    
    // 将Response 转成 Ji Model Array
    func mapResponseToJiArray<T: HTMLModelArrayProtocol>(_ type: T.Type) -> Observable<[T]> {
        return filterJiResponseError().map({ ji in
            return type.createModelArray(ji: ji) as! [T]
        })
    }
    func mapResponseToJiModel<T: HTMLModelProtocol>(_ type: T.Type) -> Observable<T> {
        return filterJiResponseError().map({ ji in
            return type.createModel(ji: ji) as! T
        })
    }
    
    /**
     在将 Ji 对象转换成 Model 之前，可能需要先获取 Ji 对象的数据
     例如获取我的收藏帖子列表时，除了需要将 Ji 数据 转换成 TopicListModel
     还需要额外获取最大页码,这个最大页面就从这个方法中获得
     */
    
    func getJiDataFirst(handler: @escaping ((_ ji: Ji) -> Void)) -> Observable<Ji> {
        return filterJiResponseError().map({ ji -> Ji in
            handler(ji)
            return ji
        })
    }
}

extension Observable where Element: Ji {
    func mapResponseToJiArray<T: HTMLModelArrayProtocol>(_ type: T.Type) -> Observable<[T]> {
        return map { ji in
            return type.createModelArray(ji: ji) as! [T]
        }
    }
    
    func mapResponseToJiModel<T: HTMLModelProtocol>(_ type: T.Type) -> Observable<T> {
        return map { ji in
            return type.createModel(ji: ji) as! T
        }
    }
}

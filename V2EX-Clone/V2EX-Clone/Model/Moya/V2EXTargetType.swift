//
//  V2EXTargetType.swift
//  V2EX-Clone
//
//  Created by yidao on 2019/9/9.
//  Copyright © 2019 yidao. All rights reserved.
//

import UIKit
import Moya
import Result
import RxSwift

private var retainProviders: [String: Any] = [:]

protocol V2EXTargetType: TargetType {
    var parameters: [String: Any]? { get }
}

extension V2EXTargetType {
    var baseURL: URL {
        return URL(string: "https://www.v2ex.com")!
    }
    
    var method: Moya.Method {
        return .get
    }
    
    /// Provides stub data for use in testing.
    var sampleData: Data {
        return Data()
    }
    
    /// The type of HTTP task to be performed.
    var task: Task {
        return requestTaskWithParameters
    }
    
    var requestTaskWithParameters: Task {
        get {
            var defaultParameters: [String: Any] = [:]
            if let parameters = self.parameters {
                for (key, value) in parameters {
                    defaultParameters[key] = value
                }
            }
            return Task.requestParameters(parameters: defaultParameters, encoding: URLEncoding())
        }
    }
    
    /// The type of validation to perform on the request. Default is `.none`.
    var validationType: ValidationType {
        return .successCodes
    }
    
    /// The headers to be used in the request.
    var headers: [String: String]? {
        return MOBILE_CLIENT_HEADERS
    }
    
    static var networkActivityPlugin: PluginType {
        return NetworkActivityPlugin { (change, type) in
            switch change {
            case .began:
                UIApplication.shared.isNetworkActivityIndicatorVisible = true
            case .ended:
                UIApplication.shared.isNetworkActivityIndicatorVisible = false
            }
        }
    }
    
    static var provider: RxSwift.Reactive<MoyaProvider<Self>> {
        let key = "\(Self.self)"
        if let provider = retainProviders[key] as? RxSwift.Reactive<MoyaProvider<Self>> {
            return provider
        }
        let provider = Self.weakProvider
        retainProviders[key] = provider
        return provider
    }

    // 不被全局持有的Provider，使用时，需要持有它，否则立即释放，终止请求
    static var weakProvider: RxSwift.Reactive<MoyaProvider<Self>> {
        var plugins: [PluginType] = [networkActivityPlugin]
        #if DEBUG
        plugins.append(LogPlugin())
        #endif
        let provider = MoyaProvider<Self>(plugins: plugins)
        return provider.rx
    }
}

extension RxSwift.Reactive where Base: MoyaProviderType {
    public func requestAPI(_ token: Base.Target, callbackQueue: DispatchQueue? = nil) -> Observable<Response> {
        return self.request(token, callbackQueue: callbackQueue).asObservable()
    }
}

private class LogPlugin: PluginType {
    func willSend(_ request: RequestType, target: TargetType) {
        print("\n-------------------\n准备请求: \(target.path)")
        print("请求方式: \(target.method.rawValue)")
        if let params =  (target as? V2EXTargetType)?.parameters {
            print(params)
        }
        print("\n")
    }
    
    func didReceive(_ result: Result<Response, MoyaError>, target: TargetType) {
        print("\n-------------------\n请求结束: \(target.path)")
        if let data = result.value?.data, let result = String(data: data, encoding: .utf8) {
            print("请求结果: \(result)")
        }
        print("\n")
    }
}

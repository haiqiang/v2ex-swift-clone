//
//  NotificationsModel.swift
//  V2EX-Clone
//
//  Created by yidao on 2019/9/24.
//  Copyright © 2019 yidao. All rights reserved.
//

import UIKit
import Ji
import Alamofire

class NotificationsModel: NSObject, BaseHTMLModelProtocol {
    var avata: String?
    var userName: String?
    var title: String?
    var date: String?
    var reply: String?
    
    var topicId: String?
    
    required init(rootNode: JiNode) {
        self.avata = rootNode.xPath("./table/tr/td[1]/a/img[@class='avatar']").first?["src"]
        self.userName = rootNode.xPath("./table/tr/td[2]/span[1]/a[1]/strong").first?.content
        self.title = rootNode.xPath("./table/tr/td[2]/span[1]").first?.content
        self.date = rootNode.xPath("./table/tr/td[2]/span[2]").first?.content
        self.reply = rootNode.xPath("./table/tr/td[2]/div[@class='payload']").first?.content
        
        if let node = rootNode.xPath("./table/tr/td[2]/span[1]/a[2]").first {
            var topicIdURL = node["href"]
            
            if var id = topicIdURL {
                if let range = id.range(of: "/t/") {
                    id.replaceSubrange(range, with: "")
                }
                if let range = id.range(of: "#") {
                    topicIdURL = String(id[..<range.lowerBound])
                }
            }
            self.topicId = topicIdURL
        }
    }
}

extension NotificationsModel {
    class func getNotifications(_ completionHandler: ((V2ValueResponse<[NotificationsModel]>) -> Void)?) {
        Alamofire.request(V2EXURL + "notifications", method: .get, parameters: nil, encoding: URLEncoding(), headers: MOBILE_CLIENT_HEADERS).responseJiHTML { (response) in
            var resultArray: [NotificationsModel] = []
            
            if let jiHTML = response.result.value {
                if let aRootNode = jiHTML.xPath("//*[@id='Wrapper']/div/div[@class='box']/div[attribute::id]") {
                    for node in aRootNode {
                        let notice = NotificationsModel(rootNode: node)
                        resultArray.append(notice)
                    }
                    
                    V2User.sharedInstance.getNotificationsCount(jiHTML.rootNode!)
                }
            }
            
            let t = V2ValueResponse<[NotificationsModel]>(value: resultArray, success: response.result.isSuccess)
            completionHandler?(t)
        }
    }
}

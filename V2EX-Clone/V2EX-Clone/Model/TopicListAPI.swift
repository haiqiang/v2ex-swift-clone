//
//  TopicListAPI.swift
//  V2EX-Clone
//
//  Created by yidao on 2019/9/26.
//  Copyright © 2019 yidao. All rights reserved.
//

import UIKit

enum TopicListAPI {
    // 获取首页列表
    case topicList(tab: String?, page: Int)
    // 获取我的收藏帖子列表
    case favoriteList(page: Int)
    // 获取节点主题列表
    case nodeTopicList(nodeName: String, page: Int)
}

extension TopicListAPI: V2EXTargetType {
    var parameters: [String : Any]? {
        switch self {
        case let .topicList(tab, page):
            if tab == "all" && page > 0 {
                return ["p" : page]
            }
            return ["tab": tab ?? "all"]
        case let .favoriteList(page):
            return ["p" : page]
        case let .nodeTopicList(_, page):
            return ["p" : page]
        }
    }
    
    var path: String {
        switch self {
        case let .topicList(tab, page):
            if tab == "all" && page > 0 {
                return "/recent"
            }
            return "/"
        case .favoriteList:
            return "/my/topics"
        case let .nodeTopicList(nodeName, _):
            return "/go/\(nodeName)"
        }
    }
}

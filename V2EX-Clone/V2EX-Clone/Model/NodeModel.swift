//
//  NodeModel.swift
//  V2EX-Clone
//
//  Created by yidao on 2019/9/23.
//  Copyright © 2019 yidao. All rights reserved.
//

import UIKit
import Ji
import Alamofire

class NodeModel: NSObject, BaseHTMLModelProtocol {
    var nodeId: String?
    var nodeName: String?
    var width: CGFloat = 0
    
    override init() {
        super.init()
    }
    
    required init(rootNode: JiNode) {
        self.nodeName = rootNode.content
        if let nodeName = self.nodeName {
            let rect = (nodeName as NSString).boundingRect(with: CGSize(width: kScreenWidth, height: 15), options: .usesLineFragmentOrigin, attributes: [NSAttributedString.Key.font: UIFont.systemFont(ofSize: 15.0)], context: nil)
            self.width = rect.width
        }
        
        if var href = rootNode["href"] {
            if let range = href.range(of: "/go/") {
                href.replaceSubrange(range, with: "")
                self.nodeId = href
            }
        }
    }
}

class NodeGroupModel: NSObject, BaseHTMLModelProtocol {
    var groupName: String?
    var childrenRows: [[Int]] = [[]]
    var children: [NodeModel] = []
    
    required init(rootNode: JiNode) {
        self.groupName = rootNode.xPath("./td[1]/span").first?.content
        for node in rootNode.xPath("./td[2]/a") {
            self.children.append(NodeModel(rootNode: node))
        }
    }
    
    class func getNodes(_ completionHandler: ((V2ValueResponse<[NodeGroupModel]>) -> Void)?) {
        Alamofire.request(V2EXURL, method: .get, parameters: nil, encoding: URLEncoding(), headers: MOBILE_CLIENT_HEADERS).responseJiHTML { (response) in
            var groupArray: [NodeGroupModel] = []
            if let jiHTML = response.result.value {
                if let nodes = jiHTML.xPath("//*[@id='Wrapper']/div/div[@class='box'][last()]/div/table/tr") {
                    for rootNode in nodes {
                        let group = NodeGroupModel(rootNode: rootNode)
                        groupArray.append(group)
                    }
                }
                completionHandler?(V2ValueResponse(value: groupArray, success: true))
                return
            }
            completionHandler?(V2ValueResponse(success: false, message: "获取失败"))
        }
    }
    
}

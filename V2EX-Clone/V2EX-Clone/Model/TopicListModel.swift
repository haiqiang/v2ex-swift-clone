//
//  TopicListModel.swift
//  V2EX-Clone
//
//  Created by yidao on 2019/9/18.
//  Copyright © 2019 yidao. All rights reserved.
//

import Foundation
import Ji
import YYText
import Alamofire

class TopicListModel: NSObject, HTMLModelArrayProtocol {
    class func createModelArray(ji: Ji) -> [Any] {
        var resultArray: [TopicListModel] = []
        if let aRootNode = ji.xPath("//body/div[@id='Wrapper']/div[@class='content']/div[@class='box']/div[@class='cell item']") {
            for aNode in aRootNode {
                let topic = TopicListModel(rootNode: aNode)
                resultArray.append(topic)
            }
            
            // 更新通知数量
            V2User.sharedInstance.getNotificationsCount(ji.rootNode!)
            
            DispatchQueue.main.async {
                if let aRootNode = ji.xPath("//body/div[@id='Wrapper']/div[@class='content']/div[@class='box']/div[@class='inner']/a[@href='/mission/daily']")?.first {
                    if aRootNode.content == "领取今日的登录奖励" {
                        UserModel.dailyRedeem()
                    }
                }
            }
        }
        return resultArray
    }
    
    var topicId: String?
    var avatar: String?
    var nodeName: String?
    var userName: String?
    var topicTitle: String?
    var topicTitleAttributedString: NSMutableAttributedString?
    var topicTitleLayout: YYTextLayout?

    var date: String?
    var lastReplyUserName: String?
    var replies: String?
    var hits: String?
    
    override init() {
        super.init()
    }
    
    init(rootNode: JiNode) {
        super.init()
        
        self.avatar = rootNode.xPath("./table/tr/td[1]/a[1]/img[@class='avatar']").first?["src"]
        self.nodeName = rootNode.xPath("./table/tr/td[3]/span[1]/a[1]").first?.content
        self.userName = rootNode.xPath("./table/tr/td[3]/span[1]/strong[1]/a[1]").first?.content

       
        let node = rootNode.xPath("./table/tr/td[3]/span[2]/a[1]").first
        self.topicTitle = node?.content
        setupTitleLayout()
        
        var topicIdURL = node?["href"]
        if var id = topicIdURL {
            if let range = id.range(of: "/t/") {
                id.replaceSubrange(range, with: "")
            }
            if let range = id.range(of: "#") {
                topicIdURL = String(id[..<range.lowerBound])
            }
        }
        self.topicId = topicIdURL
        
        self.date = rootNode.xPath("./table/tr/td[3]/span[3]").first?.content
        self.lastReplyUserName = rootNode.xPath("./table/tr/td[3]/span[3]/strong[1]/a[1]").first?.content
        self.replies  = rootNode.xPath("./table/tr/td[4]/a[1]").first?.content
    }
    
    func setupTitleLayout() {
        if let title = self.topicTitle {
            self.topicTitleAttributedString = NSMutableAttributedString(string: title,
                                                                        attributes: [NSAttributedString.Key.font: UIFont.systemFont(ofSize: 17),
                                                                    NSAttributedString.Key.foregroundColor:V2EXColor.colors.v2_topicListTitleColor,
                ])
            self.topicTitleAttributedString?.yy_lineSpacing = 3
            
            self.themeChangedHandler = { [weak self] style in
                guard let self = self else {
                    return
                }
                if let str = self.topicTitleAttributedString {
                    str.yy_color = V2EXColor.colors.v2_topicListTitleColor
                    self.topicTitleLayout = YYTextLayout(containerSize: CGSize(width: kScreenWidth - 24.0, height: 9999), text: str)
                }
            }
        }
    }
}

class FavoriteListModel: TopicListModel {
    override class func createModelArray(ji: Ji) -> [Any] {
        var resultArray:[FavoriteListModel] = []
        if let aRootNode = ji.xPath("//*[@class='cell item']"){
            for aNode in aRootNode {
                let topic = FavoriteListModel(rootNode: aNode)
                resultArray.append(topic);
            }
        }
        return resultArray
    }
    
    override init(rootNode: JiNode) {
        super.init()
   
        self.avatar = rootNode.xPath("./table/tr/td[1]/a[1]/img[@class='avatar']").first?["src"]
        self.nodeName = rootNode.xPath("./table/tr/td[3]/span[2]/a[1]").first?.content
        self.userName = rootNode.xPath("./table/tr/td[3]/span[2]/strong[1]/a").first?.content
        
        let node = rootNode.xPath("./table/tr/td[3]/span/a[1]").first
        self.topicTitle = node?.content
        self.setupTitleLayout()
        
        var topicIdUrl = node?["href"];
        
        if var id = topicIdUrl {
            if let range = id.range(of: "/t/") {
                id.replaceSubrange(range, with: "");
            }
            if let range = id.range(of: "#") {
                topicIdUrl = String(id[..<range.lowerBound])
            }
        }
        self.topicId = topicIdUrl
        
        
        let date = rootNode.xPath("./table/tr/td[3]/span[2]").first?.content
        if let date = date {
            let array = date.components(separatedBy: "•")
            if array.count == 4 {
                self.date = array[3].trimmingCharacters(in: NSCharacterSet.whitespaces)
                
            }
        }
        
        self.lastReplyUserName = rootNode.xPath("./table/tr/td[3]/span[2]/strong[2]/a[1]").first?.content
        self.replies = rootNode.xPath("./table/tr/td[4]/a[1]").first?.content
    }
}


class NodeTopicListModel: TopicListModel {
    
    override class func createModelArray(ji: Ji) -> [Any] {
        var resultArray:[NodeTopicListModel] = []
        if let aRootNode = ji.xPath("//*[@id='Wrapper']/div[@class='content']/div[@class='box']/div[@class='cell']"){
            for aNode in aRootNode {
                let topic = NodeTopicListModel(rootNode: aNode)
                resultArray.append(topic);
            }
            
            //更新通知数量
            V2User.sharedInstance.getNotificationsCount(ji.rootNode!)
        }
        return resultArray
    }
    
    override init(rootNode: JiNode) {
        super.init(rootNode: rootNode)
        
        self.avatar = rootNode.xPath("./table/tr/td[1]/a[1]/img[@class='avatar']").first?["src"]
        self.userName = rootNode.xPath("./table/tr/td[3]/span[2]/strong").first?.content
        
        let node = rootNode.xPath("./table/tr/td[3]/span/a[1]").first
        self.topicTitle = node?.content
        self.setupTitleLayout()
        
        var topicIdUrl = node?["href"];
        
        if var id = topicIdUrl {
            if let range = id.range(of: "/t/") {
                id.replaceSubrange(range, with: "");
            }
            if let range = id.range(of: "#") {
                topicIdUrl = String(id[..<range.lowerBound])
            }
        }
        self.topicId = topicIdUrl
        
        
        self.hits = rootNode.xPath("./table/tr/td[3]/span[last()]/text()").first?.content
        if let hits = self.hits {
            self.hits = String(hits[hits.index(hits.startIndex, offsetBy: 5)...])
        }
        
        self.replies  = rootNode.xPath("./table/tr/td[4]/a[1]").first?.content
    }
}

extension TopicListModel {
    /**
     收藏节点
     */
    class func favorite(_ nodeId: String, type: NSInteger) {
        V2User.sharedInstance.getOnce { (response) in
            if(response.success){
                let action = type == 0 ? "favorite/node/" : "unfavorite/node/"
                let url = V2EXURL + action + nodeId + "?once=" + V2User.sharedInstance.once!
                
                Alamofire.request(url, method: .get, parameters: nil, encoding: URLEncoding(), headers: MOBILE_CLIENT_HEADERS).responseJiHTML(complectionHandler: { (response) in
                    
                })
            }
        }
    }
}


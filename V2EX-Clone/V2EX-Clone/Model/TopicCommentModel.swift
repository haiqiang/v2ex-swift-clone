//
//  TopicCommentModel.swift
//  V2EX-Clone
//
//  Created by yidao on 2019/9/24.
//  Copyright © 2019 yidao. All rights reserved.
//

import UIKit
import YYText
import Ji
import Kingfisher
import Alamofire

class TopicCommentModel: NSObject, BaseHTMLModelProtocol {
    var replyId:String?
    var avata: String?
    var userName: String?
    var date: String?
    var comment: String?
    var favorites: Int = 0
    var textLayout:YYTextLayout?
    var images:NSMutableArray = NSMutableArray()
    //楼层
    var number:Int = 0
    
    var textAttributedString: NSMutableAttributedString?
    
    required init(rootNode: JiNode) {
        super.init()
        
        let id = rootNode.xPath("table/tr/td[3]/div[1]/div[attribute::id]").first?["id"]
        if let id = id {
            if id.hasPrefix("thank_area_") {
                self.replyId = id.replacingOccurrences(of: "thank_area_", with: "")
            }
        }
        
        self.avata = rootNode.xPath("table/tr/td[1]/img").first?["src"]
        
        self.userName = rootNode.xPath("table/tr/td[3]/strong/a").first?.content
        
        self.date = rootNode.xPath("table/tr/td[3]/span").first?.content
        
        if let str = rootNode.xPath("table/tr/td[3]/div[@class='fr']/span").first?.content  , let no =  Int(str){
            self.number = no;
        }
        
        if let favorite = rootNode.xPath("table/tr/td[3]/span[2]").first?.content {
            let array = favorite.components(separatedBy: " ")
            if array.count == 2 {
                if let i = Int(array[1]){
                    self.favorites = i
                }
            }
        }
        
        // 构造评论内容
        let commentAttributedString: NSMutableAttributedString = NSMutableAttributedString(string: "")
        let nodes = rootNode.xPath("table/tr/td[3]/div[@class='reply_content']/node()")
        self.preformAttributedString(commentAttributedString, nodes: nodes)
        let textContainer = YYTextContainer(size: CGSize(width: kScreenWidth - 24.0, height: 9999))
        self.textLayout = YYTextLayout(container: textContainer, text: commentAttributedString)
        self.textAttributedString = commentAttributedString
    }
    
    func preformAttributedString(_ commentAttributedString: NSMutableAttributedString, nodes: [JiNode]) {
        for element in nodes {
            let urlHandler: (_ attributedString: NSMutableAttributedString, _ url: String, _ url: String) -> () = { (attributedString, title, url) in
                let attr = NSMutableAttributedString(string: title, attributes: [NSAttributedString.Key.font: 14.0 * CGFloat(V2Style.sharedInstance.fontScale)])
                attr.yy_setTextHighlight(NSRange(location: 0, length: title.count), color: V2EXColor.colors.v2_linkColor, backgroundColor: UIColor(white: 0.95, alpha: 1), userInfo: ["url": url], tapAction: { (view, text, range, rect) in
                    if let highlight = text.yy_attribute(YYTextHighlightAttributeName, at: UInt(range.location)), let url = (highlight as AnyObject).userInfo["url"] as? String {
                        AnalyzeURLHelper.Analyze(url)
                    }
                }, longPressAction: nil)
                commentAttributedString.append(attr)
            }
            
            // 普通文本
            if element.name == "text", let content = element.content {
                commentAttributedString.append(NSMutableAttributedString(string: content, attributes: [NSAttributedString.Key.font: 14.0 * V2Style.sharedInstance.fontScale, NSAttributedString.Key.foregroundColor: V2EXColor.colors.v2_topicListTitleColor]))
                commentAttributedString.yy_lineSpacing = 5
            }
            
            // 图片
            else if element.name == "img", let imageURL = element["src"] {
                let font = UIFont.systemFont(ofSize: (CGFloat(14.0 * V2Style.sharedInstance.fontScale)))
                let image = V2CommentAttachmentImage()
                image.imageURL = imageURL
                let imageAttributedString = NSMutableAttributedString.yy_attachmentString(withContent: image, contentMode: .scaleAspectFit, attachmentSize: CGSize(width: 80, height: 80), alignTo:font,  alignment: .bottom)
                commentAttributedString.append(imageAttributedString)
                image.index = self.images.count
                self.images.add(imageURL)
            }
            
            else if element.name == "a", let content = element.content, let url = element["href"] {
                // 递归处理所有子节点，主要是处理下a标签下包含的img标签
                let subNodes = element.xPath("./node()")
                if subNodes.first?.name != "text" && subNodes.count > 0 {
                    self.preformAttributedString(commentAttributedString, nodes: subNodes)
                }
                if content.count > 0 {
                    urlHandler(commentAttributedString, content, url)
                }
            }
            
            else if element.name == "br" {
                commentAttributedString.yy_appendString("\n")
            }
            
            else if element.children.first?.name == "iframe", let src = element.children.first?["src"], src.count > 0 {
                urlHandler(commentAttributedString, src, src)
            }
            
            else if let content = element.content { // 其他
                commentAttributedString.append(NSMutableAttributedString(string: content, attributes: [NSAttributedString.Key.foregroundColor: V2EXColor.colors.v2_topicListTitleColor]))

            }
            
        }
    }
}

// Mark: - Request
extension TopicCommentModel {
    class func replyWithTopicId(_ topic:TopicDetailModel, content: String, completionHandler: @escaping ((V2Response) -> Void)) {
        let requestPath = "t/" + topic.topicId!
        let url = V2EXURL + requestPath
        
        V2User.sharedInstance.getOnce(url) { (response) in
            if response.success {
                let params = [
                    "content": content,
                    "once": V2User.sharedInstance.once!
                ] as [String: Any]
                
                Alamofire.request(url, method: .post, parameters: params, encoding: URLEncoding(), headers: MOBILE_CLIENT_HEADERS).responseJiHTML(complectionHandler: { (response) in
                    let url = response.response?.url
                    if url?.path.hasSuffix(requestPath) == true && url?.fragment?.hasPrefix("reply") == true {
                        completionHandler(V2Response(success: true))
                    }
                    else if let problems = response.result.value?.xPath("//*[@class='problem']/ul/li") {
                        let problemstr = problems.map{ $0.content ?? "回帖失败" }.joined(separator: "\n")
                        completionHandler(V2Response(success: false, message: problemstr))
                    }
                    else {
                        completionHandler(V2Response(success: false, message: "请求失败"))
                    }
                    
                    // 不管成功还是失败，更新一下once
                    if let jiHTML = response.result.value {
                        V2User.sharedInstance.once = jiHTML.xPath("//*[@name='once'][1]")?.first?["value"]
                    }
                })
            } else {
                completionHandler(V2Response(success: false, message: "获取once失败,请重试"))
            }
        }
    }
    
    
    class func replyThankWithReplyId(_ replyId: String, token: String, completionHandler: @escaping ((V2Response) -> Void)) {
        _ = TopicAPI.provider.requestAPI(.thankReply(replyId: replyId, once: token)).filterResponseError().subscribe(onNext: { (response) in
            if response["success"].boolValue {
                completionHandler(V2Response(success: true))
            } else {
                completionHandler(V2Response(success: false))
            }
        }, onError: { (error) in
            completionHandler(V2Response(success: false))
        })
    }
}


extension TopicCommentModel {
    /**
     用某一条评论，获取和这条评论有关的所有评论
     -parameter array: 所有的评论数组
     -parameter firstComment: 这条评论
     
     -returns: 某一条评论相关的评论，里面包含他自己
     */
    
    class func getRelevantCommentsInArray(_ allCommentsArray: [TopicCommentModel], firstComment: TopicCommentModel) -> [TopicCommentModel] {
        var relevantComments: [TopicCommentModel] = []
        var users = getUsersOfComment(firstComment)
        // 当前绘画所有相关的用户（B 回复 A，C 回复 B, D 回复C, 查看D的对话时，D C B 为相关联用户）
        var relateUsers: Set<String> = users
        for comment in allCommentsArray {
            // 被回复人所@的人也加进对话，但是不递归s获取所有关系链
            if let username = comment.userName, users.contains(username) {
                let commentUsers = getUsersOfComment(comment)
                relateUsers = relateUsers.union(commentUsers)
            }
            
            // 只找到点击的位置，之后就不找了
            if comment == firstComment {
                break
            }
        }
        
        users.insert(firstComment.userName!)
        
        for comment in allCommentsArray {
            // 判断评论中是否@的所有用户和绘画相关联的用户无关，是的话则证明这条评论时别人讲的，不属于当前对话
            let commentUsers = getUsersOfComment(comment)
            let intersetUsers = commentUsers.intersection(relateUsers)
            if commentUsers.count > 0 && intersetUsers.count <= 0 {
                continue
            }
            
            if let username = comment.userName {
                if users.contains(username) {
                    relevantComments.append(comment)
                }
            }
            
            if comment == firstComment {
                break
            }
        }
        
        return relevantComments
    }

    // 获取评论中 @ 了多少用户
    class func getUsersOfComment(_ comment: TopicCommentModel) -> Set<String> {
        // 获取所有YYTextHighlight,用来获取这条评论@多少用户
        var textHighlights: [YYTextHighlight] = []
        comment.textAttributedString!.enumerateAttribute(NSAttributedString.Key(YYTextHighlightAttributeName), in: NSRange(location: 0, length: comment.textAttributedString!.length), options: []) { (attribute, range, stop) in
            if let highlight = attribute as? YYTextHighlight {
                textHighlights.append(highlight)
            }
        }
        
        // 获取这条评论@了多少用户
        var users: Set<String> = []
        for highlight in textHighlights {
            if let url  = highlight.userInfo?["url"] as? String {
                let result = AnalyzURLResultType(url: url)
                if case .member(let member) = result {
                    users.insert(member.username)
                }
            }
        }
        return users
    }
}

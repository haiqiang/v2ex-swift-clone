//
//  MemberModel.swift
//  V2EX-Clone
//
//  Created by yidao on 2019/9/18.
//  Copyright © 2019 yidao. All rights reserved.
//

import Foundation
import Ji
import Alamofire

enum BlockState {
    case blocked
    case unBlocked
}

enum FollowState {
    case followed
    case unFollowed
}

class MemberModel: NSObject {
    var avata: String?
    var userId:String?
    var userName:String?
    var introduce:String?
    
    var blockState: BlockState = .unBlocked
    var followState: FollowState = .unFollowed
    var userToken:String?
    var blockToken:String?
    
    var topics:[MemberTopicsModel] = []
    var replies:[MemberRepliesModel] = []
 
    required init(rootNode: JiNode) {
        self.avata = rootNode.xPath("./td[1]/img").first?["src"]
        self.userName = rootNode.xPath("./td[3]/h1").first?.content
        self.introduce = rootNode.xPath("./td[3]/span[1]").first?.content
        // follow状态
        if let followNode = rootNode.xPath("./td[3]/div[1]/input[1]").first
            , let followStateClickUrl = followNode["onclick"] , followStateClickUrl.count > 0 {
            var index = followStateClickUrl.range(of: "'/")
            
            var url = followStateClickUrl[index!.upperBound...]
            let endIndex = url.range(of:"'", options: .backwards, range: nil, locale: nil)
            url = url[..<url.index(endIndex!.upperBound, offsetBy: -1)]
            
            if url.hasPrefix("follow"){
                self.followState = .unFollowed
            }
            else{
                self.followState = .followed
            }
            
            index = url.range(of: "?once=")
            self.userToken = String(url[index!.upperBound...])
            
            //UserId
            let startIndex = url.range(of:"/", options: .backwards, range: nil, locale: nil)
            self.userId = String(url[startIndex!.upperBound ..< index!.lowerBound])
            
        }
        
        // block状态
        if let followNode = rootNode.xPath("./td[3]/div[1]/input[2]").first
            , let followStateClickUrl = followNode["onclick"] , followStateClickUrl.count > 0 {
            
            var index = followStateClickUrl.range(of: "'/")
            guard index != nil else {
                return
            }
            var url = followStateClickUrl[index!.upperBound...]
            
            if url.hasPrefix("block"){
                self.blockState = .unBlocked
            }
            else{
                self.blockState = .blocked
            }
            
            
            let endIndex = url.range(of:"'", options: .backwards, range: nil, locale: nil)
            guard endIndex != nil else {
                return
            }
            url = url[..<url.index(endIndex!.upperBound, offsetBy: -1)]
            index = url.range(of: "?t=")
            self.blockToken = String(url[index!.upperBound...])
            
        }
    }

}

class MemberTopicsModel: TopicListModel {
    override init(rootNode: JiNode) {
        super.init(rootNode: rootNode)
        
        self.nodeName = rootNode.xPath("./table/tr/td[1]/span[1]/a[1]").first?.content
        self.userName = rootNode.xPath("./table/tr/td[1]/span[1]/strong[1]/a[1]")[0].content
        
        let node = rootNode.xPath("./table/tr/td[1]/span[2]/a[1]")[0]
        self.topicTitle = node.content
        
        var topicIdUrl = node["href"];
        
        if var id = topicIdUrl {
            if let range = id.range(of: "/t/") {
                id.replaceSubrange(range, with: "")
            }
            if let range = id.range(of: "#") {
                id = String(id[..<range.lowerBound])
                topicIdUrl = id
            }
        }
        self.topicId = topicIdUrl
        
        
        self.date = rootNode.xPath("./table/tr/td[1]/span[3]")[0].content
        self.lastReplyUserName = rootNode.xPath("./table/tr/td[1]/span[3]/strong[1]/a[1]").first?.content
        self.replies  = rootNode.xPath("./table/tr/td[2]/a[1]").first?.content
    }
}


class MemberRepliesModel: NSObject, BaseHTMLModelProtocol {
    var topicId: String?
    var date: String?
    var title: String?
    var reply: String?
    
    required init(rootNode: JiNode) {
        let node = rootNode.xPath("./table/tr/td[1]/span[1]/a[1]")[0]
        self.title = node.content
        
        var topicIdUrl = node["href"];
        
        if var id = topicIdUrl {
            if let range = id.range(of: "/t/") {
                id.replaceSubrange(range, with: "")
            }
            if let range = id.range(of: "#") {
                id = String(id[..<range.lowerBound])
                topicIdUrl = id
            }
        }
        self.topicId = topicIdUrl
        self.date = rootNode.xPath("./table/tr/td/div/span")[0].content
    }
}

extension MemberModel {
    class func follow(_ userId: String, userToken: String, type: FollowState, completionHandler: ((V2Response) -> Void)?) {
        let action = type == .followed ? "follow/" : "unfollow/"
        let url = V2EXURL + action + userId + "?once=" + userToken
        Alamofire.request(url, method: .get, parameters: nil, encoding: URLEncoding(), headers: MOBILE_CLIENT_HEADERS).responseJiHTML { (response) in
            completionHandler?(V2Response(success: response.result.isSuccess))
        }
    }
    
    class func block(_ userId:String,
                     userToken:String,
                     type:BlockState,
                     completionHandler: ((V2Response) -> Void)?
        ){
        let action = type == .blocked ? "block/" : "unblock/"
        let url = V2EXURL + action + userId + "?t=" + userToken
        Alamofire.request(url, method: .get, parameters: nil, encoding: URLEncoding(), headers: MOBILE_CLIENT_HEADERS).responseJiHTML { (response) in
            completionHandler?(V2Response(success: response.result.isSuccess))
        }
    }
    
    class func getMemberInfo(_ username:String , completionHandler: ((V2ValueResponse<MemberModel>) -> Void)?) {
        let url = V2EXURL + "member/" + username
        Alamofire.request(url, method: .get, parameters: nil, encoding: URLEncoding(), headers: MOBILE_CLIENT_HEADERS).responseJiHTML { (response) in
            if let jiHTML = response.result.value {
                if let aRootNode = jiHTML.xPath("//*[@id='Wrapper']/div/div[1]/div[1]/table/tr")?.first{
                    
                    let member = MemberModel(rootNode: aRootNode)
                    if let rootNodes = jiHTML.xPath("//*[@class='cell item']") {
                        for rootNode  in rootNodes {
                            member.topics.append(MemberTopicsModel(rootNode: rootNode))
                        }
                    }
                    
                    if let rootNodes = jiHTML.xPath("//*[@class='dock_area']") {
                        for rootNode in rootNodes {
                            let replyModel = MemberRepliesModel(rootNode:rootNode)
                            
                            if  let replyNode = rootNode.nextSibling {
                                replyModel.reply = replyNode.xPath("./div").first?.content
                            }
                            
                            member.replies.append(replyModel)
                        }
                    }
                    
                    completionHandler?(V2ValueResponse(value: member, success: true))
                    return
                }
            }
            completionHandler?(V2ValueResponse(success: false))
        }
    }
}

//
//  TopicAPI.swift
//  V2EX-Clone
//
//  Created by yidao on 2019/9/25.
//  Copyright © 2019 yidao. All rights reserved.
//

import Foundation
import Moya

enum TopicAPI {
    // 感谢回复
    case thankReply(replyId: String, once: String)
    case thankTopic(topicId: String, once: String)
}

extension TopicAPI: V2EXTargetType {
 
    var path: String {
        switch self {
        case let .thankReply(replyId, _):
            return "/thank/reply/\(replyId)"
        case let .thankTopic(replyId, _):
            return "/thank/topic/\(replyId)"
        }
    }
    
    var method: Moya.Method {
        switch self {
        case .thankReply:
            return .post
        case .thankTopic:
            return .post
        }
    }
    
    var parameters: [String : Any]? {
        switch self {
        case let .thankReply(_, once):
            return ["once": once]
        case let .thankTopic(_, once):
            return ["once": once]
        }
    }
}

//
//  V2Response.swift
//  V2EX-Clone
//
//  Created by yidao on 2019/9/9.
//  Copyright © 2019 yidao. All rights reserved.
//

import Foundation

enum ErrorCode: Int {
    case none = 0
    case twoFA
}

class V2Response: NSObject {
    var success: Bool = false
    var message: String = "No message"
    
    init(success: Bool, message: String?) {
        super.init()
        
        self.success = success
        if let msg = message {
            self.message = msg
        }
    }
    
    init(success: Bool) {
        super.init()
        
        self.success = success
    }
    
}

class V2ValueResponse<T>: V2Response {
    var value: T?
    var code: ErrorCode = .none
    
    convenience init(value: T, success: Bool) {
        self.init(success: success)
        self.value = value
    }
    
    convenience init(value: T, success: Bool, message: String? = nil, code: ErrorCode = .none) {
        self.init(value: value, success: success)
        if let msg = message {
            self.message = msg
        }
        self.code = code
    }
}

//
//  UIColor+Extensions.swift
//  V2EX-Clone
//
//  Created by yidao on 2019/9/6.
//  Copyright © 2019 yidao. All rights reserved.
//

import UIKit

extension UIColor {
    class var random: UIColor {
        return UIColor.RGB(r: CGFloat(arc4random_uniform(255)) / 255.0, g: CGFloat(arc4random_uniform(255)) / 255.0, b: CGFloat(arc4random_uniform(255)) / 255.0)
    }
    
    class func RGB(r: CGFloat, g: CGFloat, b: CGFloat) -> UIColor {
        return self.RGBA(r: r, g: g, b: b, alpha: 1)
    }
    
    class func RGBA(r: CGFloat, g: CGFloat, b: CGFloat, alpha: CGFloat) -> UIColor {
        return UIColor(red: r / 255.0, green: g / 255.0, blue: b / 255.0, alpha: alpha)
    }
    
    class func createImageByColor(_ color: UIColor) -> UIImage {
        return UIColor.createImageByColor(color, size: CGSize(width: 1.0, height: 1.0))
    }
    
    class func createImageByColor(_ color: UIColor, size: CGSize) -> UIImage {
        let rect = CGRect(x: 0, y: 0, width: size.width, height: size.height)
        let context = UIGraphicsGetCurrentContext()
        context?.setFillColor(color.cgColor)
        context?.fill(rect)
        
        UIGraphicsBeginImageContext(rect.size)
        let theImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        return theImage!
    }
}




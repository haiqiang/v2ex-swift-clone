//
//  UIImageView+Extensions.swift
//  V2EX-Clone
//
//  Created by yidao on 2019/9/25.
//  Copyright © 2019 yidao. All rights reserved.
//

import UIKit
import Foundation
import Kingfisher

private var lastURLKey: Void?

extension UIImageView {
    public var fin_webURL: URL? {
        return objc_getAssociatedObject(self, &lastURLKey) as? URL
    }
    
    private func fin_setWebURL(_ url: Foundation.URL) {
        objc_setAssociatedObject(self, &lastURLKey, url, .OBJC_ASSOCIATION_RETAIN)
    }
    
    func fin_setImageWithUrl(_ url: Foundation.URL, placeholderImage: UIImage?, imageModificationClosure:((_ image: UIImage) -> UIImage)?) {
        self.image = placeholderImage
        let resoure = ImageResource(downloadURL: url)
        fin_setWebURL(url)
        KingfisherManager.shared.cache.retrieveImage(forKey: resoure.cacheKey, options: nil) { (image, cacheType) in
            if image != nil {
                dispatch_async_safely_main_queue {
                    self.image = image
                }
            } else {
                KingfisherManager.shared.downloader.downloadImage(with: resoure.downloadURL, retrieveImageTask: nil, options: nil, progressBlock: nil, completionHandler: { (image, error, imageURL, originalData) in
                    if let error = error , error.code == KingfisherError.notModified.rawValue {
                        KingfisherManager.shared.cache.retrieveImage(forKey: resoure.cacheKey, options: nil, completionHandler: { (cacheImage, cacheType) -> () in
                            self.fin_setImage(cacheImage!, imageURL: imageURL!)
                        })
                        return
                    }

                    if var image = image, let originalData = originalData {
                        if let img = imageModificationClosure?(image) {
                            image = img
                        }
                        
                        // 缓存图片
                        KingfisherManager.shared.cache.store(image, original: originalData, forKey: resoure.cacheKey, toDisk: true, completionHandler: nil)
                        self.fin_setImage(image, imageURL: imageURL!)

                    }
                    
                })
            }
        }
    }
    
    private func fin_setImage(_ image: UIImage, imageURL: URL) {
        dispatch_async_safely_main_queue {
            guard imageURL == self.fin_webURL else {
                return
            }
            self.image = image
        }
    }
}


func fin_defaultImageModification() -> ((_ image:UIImage) -> UIImage) {
    return { ( image) -> UIImage in
        let roundedImage = image.roundedCornerImageWithCornerRadius(3)
        return roundedImage
    }
}

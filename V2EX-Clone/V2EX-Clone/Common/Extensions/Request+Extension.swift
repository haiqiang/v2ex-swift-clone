//
//  Request+Extension.swift
//  V2EX-Clone
//
//  Created by yidao on 2019/9/9.
//  Copyright © 2019 yidao. All rights reserved.
//

import Foundation
import Alamofire
import Ji

extension Error {
    var localizedFailureReason: String? {
        return (self as NSError).localizedFailureReason
    }
}

extension DataRequest {
    enum ErrorCode: Int {
        case noData = 1
        case dataSerializationFailed = 2
    }
    
    static func newError(_ code: ErrorCode, failureReason: String) -> NSError {
        let errorDomain = "me.fin.v2ex.error"
        let userInfo = [NSLocalizedFailureReasonErrorKey: failureReason]
        let returnError = NSError(domain: errorDomain, code: code.rawValue, userInfo: userInfo)
        return returnError
    }
    
    static func JiHTMLResponseSerializer() -> DataResponseSerializer<Ji> {
        return DataResponseSerializer { (request, response, data, error) in
            guard error == nil else { return .failure(error!) }
            
            if response?.url?.path == "/signin" && request?.url?.path != "/signin"{
                // 跳转到登录页，则证明请求的内容需要登录
                let failReason = "查看的内容需要登录！"
                let error = newError(.dataSerializationFailed, failureReason: failReason)
                return .failure(error)
            }
            
            guard let validData = data else {
                return .failure(AFError.responseSerializationFailed(reason: .inputDataNil))
            }
            if let JiHTML = Ji(htmlData: validData) {
                return .success(JiHTML)
            }
            
            let failureReason = "parse failed to serialize response."
            let error = newError(.dataSerializationFailed, failureReason: failureReason)
            return .failure(error)
        }
    }
    
    @discardableResult
    public func responseJiHTML(queue: DispatchQueue? = nil, complectionHandler: @escaping (DataResponse<Ji>) -> Void) -> Self {
        return response(responseSerializer: Alamofire.DataRequest.JiHTMLResponseSerializer(), completionHandler: complectionHandler)
    }
}

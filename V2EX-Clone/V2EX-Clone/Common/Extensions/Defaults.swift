//
//  Defaults.swift
//  V2EX-Clone
//
//  Created by yidao on 2019/9/6.
//  Copyright © 2019 yidao. All rights reserved.
//

//外观模式 https://github.com/ochococo/Design-Patterns-In-Swift
import Foundation

final public class Defaults {
    private let defaults: UserDefaults
    
    init(defaults: UserDefaults = .standard) {
        self.defaults = defaults
    }
    
    subscript(key: String) -> Any? {
        get {
            return defaults.object(forKey: key)
        }
        set {
            return defaults.set(newValue, forKey: key)
        }
    }
}

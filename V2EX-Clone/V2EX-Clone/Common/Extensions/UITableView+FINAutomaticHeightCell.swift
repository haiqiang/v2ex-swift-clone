//
//  UITableView+FINAutomaticHeightCell.swift
//  V2EX-Clone
//
//  Created by yidao on 2019/9/18.
//  Copyright © 2019 yidao. All rights reserved.
//

import UIKit

private struct AssociatedKey {
    static var CellsIdentifier = "me.fin.cellsIdentifier"
    static var HeightsCacheIdentifier = "me.fin.heightsCacheIdentifier"
    static var finHeightCacheAbsendValue = CGFloat(-1)
}
extension UITableView {
    public func fin_heightForCellWithIdentifier<T: UITableViewCell>(_ identifier: String, configuration: ((_ cell: T) -> Void)?) -> CGFloat {
        if identifier.count <= 0 {
            return 0
        }
        
        let cell = self.fin_templateCellForReuseIdentifier(identifier)
        cell.prepareForReuse()
        
        if configuration != nil {
            configuration!(cell as! T)
        }
        
        var fittingSize = cell.systemLayoutSizeFitting(UIView.layoutFittingCompressedSize)
        if self.separatorStyle != .none {
            fittingSize.height += 1.0 / UIScreen.main.scale
        }
        return fittingSize.height
    }
    
    public func fin_heightForCellWithIdentifier<T: UITableViewCell>(_ identifier: T.Type, indexpath: IndexPath, configuration: ((_ cell: T) -> Void)?) -> CGFloat {
        let identifierStr = "\(identifier)"
        if identifierStr.count <= 0 {
            return 0
        }
        
        // Hit cache
        if self.fin_hasCachedHeightAtIndexPath(indexpath) {
            let height: CGFloat = self.fin_indexPathHeightCache![indexpath.section][indexpath.row]
            return height
        }
        
        let height = self.fin_heightForCellWithIdentifier(identifierStr, configuration: configuration)
        self.fin_indexPathHeightCache![indexpath.section][indexpath.row] = height
        return height
    }
    
    public func fin_reloadData() {
        self.fin_indexPathHeightCache = [[]]
        self.reloadData()
    }
    
    private func fin_hasCachedHeightAtIndexPath(_ indexpath: IndexPath) -> Bool {
        self.fin_buildHeightCachesAtIndexpathsIfNeeded([indexpath])
        let height = self.fin_indexPathHeightCache![indexpath.section][indexpath.row]
        return height >= 0
    }
    
    private func fin_buildHeightCachesAtIndexpathsIfNeeded(_ indexPaths: Array<IndexPath>) -> Void {
        if indexPaths.count == 0 {
            return
        }
        
        if self.fin_indexPathHeightCache == nil {
            self.fin_indexPathHeightCache = []
        }
        
        for indexPath in indexPaths {
            let cacheSectionCount = self.fin_indexPathHeightCache!.count
            if indexPath.section >= cacheSectionCount {
                for i in cacheSectionCount ... indexPath.section {
                    if i >= self.fin_indexPathHeightCache!.count {
                        self.fin_indexPathHeightCache!.append([])
                    }
                }
            }
            
            let cacheCount = self.fin_indexPathHeightCache![indexPath.section].count
            if indexPath.row >= cacheCount {
                for i in cacheCount ... indexPath.row {
                    if i >= self.fin_indexPathHeightCache![indexPath.section].count {
                        self.fin_indexPathHeightCache![indexPath.section].append(AssociatedKey.finHeightCacheAbsendValue)
                    }
                }
            }
        }
        
        
    }
    
    private func fin_templateCellForReuseIdentifier(_ identifier: String) -> UITableViewCell {
        assert(identifier.count > 0, "Expect a valid identifier - \(identifier)")

        if self.fin_templateCellsByIdentifiers == nil {
            self.fin_templateCellsByIdentifiers = [:]
        }
        
        var templateCell = self.fin_templateCellsByIdentifiers?[identifier] as? UITableViewCell
        if templateCell == nil {
            templateCell = self.dequeueReusableCell(withIdentifier: identifier)
            assert(templateCell != nil, "Cell must be registered to table view for identifier - \(identifier)")
            templateCell?.contentView.translatesAutoresizingMaskIntoConstraints = false
            self.fin_templateCellsByIdentifiers?[identifier] = templateCell
        }
        
        return templateCell!
    }
    
    private var fin_templateCellsByIdentifiers: [String: AnyObject]? {
        get {
            return objc_getAssociatedObject(self, &AssociatedKey.CellsIdentifier) as? [String: AnyObject]
        }
        set {
            objc_setAssociatedObject(self, &AssociatedKey.CellsIdentifier, newValue, .OBJC_ASSOCIATION_RETAIN_NONATOMIC)
        }
    }
    
    private var fin_indexPathHeightCache: [[CGFloat]]? {
        get {
            return objc_getAssociatedObject(self, &AssociatedKey.HeightsCacheIdentifier) as? [[CGFloat]]
        }
        set {
            objc_setAssociatedObject(self, &AssociatedKey.HeightsCacheIdentifier, newValue, .OBJC_ASSOCIATION_RETAIN_NONATOMIC)
        }
    }
}


extension UITableView {
    func cancelEstimatedHeight() {
        self.estimatedRowHeight = 0
        self.estimatedSectionFooterHeight = 0
        self.estimatedSectionHeaderHeight = 0
    }
}

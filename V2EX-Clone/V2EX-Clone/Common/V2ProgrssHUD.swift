//
//  V2ProgrssHUD.swift
//  V2EX-Clone
//
//  Created by yidao on 2019/9/9.
//  Copyright © 2019 yidao. All rights reserved.
//

import UIKit
import SVProgressHUD

class V2ProgrssHUD {
    // 信息提示
    class func inform(_ status: String) {
        SVProgressHUD.showInfo(withStatus: status)
    }
    
    // 进度提示
    class func show(_ status: String) {
        SVProgressHUD.show(withStatus: status)
    }
    
    class func showWithClearMask() {
        SVProgressHUD.setDefaultMaskType(.clear)
        SVProgressHUD.show()
    }
    
    class func dismiss() {
        SVProgressHUD.dismiss()
    }
    
    class func success(_ status: String) {
        SVProgressHUD.showSuccess(withStatus: status)
    }
    
    class func error(_ status: String) {
        SVProgressHUD.showError(withStatus: status)
    }
}

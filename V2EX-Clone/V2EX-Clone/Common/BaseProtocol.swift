//
//  BaseProtocol.swift
//  V2EX-Clone
//
//  Created by yidao on 2019/9/9.
//  Copyright © 2019 yidao. All rights reserved.
//

import Foundation
import Ji

protocol BaseHTMLModelProtocol {
    init(rootNode: JiNode)
}

// 实现这个协议的类，可用于Moya自动解析出这个类的model的对象数组
protocol HTMLModelArrayProtocol {
    static func createModelArray(ji: Ji) -> [Any]
}

// 实现这个协议的类，可用于Moya自动解析出这个类的model的对象
protocol HTMLModelProtocol {
    static func createModel(ji: Ji) -> Any
}

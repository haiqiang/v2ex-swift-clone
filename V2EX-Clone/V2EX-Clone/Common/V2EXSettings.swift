//
//  V2EXSettings.swift
//  V2EX-Clone
//
//  Created by yidao on 2019/9/6.
//  Copyright © 2019 yidao. All rights reserved.
//

import Foundation

private let keyPrefix = "me.fin.V2EXSettings."

class V2EXSettings: NSObject {
    static let instance = V2EXSettings()
    
    private override init() {
        super.init()
    }
    
    subscript(key: String) -> String? {
        get {
            return Defaults()[key + keyPrefix] as? String
        }
        set {
            Defaults()[key + keyPrefix] = newValue
        }
    }
}

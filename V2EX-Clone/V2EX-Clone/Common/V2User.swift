//
//  V2User.swift
//  V2EX-Clone
//
//  Created by yidao on 2019/9/9.
//  Copyright © 2019 yidao. All rights reserved.
//

import UIKit
import Alamofire
import Ji

let kUserName = "me.fin.username"

class V2User: NSObject {
    static let sharedInstance = V2User()
    private var _user: UserModel?
    @objc dynamic var username: String?
    private var _once: String?
    @objc dynamic var notificationCount: Int = 0
    
    var user: UserModel? {
        get {
            return self._user
        }
        set {
            dispatch_async_safely_main_queue {
                self._user = newValue
                self.username = newValue?.username
            }
        }
    }
    
    // 全局once字符串，用于用户各种操作，例如回帖、登录。
    var once: String? {
        get {
            let onceStr = _once
            _once = nil
            return onceStr
        }
        set {
            _once = newValue
        }
    }
    
    var hasOnce: Bool {
        return _once != nil && _once!.count > 0
    }
    
    var isLogin: Bool {
        if let len = self.username?.count, len > 0 {
            return true
        } else {
            return false
        }
    }
    
    private override init() {
        super.init()
        
        dispatch_async_safely_main_queue {
            self.setup()
            if self.isLogin {
                self.verifyLoginStatus()
            }
        }
    }
    
    func setup() {
        self.username = V2EXSettings.instance[kUserName]
    }
    
    func verifyLoginStatus() {
        Alamofire.request(V2EXURL+"new", method: .get, parameters: nil, encoding: URLEncoding(), headers: MOBILE_CLIENT_HEADERS).responseString { [weak self] (response) in
            guard let self = self else { return }
            if response.response?.url?.path == "/signin" {
                // 没有登录，注销客户端
                dispatch_async_safely_main_queue {
                    self.loginOut()
                }
            }
        }
    }
    
    // 删除客户端所有cookies
    func removeAllCookies() {
        let storage = HTTPCookieStorage.shared
        if let cookies = storage.cookies {
            for cookie in cookies {
                storage.deleteCookie(cookie)
            }
        }
    }
    
    func loginOut() {
        removeAllCookies()
        self.username = nil
        self.user = nil
        self.once = nil
        self.notificationCount = 0
        V2EXSettings.instance[kUserName] = nil
    }
    
    func ensureLoginWithHandler(_ handler: () -> ()) {
        guard self.isLogin else {
            V2ProgrssHUD.inform("请先登录")
            return
        }
        handler()
    }
    
    func getOnce(_ url: String = V2EXURL + "signin", completionHandler: @escaping (V2Response) -> Void) {
        Alamofire.request(url, method: .get, parameters: nil, encoding: URLEncoding(), headers: MOBILE_CLIENT_HEADERS).responseJiHTML { (response) in
            if let JiHTML = response.result.value {
                if let once = JiHTML.xPath("//*[@name='once'][1]")?.first?["value"] {
                    self.once = once
                    completionHandler(V2Response(success: true))
                    return
                }
            }
            completionHandler(V2Response(success: false))
        }
    }
    
    func getNotificationsCount(_ rootNode: JiNode) {
        let notification = rootNode.xPath("//head/title").first?.content
        if let notification = notification {
            self.notificationCount = 0
            
            let regex = try! NSRegularExpression(pattern: "V2EX \\([0-9]+\\)", options: [.caseInsensitive])
            regex.enumerateMatches(in: notification, options: [.withoutAnchoringBounds], range: NSRange(location: 0, length: notification.count)) { (result, flags, stop) in
                if let result = result {
                    let startIndex = notification.index(notification.startIndex, offsetBy: result.range.location + 6)
                    let endIndex = notification.index(notification.startIndex, offsetBy: result.range.location + result.range.length - 1)
                    let count = notification[startIndex..<endIndex]
                    if let account = Int(count) {
                        self.notificationCount = account
                    }
                }
            }
        }
    }
}

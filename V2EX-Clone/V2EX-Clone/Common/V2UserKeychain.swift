//
//  V2UserKeychain.swift
//  V2EX-Clone
//
//  Created by yidao on 2019/9/10.
//  Copyright © 2019 yidao. All rights reserved.
//

import Foundation
import KeychainSwift

class V2UserKeychain {
    static let userKey = "me.fin.testDict"
    static let sharedInstance = V2UserKeychain()
    private let keychain = KeychainSwift()
    
    fileprivate(set) var users: [String: LocalSecurityAccountModel] = [:]
    
    private init() {
        let _ = loadUserDict()
    }
    
    func addUser(_ user: LocalSecurityAccountModel) {
        if let username = user.username {
            self.users[username] = user
            self.saveUsersDict()
        } else {
            assert(false, "user must not be nil")
        }
    }
    
    func addUser(_ username: String, password: String, avata: String? = nil) {
        let user = LocalSecurityAccountModel()
        user.username = username
        user.avatar = avata
        self.addUser(user)
    }
    
    func loadUserDict() -> [String: LocalSecurityAccountModel] {
        if users.count == 0 {
            let data = keychain.getData(V2UserKeychain.userKey)
            if let data = data {
                if let unarchiver = try? NSKeyedUnarchiver(forReadingFrom: data) {
                    
                    let userDict = try? unarchiver.decodeTopLevelDecodable(Dictionary<String, LocalSecurityAccountModel>.self, forKey: NSKeyedArchiveRootObjectKey)
                    unarchiver.finishDecoding()
                    
                    if let userDic = userDict {
                        self.users = userDic
                    }
                }
            }
        }
        return self.users
    }
    
    func saveUsersDict() {
        let data = NSMutableData()
//        let archiver = NSKeyedArchiver(requiringSecureCoding: false)
        let archiver = NSKeyedArchiver(forWritingWith: data)
        try! archiver.encodeEncodable(self.users, forKey: NSKeyedArchiveRootObjectKey)
        archiver.finishEncoding()
        keychain.set(data as Data, forKey: V2UserKeychain.userKey)
    }
    
    func removeUser(_ username: String) {
        self.users.removeValue(forKey: username)
        self.saveUsersDict()
    }
    
    func update(_ username: String, password: String? = nil, avatar: String? = nil) {
        if let user = self.users[username] {
            if let avatar = avatar {
                user.avatar = avatar
            }
            self.saveUsersDict()
        }
    }
}

class LocalSecurityAccountModel: Codable {
    
    var username: String?
    var avatar: String?
    
}


//
//  V2EXColor.swift
//  V2EX-Clone
//
//  Created by yidao on 2019/9/6.
//  Copyright © 2019 yidao. All rights reserved.
//

import UIKit
import KVOController

protocol V2EXColorProtocol {
    var v2_backgroundColor: UIColor { get }
    var v2_navigationBarTintColor: UIColor { get }
    var v2_topicListTitleColor: UIColor { get }
    var v2_topicListUserNameColor: UIColor { get }
    var v2_topicListDateColor: UIColor { get }
    
    var v2_linkColor: UIColor { get }
    
    var v2_textViewBackgroundColor: UIColor { get }
    
    var v2_cellWhiteBackgroundColor: UIColor { get }
    
    var v2_nodeBackgroundColor: UIColor { get }
    
    var v2_separatorColor: UIColor { get }
    
    var v2_leftNodeBackgroundColor: UIColor { get }
    var v2_leftNodeBackgroundHighLightColor: UIColor { get }
    var v2_leftNodeTintColor: UIColor { get }

    var v2_noticePointColor: UIColor { get }
    
    var v2_buttonBackgroundColor: UIColor { get }
}

class V2EXDefaultColor: NSObject, V2EXColorProtocol {
    var v2_backgroundColor: UIColor {
        return UIColor.RGB(r: 242.0, g: 243.0, b: 245.0)
    }
    
    var v2_navigationBarTintColor: UIColor {
        return UIColor.RGB(r: 102.0, g: 102.0, b: 102.0)
    }
    
    var v2_topicListTitleColor: UIColor {
        return UIColor.RGB(r: 15.0, g: 15.0, b: 15.0)
    }
    
    var v2_topicListUserNameColor: UIColor {
        return UIColor.RGB(r: 53.0, g: 53.0, b: 53.0)
    }
    
    var v2_topicListDateColor: UIColor {
        return UIColor.RGB(r: 173.0, g: 173.0, b: 173.0)
    }
    
    var v2_linkColor: UIColor {
        return UIColor.RGB(r: 119.0, g: 128.0, b: 135.0)
    }
    
    var v2_textViewBackgroundColor: UIColor {
        return UIColor.white
    }
    
    var v2_cellWhiteBackgroundColor: UIColor {
        return UIColor.white
    }
    
    var v2_nodeBackgroundColor: UIColor {
        return UIColor.RGB(r: 242.0, g: 242.0, b: 242.0)
    }
    
    var v2_separatorColor: UIColor {
        return UIColor.RGB(r: 190.0, g: 190.0, b: 190.0)
    }
    
    var v2_leftNodeBackgroundColor: UIColor {
        return UIColor.RGBA(r: 255.0, g: 255.0, b: 255.0, alpha: 0.30)
    }
    
    var v2_leftNodeBackgroundHighLightColor: UIColor {
        return UIColor.RGBA(r: 255.0, g: 255.0, b: 255.0, alpha: 0.20)
    }
    
    var v2_leftNodeTintColor: UIColor {
        return UIColor.RGBA(r: 0, g: 0, b: 0, alpha: 0.55)
    }
    
    var v2_noticePointColor: UIColor {
        return UIColor.RGB(r: 207.0, g: 70.0, b: 71.0)
    }
    
    var v2_buttonBackgroundColor: UIColor {
        return UIColor.RGB(r: 85.0, g: 172.0, b: 238.0)
    }
    
    static let instance = V2EXDefaultColor()
    
    private override init() {
        super.init()
    }
    
}

class V2EXDarkColor: NSObject, V2EXColorProtocol {
    var v2_backgroundColor: UIColor {
        return UIColor.RGB(r: 32.0, g: 31.0, b: 35.0)
    }
    
    var v2_navigationBarTintColor: UIColor {
        return UIColor.RGB(r: 165.0, g: 165.0, b: 165.0)
    }
    
    var v2_topicListTitleColor: UIColor {
        return UIColor.RGB(r: 145.0, g: 145.0, b: 145.0)
    }
    
    var v2_topicListUserNameColor: UIColor {
        return UIColor.RGB(r: 125.0, g: 125.0, b: 125.0)
    }
    
    var v2_topicListDateColor: UIColor {
        return UIColor.RGB(r: 100.0, g: 100.0, b: 100.0)
    }
    
    var v2_linkColor: UIColor {
        return UIColor.RGB(r: 119.0, g: 128.0, b: 135.0)
    }
    
    var v2_textViewBackgroundColor: UIColor {
        return UIColor.RGB(r: 35.0, g: 34.0, b: 38.0)
    }
    
    var v2_cellWhiteBackgroundColor: UIColor {
        return UIColor.RGB(r: 35.0, g: 34.0, b: 38.0)
    }
    
    var v2_nodeBackgroundColor: UIColor {
        return UIColor.RGB(r: 40.0, g: 40.0, b: 40.0)
    }
    
    var v2_separatorColor: UIColor {
        return UIColor.RGB(r: 46.0, g: 45.0, b: 49.0)
    }
    
    var v2_leftNodeBackgroundColor: UIColor {
        return UIColor.RGBA(r: 255.0, g: 255.0, b: 255.0, alpha: 0.30)
    }
    
    var v2_leftNodeBackgroundHighLightColor: UIColor {
        return UIColor.RGBA(r: 255.0, g: 255.0, b: 255.0, alpha: 0.20)
    }
    
    var v2_leftNodeTintColor: UIColor {
        return UIColor.RGBA(r: 0, g: 0, b: 0, alpha: 0.55)
    }
    
    var v2_noticePointColor: UIColor {
        return UIColor.RGB(r: 207.0, g: 70.0, b: 71.0)
    }
    
    var v2_buttonBackgroundColor: UIColor {
        return UIColor.RGB(r: 207.0, g: 70.0, b: 71.0)
    }
    
    static let instance = V2EXDarkColor()
    private override init() {
        super.init()
    }
}

class V2EXColor: NSObject {
    static let STYLE_KEY = "styleKey"
    
    static let V2EXColorStyleDefault = "Default"
    static let V2EXColorStyleDark = "Dark"
    
    fileprivate static var _colors: V2EXColorProtocol?
    
    static var colors: V2EXColorProtocol {
        get {
            if let c = V2EXColor._colors {
                return c
            } else {
                if V2EXColor.instance.style == V2EXColor.V2EXColorStyleDefault {
                    return V2EXDefaultColor.instance
                } else {
                    return V2EXDarkColor.instance
                }
            }
        }
        set {
            V2EXColor._colors = newValue
        }
    }
    
    @objc dynamic var style: String
    static let instance = V2EXColor()
    
    private override init() {
        if let style = V2EXSettings.instance[V2EXColor.STYLE_KEY] {
            self.style = style
        } else {
            self.style = V2EXColor.V2EXColorStyleDefault
        }
        super.init()
    }
    
    func setStyleAndSave(_ style: String) {
        if self.style == style {
            return
        }
        
        if style == V2EXColor.V2EXColorStyleDefault {
            V2EXColor.colors = V2EXDefaultColor.instance
        } else {
            V2EXColor.colors = V2EXDarkColor.instance
        }
        
        self.style = style
        V2EXSettings.instance[V2EXColor.STYLE_KEY] = style
    }
}

extension NSObject {
    private struct AssociatedKeys {
        static var themeChanged = "themeChanged"
    }
    
    typealias ThemeChangedClosure = @convention(block) (_ style: String) -> Void
    
    var themeChangedHandler: ThemeChangedClosure? {
        get {
            let closureObject: AnyObject? = objc_getAssociatedObject(self, &AssociatedKeys.themeChanged) as AnyObject?
            guard closureObject != nil else {
                return nil
            }
            let closure = unsafeBitCast(closureObject, to: ThemeChangedClosure.self)
            return closure
        }
        set {
            guard let value = newValue else {
                return
            }
            let dealObject: AnyObject = unsafeBitCast(value, to: AnyObject.self)
            objc_setAssociatedObject(self, &AssociatedKeys.themeChanged, dealObject, objc_AssociationPolicy.OBJC_ASSOCIATION_RETAIN_NONATOMIC)
            
            // 设置KVO监听
            self.kvoController.observe(V2EXColor.instance, keyPath: "style", options: [.initial, .new]) { [weak self](nav, color, change) in
                self?.themeChangedHandler?(V2EXColor.instance.style)
            }
        }
    }
}





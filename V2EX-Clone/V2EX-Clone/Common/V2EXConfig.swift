//
//  V2EXConfig.swift
//  V2EX-Clone
//
//  Created by yidao on 2019/9/6.
//  Copyright © 2019 yidao. All rights reserved.
//

import UIKit
import DeviceKit

let kScreenWidth = UIScreen.main.bounds.width
let kScreenHeight = UIScreen.main.bounds.height

let kNavigationBarHeight: CGFloat = {
    if UIDevice.current.isIphoneX {
        return 88
    }
    return 64
}()

//用户代理，使用这个切换是获取 m站点 还是www站数据
let USER_AGENT = "Mozilla/5.0 (iPhone; CPU iPhone OS 8_0 like Mac OS X) AppleWebKit/600.1.3 (KHTML, like Gecko) Version/8.0 Mobile/12A4345d Safari/600.1.4";
let MOBILE_CLIENT_HEADERS = ["user-agent":USER_AGENT]

let V2EXURL = "https://www.v2ex.com/"

let SEPARATOR_HEIGHT = 1.0 / UIScreen.main.scale

func NSLocalozedString(_ key: String) -> String {
    return NSLocalizedString(key, comment: "")
}

func dispatch_async_safely_main_queue(_ block: @escaping () -> ()) {
    if Thread.isMainThread {
        block()
    } else {
        DispatchQueue.main.async {
            block()
        }
    }
}

extension UIDevice {
    var isIphoneX: Bool {
        get {
            let device = Device()
            if device.isOneOf(Device.allXSeriesDevices + Device.allSimulatorXSeriesDevices) {
                return true
            }
            return false
        }
    }
}




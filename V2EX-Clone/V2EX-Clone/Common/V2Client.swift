//
//  V2Client.swift
//  V2EX-Clone
//
//  Created by yidao on 2019/9/11.
//  Copyright © 2019 yidao. All rights reserved.
//

import UIKit
import DrawerController

class V2Client: NSObject {
    static let instance = V2Client()
    
    var drawerController: DrawerController?
    var centerViewController: HomeViewController?
    var centerNavigation: V2EXNavigationController?
    
    var topNavigationController: UINavigationController {
        return V2Client.getTopNavigationController(currentNav: V2Client.instance.centerNavigation!)
    }
    
    private class func getTopNavigationController(currentNav: UINavigationController) -> UINavigationController {
        if let topNav = currentNav.visibleViewController?.navigationController {
            if topNav != currentNav && topNav.isKind(of: UINavigationController.self) {
                return getTopNavigationController(currentNav: topNav)
            }
        }
        return currentNav
    }
    
    private override init() {
        super.init()
    }
}

//
//  V2EXMentionedBindingParser.swift
//  V2EX-Clone
//
//  Created by yidao on 2019/9/24.
//  Copyright © 2019 yidao. All rights reserved.
//

import UIKit
import YYText

class V2EXMentionedBindingParser: NSObject, YYTextParser {
    var regex: NSRegularExpression
    
    override init() {
        self.regex = try! NSRegularExpression(pattern: "@(\\S+)\\s", options: [.caseInsensitive])
        super.init()
    }
    
    func parseText(_ text: NSMutableAttributedString?, selectedRange: NSRangePointer?) -> Bool {
        guard let text = text else {
            return false
        }
        
        self.regex.enumerateMatches(in: text.string, options: [.withoutAnchoringBounds], range: text.yy_rangeOfAll()) { (result, flags, stop) in
            if let result = result {
                let range = result.range
                if range.location == NSNotFound || range.length < 1 {
                    return
                }
                
                if text.attribute(NSAttributedString.Key(YYTextBindingAttributeName), at: range.location, effectiveRange: nil) != nil {
                    return
                }
                
                let bindlingRange = NSMakeRange(range.location, range.length - 1)
                let binding = YYTextBinding()
                binding.deleteConfirm = true
                text.yy_setTextBinding(binding, range: bindlingRange)
                text.yy_setColor(UIColor.RGB(r: 0, g: 132, b: 255), range: bindlingRange)
            }
        }
        return false
    }
    
}

class ReplyViewController: WritingViewController {
    var atSomeone: String?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "reply".localized
        if let atSomeone = self.atSomeone {
            let str = NSMutableAttributedString(string: atSomeone)
            str.yy_font = self.textView!.font
            str.yy_color = self.textView!.textColor
            
            self.textView!.attributedText = str
            self.textView!.selectedRange = NSRange(location: atSomeone.count, length: 0)
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        self.textView?.becomeFirstResponder()
    }
    
    override func rightClick() {
        if self.textView?.text == nil || (self.textView?.text.count)! <= 0 {
            return
        }
        
        V2ProgrssHUD.showWithClearMask()
        TopicCommentModel.replyWithTopicId(self.topicModel!, content: self.textView!.text) { (response) in
            if response.success {
                V2ProgrssHUD.success("回复成功")
                self.dismiss(animated: true, completion: nil)
            } else {
                V2ProgrssHUD.error(response.message)
            }
        }
    }
}

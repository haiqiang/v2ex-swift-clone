# V2EX-Swift-Clone

Swift5.0

⌚️
1.learn from https://github.com/Finb/V2ex-Swift

2019.9.9
1.移除ObjectMapper， 使用Codable

2019.9.17
Unrecognized selector replacementObjectForKeyedArchiver:
[Codable & NSKeyArchiver](https://stackoverflow.com/questions/49951731/nskeyedarchiver-does-not-archive-my-codable-class?r=SearchResults)

2019.9.19  (Note: JSON parameters type)
1. confirm parameters type ~~~debugDescription: "Expected to decode String but found a number instead.", underlyingError: nil)  
2.[convert API parameters type](https://www.jianshu.com/p/21c8724e7b12)

2019.9.23
This is likely occurring because the flow layout subclass V2EX_Clone.V2LeftAlignedCollectionViewFlowLayout is modifying attributes returned by UICollectionViewFlowLayout without copying them
[reference this blog to fix console issues](https://www.jianshu.com/p/e81423ac91eb)

